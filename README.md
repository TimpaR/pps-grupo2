
# DetailingGarage APP

Nuestro Sistema de Gestión de Turnos es una solución completa diseñada específicamente para optimizar y mejorar la experiencia de los clientes en los servicios de lavado y cuidado de vehículos. Con una interfaz intuitiva y funcionalidades avanzadas, nuestro sistema está diseñado para brindar comodidad, eficiencia y calidad en cada interacción.

El cliente podra hacer ABM de cuenta en la cual podra cargar sus vehiculos a los cuales podra agendar un turno para el servicio que desee realizar.

El empleado podra hacer ABM de turnos. 

El administrador podra hacer ABM de usuarios, ABM de sucursales ABM de servicios , eliminar reseñas, visualizar las estadisticas y agregar vehiculos a un usuario preexistente.
## Tecnologias

**Frontend:** React, TailwindCSS

**Backend:** ASP.NET Core Web Application

**DataBase:** MySQL


## Autores

- [@tomironcoroni](https://gitlab.com/tomironcoroni)
- [@RuizFrancisco](https://gitlab.com/RuizFrancisco)
- [@TimpaR](https://gitlab.com/TimpaR)
- [@sebassueiro](https://gitlab.com/sebassueiro)

![Logo APP](https://i.pinimg.com/736x/b7/76/5b/b7765b063b3eebebd074fd3db2732d38.jpg)


