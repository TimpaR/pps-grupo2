
# Proyecto
DetailingGARAGE API

## Descripción del Proyecto
Este repositorio contiene el código fuente y la documentación asociada al proyecto Detailing API desarrollado en ASP.NET con Visual Studio. Este proyecto es parte del Trabajo Práctico Integrador (TPI) del Grupo 2, enfocado en proporcionar una API RESTful para gestionar y mostrar detalles específicos de elementos dentro de una aplicación web.

## Estructura del Proyecto
El proyecto está organizado en diferentes directorios y archivos clave:

/Controllers: Contiene los controladores de la API que manejan las solicitudes HTTP.

/Context: Contiene la configuración y el contexto de Entity Framework Core para la base de datos.

/Entities: Define las entidades que representan las tablas de la base de datos.

/Models: Define los modelos de datos utilizados por la API.

/Observer: Implementa el patrón Observer para ciertas funcionalidades del proyecto.

/Repositories: Contiene los repositorios que manejan el acceso a datos y las operaciones CRUD.

/Services: Contiene lógica adicional de negocio o servicios utilizados por los controladores.

/Utils: Contiene clases de utilidad, como mapeos de objetos entre capas del sistema.

/Tests: Contiene pruebas unitarias y de integración para garantizar la calidad del código.

/Properties: Contiene configuraciones y propiedades del proyecto.

appsettings.json: Archivo de configuración principal para configurar la aplicación, incluyendo la cadena de conexión a la base de datos.

Startup.cs: Clase principal donde se configura y se establece el comportamiento de la aplicación ASP.NET Core.

## Tecnologías Utilizadas
El proyecto hace uso de las siguientes tecnologías y herramientas:

* ASP.NET Core: Framework utilizado para construir aplicaciones web y APIs en .NET.

* C#: Lenguaje de programación principal utilizado en el desarrollo del API.

* Entity Framework Core: ORM utilizado para interactuar con la base de datos.

* Swagger: Herramienta para documentar automáticamente la API.

* xUnit y Moq: Frameworks utilizados para escribir pruebas unitarias y de integración.

* Visual Studio: Entorno de desarrollo integrado (IDE) utilizado para desarrollar, depurar y administrar el proyecto.
## Instalación y Uso
1) Clona este repositorio en tu máquina local.
```bash
https://gitlab.com/TimpaR/pps-grupo2.git
```
2) Abre el proyecto en Visual Studio.
3) Configura las conexiones a la base de datos y otras configuraciones necesarias en appsettings.json o archivos de configuración relevantes.
4) Compila y ejecuta la aplicación desde Visual Studio.