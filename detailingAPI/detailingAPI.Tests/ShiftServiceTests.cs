﻿using detailingAPI.Entities;
using detailingAPI.Repositories.Interfaces;
using detailingAPI.Services.Implementations;
using Moq;

namespace detailingAPI.Tests
{
    public class ShiftServiceTests
    {
        [Fact]
        public async Task TestObserverPattern()
        {
            var mockShiftRepository = new Mock<IShiftRepository>();
            var shiftService = new ShiftService(mockShiftRepository.Object);
            var mockShiftObserver = new Mock<IShiftObserver>();

            shiftService.AgregarObservador(mockShiftObserver.Object);

            var shiftId = 1;
            var shiftDate = new DateOnly(2024, 6, 30);
            var shiftStartTime = new TimeOnly(9, 0);
            var shiftEndTime = new TimeOnly(11, 0);
            var mockShift = new Shift
            {
                IdShift = shiftId,
                IdVehicle = 1,
                IdBranch = 1,
                IdService = 1,
                Date = shiftDate,
                StartTime = shiftStartTime,
                EndTime = shiftEndTime
            };

            mockShiftRepository.Setup(repo => repo.CreateShift(It.IsAny<Shift>()))
                               .ReturnsAsync((Shift s) =>
                               {
                                   s.IdShift = shiftId; 
                                   return s;
                               });

            var createdShift = await shiftService.CreateShift(mockShift);

            mockShiftObserver.Verify(o => o.Actualizar(createdShift, "creación"), Times.Once);
        }
    }
}
