﻿using detailingAPI.Entities;
using detailingAPI.Repositories;
using detailingAPI.Services.Implementations;
using detailingAPI.Services.Interfaces;
using Moq;

namespace detailingAPI.Tests.Services
{
    public class BranchServiceTests
    {
        private readonly Mock<IBranchRepository> _branchRepositoryMock;
        private readonly IBranchService _branchService;

        public BranchServiceTests()
        {
            _branchRepositoryMock = new Mock<IBranchRepository>();
            _branchService = new BranchService(_branchRepositoryMock.Object);
        }

        [Fact]
        public async Task GetAllBranches_ShouldReturnAllBranches()
        {
            // Arrange
            var branches = new List<Branch>
            {
                new Branch { IdBranch = 1, Name = "Branch1", Location = "Location1" },
                new Branch { IdBranch = 2, Name = "Branch2", Location = "Location2" }
            };
            _branchRepositoryMock.Setup(repo => repo.GetAllBranches()).ReturnsAsync(branches);

            // Act
            var result = await _branchService.GetAllBranches();

            // Assert
            Assert.Equal(branches.Count, result.Count());
            Assert.Equal(branches, result);
        }

        [Fact]
        public async Task GetBranchById_ShouldReturnBranch_WhenBranchExists()
        {
            // Arrange
            var branchId = 1;
            var branch = new Branch { IdBranch = branchId, Name = "Branch1", Location = "Location1" };
            _branchRepositoryMock.Setup(repo => repo.GetBranchById(branchId)).ReturnsAsync(branch);

            // Act
            var result = await _branchService.GetBranchById(branchId);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(branchId, result.IdBranch);
            Assert.Equal("Branch1", result.Name);
            Assert.Equal("Location1", result.Location);
        }

        [Fact]
        public async Task GetBranchById_ShouldReturnNull_WhenBranchDoesNotExist()
        {
            // Arrange
            var branchId = 1;
            _branchRepositoryMock.Setup(repo => repo.GetBranchById(branchId)).ReturnsAsync((Branch?)null);

            // Act
            var result = await _branchService.GetBranchById(branchId);

            // Assert
            Assert.Null(result);
        }

        [Fact]
        public async Task CreateBranch_ShouldReturnCreatedBranch()
        {
            // Arrange
            var branch = new Branch { Name = "New Branch", Location = "New Location" };
            _branchRepositoryMock.Setup(repo => repo.CreateBranch(branch)).ReturnsAsync(branch);

            // Act
            var result = await _branchService.CreateBranch(branch);

            // Assert
            Assert.NotNull(result);
            Assert.Equal("New Branch", result.Name);
            Assert.Equal("New Location", result.Location);
        }

        [Fact]
        public async Task UpdateBranch_ShouldReturnUpdatedBranch()
        {
            // Arrange
            var branch = new Branch { IdBranch = 1, Name = "Updated Branch", Location = "Updated Location" };
            _branchRepositoryMock.Setup(repo => repo.UpdateBranch(branch)).ReturnsAsync(branch);

            // Act
            var result = await _branchService.UpdateBranch(branch);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(branch.IdBranch, result.IdBranch);
            Assert.Equal("Updated Branch", result.Name);
            Assert.Equal("Updated Location", result.Location);
        }

        [Fact]
        public async Task DeleteBranch_ShouldReturnTrue_WhenBranchIsDeleted()
        {
            // Arrange
            var branchId = 1;
            _branchRepositoryMock.Setup(repo => repo.DeleteBranch(branchId)).ReturnsAsync(true);

            // Act
            var result = await _branchService.DeleteBranch(branchId);

            // Assert
            Assert.True(result);
        }

        [Fact]
        public async Task DeleteBranch_ShouldReturnFalse_WhenBranchIsNotDeleted()
        {
            // Arrange
            var branchId = 1;
            _branchRepositoryMock.Setup(repo => repo.DeleteBranch(branchId)).ReturnsAsync(false);

            // Act
            var result = await _branchService.DeleteBranch(branchId);

            // Assert
            Assert.False(result);
        }
    }
}
