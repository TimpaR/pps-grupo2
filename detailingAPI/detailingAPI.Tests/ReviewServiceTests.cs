﻿using detailingAPI.Entities;
using detailingAPI.Repositories.Interfaces;
using detailingAPI.Services.Implementations;
using detailingAPI.Services.Interfaces;
using Moq;

namespace detailingAPI.Tests.Services
{
    public class ReviewServiceTests
    {
        private readonly Mock<IReviewRepository> _reviewRepositoryMock;
        private readonly IReviewService _reviewService;

        public ReviewServiceTests()
        {
            _reviewRepositoryMock = new Mock<IReviewRepository>();
            _reviewService = new ReviewService(_reviewRepositoryMock.Object);
        }

        [Fact]
        public async Task GetAllReviews_ShouldReturnAllReviews()
        {
            // Arrange
            var reviews = new List<Review>
            {
                new Review { IdReview = 1, Rating = 5, Comment = "Good service" },
                new Review { IdReview = 2, Rating = 4, Comment = "Nice experience" }
            };
            _reviewRepositoryMock.Setup(repo => repo.GetAllReviews()).ReturnsAsync(reviews);

            // Act
            var result = await _reviewService.GetAllReviews();

            // Assert
            Assert.Equal(reviews.Count, result.Count());
            Assert.Equal(reviews, result);
        }

        [Fact]
        public async Task GetReviewById_ShouldReturnReview_WhenReviewExists()
        {
            // Arrange
            var reviewId = 1;
            var review = new Review { IdReview = reviewId, Rating = 5, Comment = "Good service" };
            _reviewRepositoryMock.Setup(repo => repo.GetReviewById(reviewId)).ReturnsAsync(review);

            // Act
            var result = await _reviewService.GetReviewById(reviewId);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(reviewId, result.IdReview);
            Assert.Equal(5, result.Rating);
            Assert.Equal("Good service", result.Comment);
        }

        [Fact]
        public async Task GetReviewById_ShouldReturnNull_WhenReviewDoesNotExist()
        {
            // Arrange
            var reviewId = 1;
            _reviewRepositoryMock.Setup(repo => repo.GetReviewById(reviewId)).ReturnsAsync((Review)null);

            // Act
            var result = await _reviewService.GetReviewById(reviewId);

            // Assert
            Assert.Null(result);
        }

        [Fact]
        public async Task CreateReview_ShouldReturnCreatedReview()
        {
            // Arrange
            var review = new Review { Rating = 5, Comment = "Great experience" };
            _reviewRepositoryMock.Setup(repo => repo.CreateReview(review)).ReturnsAsync(review);

            // Act
            var result = await _reviewService.CreateReview(review);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(5, result.Rating);
            Assert.Equal("Great experience", result.Comment);
        }

        [Fact]
        public async Task DeleteReview_ShouldReturnTrue_WhenReviewIsDeleted()
        {
            // Arrange
            var reviewId = 1;
            _reviewRepositoryMock.Setup(repo => repo.DeleteReview(reviewId)).ReturnsAsync(true);

            // Act
            var result = await _reviewService.DeleteReview(reviewId);

            // Assert
            Assert.True(result);
        }

        [Fact]
        public async Task DeleteReview_ShouldReturnFalse_WhenReviewIsNotDeleted()
        {
            // Arrange
            var reviewId = 1;
            _reviewRepositoryMock.Setup(repo => repo.DeleteReview(reviewId)).ReturnsAsync(false);

            // Act
            var result = await _reviewService.DeleteReview(reviewId);

            // Assert
            Assert.False(result);
        }
    }
}
