﻿namespace detailingAPI.Entities;

public partial class Shift
{
    public int IdShift { get; set; }

    public int? IdVehicle { get; set; }

    public int IdBranch { get; set; }

    public int? IdService { get; set; }

    public DateOnly Date { get; set; }

    public TimeOnly StartTime { get; set; }

    public TimeOnly EndTime { get; set; }

    public virtual Branch IdBranchNavigation { get; set; } = null!;

    public virtual Service? IdServiceNavigation { get; set; }

    public virtual Vehicle? IdVehicleNavigation { get; set; }
}
