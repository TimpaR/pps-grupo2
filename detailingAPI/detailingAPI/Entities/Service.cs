﻿namespace detailingAPI.Entities;

public partial class Service
{
    public int IdServicio { get; set; }

    public string? UrlImage { get; set; }

    public string Name { get; set; } = null!;

    public string Description { get; set; } = null!;

    public decimal Price { get; set; }

    public virtual ICollection<Shift> Shifts { get; set; } = new List<Shift>();
}
