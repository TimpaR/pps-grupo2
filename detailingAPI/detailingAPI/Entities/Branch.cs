﻿namespace detailingAPI.Entities;

public partial class Branch
{
    public int IdBranch { get; set; }

    public string Location { get; set; } = null!;

    public string Name { get; set; } = null!;

    public string? Phone { get; set; }

    public virtual ICollection<Shift> Shifts { get; set; } = new List<Shift>();

    public virtual ICollection<User> Users { get; set; } = new List<User>();
}
