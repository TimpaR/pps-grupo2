﻿namespace detailingAPI.Entities;

public partial class Vehicle
{
    public int IdVehicle { get; set; }

    public int IdClient { get; set; }

    public string CarPatent { get; set; } = null!;

    public string Model { get; set; } = null!;

    public string Brand { get; set; } = null!;

    public virtual User IdClientNavigation { get; set; } = null!;

    public virtual ICollection<Shift> Shifts { get; set; } = new List<Shift>();
}
