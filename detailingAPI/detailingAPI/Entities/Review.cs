﻿namespace detailingAPI.Entities;

public partial class Review
{
    public int IdReview { get; set; }

    public int IdClient { get; set; }

    public int Rating { get; set; }

    public string? Comment { get; set; }

    public virtual User IdClientNavigation { get; set; } = null!;
}
