﻿using detailingAPI.Entities;

public interface IShiftObservable
{
    void AgregarObservador(IShiftObserver observador);
    void EliminarObservador(IShiftObserver observador);
    void NotificarObservadores(Shift shift, string tipoAccion);
}
