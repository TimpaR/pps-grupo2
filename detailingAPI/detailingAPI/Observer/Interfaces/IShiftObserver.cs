﻿using detailingAPI.Entities;

public interface IShiftObserver
{
    void Actualizar(Shift shift, string tipoAccion);
}