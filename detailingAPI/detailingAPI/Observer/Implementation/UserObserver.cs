﻿using detailingAPI.Entities;

public class UserObserver : IShiftObserver
{
    public void Actualizar(Shift shift, string tipoAccion)
    {
        Console.WriteLine($"Actualización para usuario relacionado con turno {shift.IdShift}, acción: {tipoAccion}");
    }
}