﻿using detailingAPI.Entities;

public class ShiftObservable : IShiftObservable
{
    private readonly List<IShiftObserver> _observadores = new List<IShiftObserver>();

    public void AgregarObservador(IShiftObserver observador)
    {
        _observadores.Add(observador);
    }

    public void EliminarObservador(IShiftObserver observador)
    {
        _observadores.Remove(observador);
    }

    public void NotificarObservadores(Shift shift, string tipoAccion)
    {
        foreach (var observador in _observadores)
        {
            observador.Actualizar(shift, tipoAccion);
        }
    }
}
