﻿public class BranchCreateDTO
{
    public string Location { get; set; } = null!;

    public string Name { get; set; } = null!;

    public string? Phone { get; set; }
}
