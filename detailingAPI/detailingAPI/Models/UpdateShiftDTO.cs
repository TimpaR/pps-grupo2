﻿namespace detailingAPI.Models
{
    public class UpdateShiftDTO
    {
        public int? IdVehicle { get; set; }

        public int IdBranch { get; set; }

        public int? IdService { get; set; }
    }
}
