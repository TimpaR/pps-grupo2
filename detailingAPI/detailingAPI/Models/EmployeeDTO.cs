﻿namespace detailingAPI.DTOs
{
    public class EmployeeDTO
    {
        public string Name { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }
        public string IdBranch { get; set; }
    }
}
