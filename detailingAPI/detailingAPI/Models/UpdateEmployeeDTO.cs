﻿namespace detailingAPI.Models
{
    public class UpdateEmployeeDTO
    {
        public string Name { get; set; }
        public string Lastname { get; set; }
        public string Phone { get; set; }
        public int? IdBranch { get; set; }
    }
}
