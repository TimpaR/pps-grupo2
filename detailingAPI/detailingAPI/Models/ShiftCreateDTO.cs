﻿namespace detailingAPI.Models
{
    public class ShiftCreateDTO
    {

        public int? IdVehicle { get; set; }

        public int IdBranch { get; set; }

        public int? IdService { get; set; }

        public DateOnly Date { get; set; }

        public TimeOnly StartTime { get; set; }

        public TimeOnly EndTime { get; set; }

    }
}
