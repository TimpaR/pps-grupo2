﻿namespace detailingAPI.Models
{
    public class MessageDto
    {
        public const string UserRegisteredSuccessfully = "Usuario registrado correctamente";
        public const string ErrorRegisteringUser = "Error al registrar el usuario";
        public const string ErrorSomethingWentWrong = "Algo salió mal, por favor inténtalo de nuevo más tarde";
        public const string AllFieldsRequired = "Todos los campos son obligatorios";
    }
}
