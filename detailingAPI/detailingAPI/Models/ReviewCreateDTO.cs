﻿namespace detailingAPI.Models
{
    public class ReviewCreateDTO
    {
        public int IdClient { get; set; }

        public int Rating { get; set; }

        public string? Comment { get; set; }
    }
}
