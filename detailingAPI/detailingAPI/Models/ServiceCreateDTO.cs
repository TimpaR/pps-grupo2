﻿namespace detailingAPI.Models
{
    public class ServiceCreateDTO
    {
        public string? UrlImage { get; set; }
        public string Name { get; set; }
        public string Description { get; set; } = null!;
        public decimal Price { get; set; }
    }
}
