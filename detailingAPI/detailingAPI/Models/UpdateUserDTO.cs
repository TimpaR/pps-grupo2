﻿namespace detailingAPI.Models
{
    public class UpdateUserDTO
    {
        public string Name { get; set; }
        public string Lastname { get; set; }
        public string Phone { get; set; }
    }
}
