﻿namespace detailingAPI.Models
{
    public class VehicleDTO
    {
        public string CarPatent { get; set; }

        public string Brand { get; set; }

        public string Model { get; set; }

        public int IdClient { get; set; }

    }
}
