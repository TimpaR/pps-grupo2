﻿namespace detailingAPI.Models
{
    public class UpdateServiceDTO
    {
        public string? UrlImage { get; set; }
        public string Description { get; set; } = null!;
        public decimal Price { get; set; }
    }
}
