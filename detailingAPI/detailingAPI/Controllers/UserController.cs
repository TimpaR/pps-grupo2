﻿using AutoMapper;
using detailingAPI.DTOs;
using detailingAPI.Entities;
using detailingAPI.Models;
using detailingAPI.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace detailingAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public UserController(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        [HttpPost("client")]
        public async Task<IActionResult> Register(ClientDTO clientDTO)
        {
            try
            {
                var existingUser = _userRepository.GetUserByEmail(clientDTO.Email);
                if (existingUser != null)
                {
                    return Conflict("El correo electrónico ya está registrado.");
                }

                var user = _mapper.Map<User>(clientDTO);
                user.UserType = "client";

                _userRepository.CreateUser(user, clientDTO.Password);

                return Ok("Usuario cliente creado exitosamente.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error al registrar usuario cliente: {ex.Message}");
            }
        }

        [HttpPost("employee")]
        public async Task<IActionResult> Register(EmployeeDTO employeeDTO)
        {
            try
            {
                var roleClaim = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "Role")?.Value;
                if (roleClaim != "admin")
                {
                    return Unauthorized("Acceso denegado. Se requiere permisos de administrador.");
                }

                var existingUser = _userRepository.GetUserByEmail(employeeDTO.Email);
                if (existingUser != null)
                {
                    return Conflict("El correo electrónico ya está registrado.");
                }

                var user = _mapper.Map<User>(employeeDTO);
                user.UserType = "employee";

                _userRepository.CreateUser(user, employeeDTO.Password);

                return Ok("Usuario empleado creado exitosamente.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error al registrar usuario empleado: {ex.Message}");
            }
        }

        [HttpPost("admin")]
        public async Task<IActionResult> Register(AdminDTO adminDTO)
        {
            try
            {
                var roleClaim = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "Role")?.Value;
                if (roleClaim != "admin")
                {
                    return Unauthorized("Acceso denegado. Se requiere permisos de administrador.");
                }

                var existingUser = _userRepository.GetUserByEmail(adminDTO.Email);
                if (existingUser != null)
                {
                    return Conflict("El correo electrónico ya está registrado.");
                }

                var user = _mapper.Map<User>(adminDTO);
                user.UserType = "admin";

                _userRepository.CreateUser(user, adminDTO.Password);

                return Ok("Usuario administrador creado exitosamente.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error al registrar usuario administrador: {ex.Message}");
            }
        }

        [HttpGet("GetAllUsers")]
        public async Task<ActionResult<IEnumerable<User>>> GetUsers()
        {
            try
            {
                var users = await _userRepository.GetAllUsers();
                if (users == null || !users.Any())
                {
                    return NotFound("No se encontraron usuarios.");
                }

                return Ok(users);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error al obtener usuarios: {ex.Message}");
            }
        }

        [HttpGet("GetUserById/{id}")]
        public async Task<ActionResult<User>> GetUserById(int id)
        {
            var user = await _userRepository.GetUserById(id);
            if (user == null)
            {
                return NotFound($"No se encontró el usuario con ID {id}.");
            }

            return Ok(user);
        }

        [HttpPut("updateEmployee/{id}")]
        public async Task<IActionResult> UpdateEmployee(int id, [FromBody] UpdateEmployeeDTO updateEmployeeDTO)
        {
            try
            {
                var roleClaim = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "Role")?.Value;
                if (roleClaim != "admin")
                {
                    return Unauthorized("Acceso denegado. Se requiere permisos de administrador.");
                }

                var user = await _userRepository.GetUserById(id);
                if (user == null)
                {
                    return NotFound("Usuario no encontrado.");
                }

                user.Name = updateEmployeeDTO.Name;
                user.Lastname = updateEmployeeDTO.Lastname;
                user.Phone = updateEmployeeDTO.Phone;
                user.IdBranch = updateEmployeeDTO.IdBranch;

                await _userRepository.UpdateUser(user);

                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error al actualizar empleado: {ex.Message}");
            }
        }

        [HttpPut("updateUser/{id}")]
        public async Task<IActionResult> UpdateUser(int id, [FromBody] UpdateUserDTO updateUserDTO)
        {
            

            try
            {
                var roleClaim = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "Role")?.Value;
                if (roleClaim != "admin" && roleClaim != "client")
                {
                    return Unauthorized("Acceso denegado. Se requiere permisos de administrador.");
                }
                var user = await _userRepository.GetUserById(id);
                if (user == null)
                {
                    return NotFound("Usuario no encontrado.");
                }
            
                user.Name = updateUserDTO.Name;
                user.Lastname = updateUserDTO.Lastname;
                user.Phone = updateUserDTO.Phone;

                await _userRepository.UpdateUser(user);

                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error al actualizar usuario: {ex.Message}");
            }
        }

        [HttpDelete("deleteUser/{id}")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            try
            {
                var roleClaim = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "Role")?.Value;
                if (roleClaim != "admin")
                {
                    return Unauthorized("Acceso denegado. Se requiere permisos de administrador.");
                }

                var success = await _userRepository.DeleteUser(id);
                if (!success)
                {
                    return NotFound("Usuario no encontrado.");
                }

                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error al eliminar usuario: {ex.Message}");
            }
        }
    }
}
