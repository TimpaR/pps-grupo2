﻿using detailingAPI.Entities;
using detailingAPI.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace detailingAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BranchesController : ControllerBase
    {
        private readonly IBranchService _branchService;

        public BranchesController(IBranchService branchService)
        {
            _branchService = branchService;
        }

        [HttpGet("getAllBranchs")]
        public async Task<ActionResult<IEnumerable<Branch>>> GetBranches()
        {
            try
            {
                var branches = await _branchService.GetAllBranches();
                return Ok(branches);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error al obtener las sucursales: {ex.Message}");
            }
        }

        [HttpGet("getBranch/{id}")]
        public async Task<ActionResult<Branch>> GetBranch(int id)
        {
            try
            {
                var branch = await _branchService.GetBranchById(id);
                if (branch == null)
                {
                    return NotFound($"No se encontró la sucursal con ID {id}");
                }
                return Ok(branch);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error al obtener la sucursal: {ex.Message}");
            }
        }

        [HttpPost("createBranch")]
        public async Task<ActionResult<Branch>> CreateBranch([FromBody] BranchCreateDTO branchCreateDTO)
        {
            try
            {
                var roleClaim = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "Role")?.Value;
                if (roleClaim != "admin")
                {
                    return Unauthorized("Acceso denegado. Se requieren permisos de administrador.");
                }

                var branch = new Branch
                {
                    Location = branchCreateDTO.Location,
                    Name = branchCreateDTO.Name,
                    Phone = branchCreateDTO.Phone
                };

                var createdBranch = await _branchService.CreateBranch(branch);
                return CreatedAtAction(nameof(GetBranch), new { id = createdBranch.IdBranch }, createdBranch);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error al crear la sucursal: {ex.Message}");
            }
        }

        [HttpPut("updateBranch/{id}")]
        public async Task<IActionResult> UpdateBranch(int id, [FromBody] UpdateBranchDTO updateBranchDTO)
        {
            try
            {
                var roleClaim = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "Role")?.Value;
                if (roleClaim != "admin")
                {
                    return Unauthorized("Acceso denegado. Se requieren permisos de administrador.");
                }

                var branch = await _branchService.GetBranchById(id);
                if (branch == null)
                {
                    return NotFound($"No se encontró la sucursal con ID {id}");
                }

                branch.Phone = updateBranchDTO.Phone;

                await _branchService.UpdateBranch(branch);
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error al actualizar la sucursal: {ex.Message}");
            }
        }

        [HttpDelete("deleteBranch/{id}")]
        public async Task<IActionResult> DeleteBranch(int id)
        {
            try
            {
                var roleClaim = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "Role")?.Value;
                if (roleClaim != "admin")
                {
                    return Unauthorized("Acceso denegado. Se requieren permisos de administrador.");
                }

                // Verificar si la sucursal tiene empleados
                var hasEmployees = await _branchService.BranchHasEmployees(id);
                if (hasEmployees)
                {
                    return BadRequest($"No se puede eliminar la sucursal con ID {id} porque tiene empleados asignados.");
                }

                var success = await _branchService.DeleteBranch(id);
                if (!success)
                {
                    return NotFound($"No se encontró la sucursal con ID {id}");
                }

                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error al eliminar la sucursal: {ex.Message}");
            }
        }
    }
}
