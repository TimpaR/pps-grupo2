﻿using detailingAPI.Entities;
using detailingAPI.Models;
using detailingAPI.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace detailingAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServiceController : ControllerBase
    {
        private readonly IServiceService _serviceService;

        public ServiceController(IServiceService serviceService)
        {
            _serviceService = serviceService;
        }

        [HttpGet("getAllServices")]
        public async Task<ActionResult<IEnumerable<Service>>> GetServices()
        {
            try
            {
                var services = await _serviceService.GetAllServices();
                if (services == null || !services.Any())
                {
                    return NotFound("No se encontraron servicios.");
                }

                return Ok(services);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error al obtener los servicios: {ex.Message}");
            }
        }

        [HttpGet("getService/{id}")]
        public async Task<ActionResult<Service>> GetService(int id)
        {
            try
            {
                var service = await _serviceService.GetServiceById(id);
                if (service == null)
                {
                    return NotFound("Servicio no encontrado");
                }

                return Ok(service);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error al obtener el servicio: {ex.Message}");
            }
        }

        [HttpPost("createService")]
        public async Task<ActionResult<Service>> CreateService([FromBody] ServiceCreateDTO serviceCreateDTO)
        {
            try
            {
                var roleClaim = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "Role")?.Value;
                if (roleClaim != "admin")
                {
                    return Unauthorized("Acceso denegado. Se requiere permisos de administrador.");
                }

                var service = new Service
                {
                    UrlImage = serviceCreateDTO.UrlImage,
                    Name = serviceCreateDTO.Name,
                    Description = serviceCreateDTO.Description,
                    Price = serviceCreateDTO.Price,
                };

                var createdService = await _serviceService.CreateService(service);
                if (createdService == null)
                {
                    return BadRequest("No se pudo crear el servicio.");
                }

                return Ok("Servicio creado exitosamente.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error al crear el servicio: {ex.Message}");
            }
        }

        [HttpPut("updateService/{id}")]
        public async Task<IActionResult> UpdateService(int id, [FromBody] UpdateServiceDTO updateServiceDTO)
        {
            try
            {
                var roleClaim = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "Role")?.Value;
                if (roleClaim != "admin")
                {
                    return Unauthorized("Acceso denegado. Se requiere permisos de administrador.");
                }

                var service = await _serviceService.GetServiceById(id);
                if (service == null)
                {
                    return NotFound("Servicio no encontrado");
                }

                service.UrlImage = updateServiceDTO.UrlImage;
                service.Description = updateServiceDTO.Description;
                service.Price = updateServiceDTO.Price;

                await _serviceService.UpdateService(service);
                return Ok("Servicio actualizado exitosamente");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error al actualizar el servicio: {ex.Message}");
            }
        }

        [HttpDelete("deleteService/{id}")]
        public async Task<IActionResult> DeleteService(int id)
        {
            try
            {
                var roleClaim = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "Role")?.Value;
                if (roleClaim != "admin")
                {
                    return Unauthorized("Acceso denegado. Se requiere permisos de administrador.");
                }

                var hasShiftsAssigned = await _serviceService.ServiceHasShifts(id);
                if (hasShiftsAssigned)
                {
                    return BadRequest("No se puede eliminar el servicio porque tiene turnos asignados.");
                }

                var success = await _serviceService.DeleteService(id);
                if (!success)
                {
                    return NotFound("Servicio no encontrado.");
                }

                return Ok("Servicio eliminado exitosamente.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error al eliminar el servicio: {ex.Message}");
            }
        }
    }
}
