﻿using detailingAPI.Entities;
using detailingAPI.Models;
using detailingAPI.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace detailingAPI.Controllers
{
    [Route("review")]
    [ApiController]
    public class ReviewController : ControllerBase
    {
        private readonly IReviewService _reviewService;

        public ReviewController(IReviewService reviewService)
        {
            _reviewService = reviewService;
        }

        [HttpGet("GetAll")]
        public async Task<ActionResult<IEnumerable<Review>>> GetReviews()
        {
            try
            {
                var reviews = await _reviewService.GetAllReviews();
                return Ok(reviews);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error al obtener las revisiones: {ex.Message}");
            }
        }

        [HttpGet("GetById/{id}")]
        public async Task<ActionResult<Review>> GetReview(int id)
        {
            try
            {
                var review = await _reviewService.GetReviewById(id);
                if (review == null)
                {
                    return NotFound($"No se encontró la revisión con ID {id}");
                }

                return Ok(review);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error al obtener la revisión: {ex.Message}");
            }
        }

        [HttpPost("createReview")]
        public async Task<ActionResult<Review>> CreateReview([FromBody] ReviewCreateDTO reviewCreateDTO)
        {
            try
            {
                var roleClaim = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "Role")?.Value;
                if (roleClaim != "client")
                {
                    return Unauthorized("Acceso denegado. Se requieren permisos de Cliente.");
                }

                var review = new Review
                {
                    IdClient = reviewCreateDTO.IdClient,
                    Rating = reviewCreateDTO.Rating,
                    Comment = reviewCreateDTO.Comment
                };

                var createdReview = await _reviewService.CreateReview(review);
                return CreatedAtAction(nameof(GetReview), new { id = createdReview.IdReview }, createdReview);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error al crear la revisión: {ex.Message}");
            }
        }

        [HttpDelete("deleteReview/{id}")]
        public async Task<IActionResult> DeleteReview(int id)
        {
            try
            {
                var roleClaim = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "Role")?.Value;
                if (roleClaim != "admin")
                {
                    return Unauthorized("Acceso denegado. Se requieren permisos de administrador.");
                }

                var success = await _reviewService.DeleteReview(id);
                if (!success)
                {
                    return NotFound($"No se encontró la revisión con ID {id}");
                }

                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error al eliminar la revisión: {ex.Message}");
            }
        }
    }
}
