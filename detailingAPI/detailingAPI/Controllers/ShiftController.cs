﻿using detailingAPI.Entities;
using detailingAPI.Models;
using detailingAPI.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace detailingAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShiftController : ControllerBase
    {
        private readonly IShiftService _shiftService;

        public ShiftController(IShiftService shiftService)
        {
            _shiftService = shiftService;
        }

        [HttpGet("getAllShift")]
        public async Task<ActionResult<IEnumerable<Shift>>> GetShifts()
        {
            try
            {
                var shifts = await _shiftService.GetAllShifts();
                if (shifts == null || !shifts.Any())
                {
                    return NotFound("No se encontraron turnos.");
                }

                return Ok(shifts);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error al obtener los turnos: {ex.Message}");
            }
        }

        [HttpGet("getShiftById/{id}")]
        public async Task<ActionResult<Shift>> GetShift(int id)
        {
            try
            {
                var shift = await _shiftService.GetShiftById(id);
                if (shift == null)
                {
                    return NotFound("Turno no encontrado");
                }

                return Ok(shift);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error al obtener el turno: {ex.Message}");
            }
        }
        [HttpGet("getShiftsByBranchId/{branchId}")]
        public async Task<ActionResult<IEnumerable<Shift>>> GetShiftsByBranchId(int branchId)
        {
            try
            {
                var shifts = await _shiftService.GetShiftsByBranchId(branchId);
                if (shifts == null || !shifts.Any())
                {
                    return NotFound($"No se encontraron turnos para la sucursal con ID {branchId}");
                }

                return Ok(shifts);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error al obtener los turnos de la sucursal: {ex.Message}");
            }
        }

        [HttpPost("createShift")]
        public async Task<ActionResult<Shift>> CreateShift([FromBody] ShiftCreateDTO shiftCreateDTO)
        {
            try
            {
                var roleClaim = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "Role")?.Value;
                if (roleClaim != "employee")
                {
                    return Unauthorized("Acceso denegado. Se requiere permisos de empleado.");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var shift = new Shift
                {
                    IdVehicle = shiftCreateDTO.IdVehicle,
                    IdBranch = shiftCreateDTO.IdBranch,
                    IdService = shiftCreateDTO.IdService,
                    Date = shiftCreateDTO.Date,
                    StartTime = shiftCreateDTO.StartTime,
                    EndTime = shiftCreateDTO.EndTime
                };

                var createdShift = await _shiftService.CreateShift(shift);
                return CreatedAtAction(nameof(GetShift), new { id = createdShift.IdShift }, createdShift);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error al crear el turno: {ex.Message}");
            }
        }

        [HttpPut("updateShift/{id}")]
        public async Task<IActionResult> UpdateShift(int id, [FromBody] UpdateShiftDTO updateShiftDTO)
        {
            try
            {
                var roleClaim = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "Role")?.Value;
                if (roleClaim != "employee" && roleClaim != "client")
                {
                    return Unauthorized("Acceso denegado. Se requiere permisos de empleado o cliente.");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var shift = await _shiftService.GetShiftById(id);
                if (shift == null)
                {
                    return NotFound("Turno no encontrado");
                }

                shift.IdVehicle = updateShiftDTO.IdVehicle;
                shift.IdBranch = updateShiftDTO.IdBranch;
                shift.IdService = updateShiftDTO.IdService;

                await _shiftService.UpdateShift(shift);
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error al actualizar el turno: {ex.Message}");
            }
        }

        [HttpDelete("deleteShift/{id}")]
        public async Task<IActionResult> DeleteShift(int id)
        {
            try
            {
                var roleClaim = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "Role")?.Value;
                if (roleClaim != "employee")
                {
                    return Unauthorized("Acceso denegado. Se requiere permisos de empleado.");
                }

                var success = await _shiftService.DeleteShift(id);
                if (!success)
                {
                    return NotFound("Turno no encontrado.");
                }

                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error al eliminar el turno: {ex.Message}");
            }
        }
    }
}
