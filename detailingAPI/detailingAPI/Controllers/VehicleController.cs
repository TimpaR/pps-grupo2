﻿using AutoMapper;
using detailingAPI.Entities;
using detailingAPI.Models;
using detailingAPI.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace detailingAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehicleController : ControllerBase
    {
        private readonly IVehicleService _vehicleService;
        private readonly IMapper _mapper;

        public VehicleController(IVehicleService vehicleService, IMapper mapper)
        {
            _vehicleService = vehicleService;
            _mapper = mapper;
        }

        [HttpGet("getAllVehicles")]
        public async Task<ActionResult<IEnumerable<Vehicle>>> GetVehicles()
        {
            try
            {
                var vehicles = await _vehicleService.GetAllVehicles();
                if (vehicles == null || !vehicles.Any())
                {
                    return NotFound("No se encontraron vehículos.");
                }

                return Ok(vehicles);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error al obtener todos los vehículos: {ex.Message}");
            }
        }

        [HttpGet("getVehicleById/{id}")]
        public ActionResult<Vehicle> GetVehicleById(int id)
        {
            var vehicle = _vehicleService.GetVehicleById(id);
            if (vehicle == null)
            {
                return NotFound("No se encontró el vehículo");
            }

            return Ok(vehicle);
        }

        [HttpGet("")]
        public ActionResult<IEnumerable<Vehicle>> GetVehiclesByUserId(int userId)
        {
            var vehicle = _vehicleService.GetVehiclesByUserId(userId);
            if (vehicle == null)
            {
                return Unauthorized("El usuario no tiene la reclamación 'IdUser'");
            }

            var vehicles = _vehicleService.GetVehiclesByUserId(userId);

            return Ok(vehicles);
        }

        [HttpPost("createVehicle")]
        public ActionResult AddVehicle([FromBody] VehicleDTO vehicleDto)
        {
            try
            {
                var roleClaim = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "Role")?.Value;
                if (roleClaim != "client")
                {
                    return Unauthorized("Acceso denegado. Se requiere permisos de cliente.");
                }

                var vehicle = _mapper.Map<Vehicle>(vehicleDto);
                _vehicleService.AddVehicle(vehicle);

                return Ok("Vehículo agregado exitosamente.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error al crear el vehículo: {ex.Message}");
            }
        }

        [HttpDelete("deleteVehicle/{id}")]
        public ActionResult DeleteVehicle(int id)
        {
            try
            {
                var roleClaim = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "Role")?.Value;
                if (roleClaim != "client" && roleClaim != "admin" && roleClaim != "employee")
                {
                    return Unauthorized("Acceso denegado. Se requiere permisos de cliente.");
                }

                _vehicleService.DeleteVehicle(id);

                return Ok("Vehículo eliminado exitosamente.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Error al eliminar el vehículo: {ex.Message}");
            }
        }
    }
}
