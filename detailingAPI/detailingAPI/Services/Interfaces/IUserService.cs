﻿using detailingAPI.Entities;

namespace detailingAPI.Services
{
    public interface IUserService
    {
        void CreateUser(User user, string password);
        Task<User> GetUserByEmailAsync(string email);
        Task<IEnumerable<User>> GetAllUsers();
        Task<User?> GetUserById(int id);
        Task<User> UpdateUser(User user);
        Task<bool> DeleteUser(int id);
    }
}
