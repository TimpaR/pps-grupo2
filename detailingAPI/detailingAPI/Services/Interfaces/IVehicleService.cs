﻿using detailingAPI.Entities;

namespace detailingAPI.Services.Interfaces
{
    public interface IVehicleService
    {
        Vehicle GetVehicleById(int vehicleId);
        IEnumerable<Vehicle> GetVehiclesByUserId(int userId);
        void AddVehicle(Vehicle vehicle);
        Task<IEnumerable<Vehicle>> GetAllVehicles();
        void DeleteVehicle(int id);
    }
}
