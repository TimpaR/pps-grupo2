﻿using detailingAPI.Entities;

namespace detailingAPI.Services.Interfaces
{
    public interface IServiceService
    {
        Task<IEnumerable<Service>> GetAllServices();
        Task<Service?> GetServiceById(int id);
        Task<Service> CreateService(Service service);
        Task<Service> UpdateService(Service service);
        Task<bool> DeleteService(int id);
        Task<bool> ServiceHasShifts(int serviceId);
    }
}
