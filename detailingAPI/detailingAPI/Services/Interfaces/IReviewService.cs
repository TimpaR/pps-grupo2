﻿using detailingAPI.Entities;

namespace detailingAPI.Services.Interfaces
{
    public interface IReviewService
    {
        Task<IEnumerable<Review>> GetAllReviews();
        Task<Review?> GetReviewById(int id);
        Task<Review> CreateReview(Review review);
        Task<bool> DeleteReview(int id);
    }
}
