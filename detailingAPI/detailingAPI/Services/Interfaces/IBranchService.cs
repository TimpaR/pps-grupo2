﻿using detailingAPI.Entities;

namespace detailingAPI.Services.Interfaces
{
    public interface IBranchService
    {
        Task<IEnumerable<Branch>> GetAllBranches();
        Task<Branch?> GetBranchById(int id);
        Task<Branch> CreateBranch(Branch branch);
        Task<Branch> UpdateBranch(Branch branch);
        Task<bool> DeleteBranch(int id);
        Task<bool> BranchHasEmployees(int branchId);
    }
}
