﻿namespace detailingAPI.Services
{
    public interface IAuthService
    {
        Task<bool> AuthenticateAsync(string email, string password);
    }
}
