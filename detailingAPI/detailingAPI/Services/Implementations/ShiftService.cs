﻿using detailingAPI.Entities;
using detailingAPI.Repositories.Interfaces;
using detailingAPI.Services.Interfaces;

namespace detailingAPI.Services.Implementations
{
    public class ShiftService : IShiftService, IShiftObservable
    {
        private readonly IShiftRepository _shiftRepository;
        private readonly List<IShiftObserver> _observadores = new List<IShiftObserver>();

        public ShiftService(IShiftRepository shiftRepository)
        {
            _shiftRepository = shiftRepository;
        }

        public async Task<IEnumerable<Shift>> GetAllShifts()
        {
            return await _shiftRepository.GetAllShifts();
        }

        public async Task<Shift?> GetShiftById(int id)
        {
            return await _shiftRepository.GetShiftById(id);
        }

        public async Task<Shift> CreateShift(Shift shift)
        {
            var createdShift = await _shiftRepository.CreateShift(shift);
            NotificarObservadores(createdShift, "creación");
            return createdShift;
        }

        public async Task<Shift> UpdateShift(Shift shift)
        {
            var updatedShift = await _shiftRepository.UpdateShift(shift);
            NotificarObservadores(updatedShift, "actualización");
            return updatedShift;
        }

        public async Task<bool> DeleteShift(int id)
        {
            var deleted = await _shiftRepository.DeleteShift(id);
            if (deleted)
            {
                var deletedShift = new Shift { IdShift = id };
                NotificarObservadores(deletedShift, "eliminación");
            }
            return deleted;
        }
        public async Task<IEnumerable<Shift>> GetShiftsByBranchId(int branchId)
        {
            return await _shiftRepository.GetShiftsByBranchId(branchId);
        }

        public void AgregarObservador(IShiftObserver observador)
        {
            _observadores.Add(observador);
        }

        public void EliminarObservador(IShiftObserver observador)
        {
            _observadores.Remove(observador);
        }

        public void NotificarObservadores(Shift shift, string tipoAccion)
        {
            foreach (var observador in _observadores)
            {
                observador.Actualizar(shift, tipoAccion);
            }
        }
    }
}
