﻿using detailingAPI.Entities;
using detailingAPI.Repositories;
using detailingAPI.Services.Interfaces;

namespace detailingAPI.Services.Implementations
{
    public class BranchService : IBranchService
    {
        private readonly IBranchRepository _branchRepository;

        public BranchService(IBranchRepository branchRepository)
        {
            _branchRepository = branchRepository;
        }

        public async Task<IEnumerable<Branch>> GetAllBranches()
        {
            return await _branchRepository.GetAllBranches();
        }

        public async Task<Branch?> GetBranchById(int id)
        {
            return await _branchRepository.GetBranchById(id);
        }

        public async Task<Branch> CreateBranch(Branch branch)
        {
            return await _branchRepository.CreateBranch(branch);
        }

        public async Task<Branch> UpdateBranch(Branch branch)
        {
            return await _branchRepository.UpdateBranch(branch);
        }

        public async Task<bool> DeleteBranch(int id)
        {
            return await _branchRepository.DeleteBranch(id);
        }

        public async Task<bool> BranchHasEmployees(int branchId)
        {
            return await _branchRepository.BranchHasEmployees(branchId);
        }
 
    }
}
