﻿using AutoMapper;
using detailingAPI.Services.Interfaces;
using detailingAPI.Entities;
using detailingAPI.Repositories.Interfaces;
using detailingAPI.DBContext;

namespace detailingAPI.Services.Implementations
{
    public class VehicleService : IVehicleService
    {
        private readonly IVehicleRepository _vehicleRepository;
        

        public VehicleService(IVehicleRepository vehicleRepository)
        {
            _vehicleRepository = vehicleRepository;
            
        }

        public Vehicle GetVehicleById(int id)
        {
            return _vehicleRepository.GetVehicleById(id);
            
        }

        public IEnumerable<Vehicle> GetVehiclesByUserId(int userId) 
        {
            return _vehicleRepository.GetVehiclesByUserID(userId);
            
        }

        public void AddVehicle(Vehicle vehicle) 
        {
            _vehicleRepository.AddVehicle(vehicle);
        }

        public async Task<IEnumerable<Vehicle>> GetAllVehicles()
        {
            return await _vehicleRepository.GetAllVehicles();
        }

        public void DeleteVehicle(int id) 
        {
            _vehicleRepository.DeleteVehicle(GetVehicleById(id));
        }
    }
}
