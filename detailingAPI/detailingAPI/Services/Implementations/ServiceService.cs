﻿using detailingAPI.Entities;
using detailingAPI.Repositories;
using detailingAPI.Repositories.Interfaces;
using detailingAPI.Services.Interfaces;

namespace detailingAPI.Services.Implementations
{
    public class ServiceService : IServiceService
    {
        private readonly IServiceRepository _serviceRepository;

        public ServiceService(IServiceRepository serviceRepository)
        {
            _serviceRepository = serviceRepository;
        }

        public async Task<IEnumerable<Service>> GetAllServices()
        {
            return await _serviceRepository.GetAllServices();
        }

        public async Task<Service?> GetServiceById(int id)
        {
            return await _serviceRepository.GetServiceById(id);
        }

        public async Task<Service> CreateService(Service service)
        {
            return await _serviceRepository.CreateService(service);
        }

        public async Task<Service> UpdateService(Service service)
        {
            return await _serviceRepository.UpdateService(service);
        }

        public async Task<bool> DeleteService(int id)
        {
            return await _serviceRepository.DeleteService(id);
        }
        public async Task<bool> ServiceHasShifts(int serviceId)
        {
            return await _serviceRepository.ServiceHasShifts(serviceId);
        }
    }
}
