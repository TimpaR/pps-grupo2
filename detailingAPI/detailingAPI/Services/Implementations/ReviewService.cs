﻿using detailingAPI.Entities;
using detailingAPI.Repositories.Interfaces;
using detailingAPI.Services.Interfaces;

namespace detailingAPI.Services.Implementations
{
    public class ReviewService : IReviewService
    {
        private readonly IReviewRepository _reviewRepository;

        public ReviewService (IReviewRepository reviewRepository)
        {
            _reviewRepository = reviewRepository;
        }

        public async Task<IEnumerable<Review>> GetAllReviews()
        {
            return await _reviewRepository.GetAllReviews();
        }

        public async Task<Review?> GetReviewById(int id)
        {
            return await _reviewRepository.GetReviewById(id);
        }

        public async Task<Review> CreateReview(Review review)
        {
            return await _reviewRepository.CreateReview(review);
        }

        public async Task<bool> DeleteReview(int id)
        {
            return await _reviewRepository.DeleteReview(id);
        }
    }
}
