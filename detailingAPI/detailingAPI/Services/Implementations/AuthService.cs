﻿using System.Threading.Tasks;
using detailingAPI.Repositories;

namespace detailingAPI.Services
{
    public class AuthService : IAuthService
    {
        private readonly IAuthRepository _authRepository;

        public AuthService(IAuthRepository authRepository)
        {
            _authRepository = authRepository;
        }

        public Task<bool> AuthenticateAsync(string email, string password)
        {
            return _authRepository.AuthenticateAsync(email, password);
        }
    }
}
