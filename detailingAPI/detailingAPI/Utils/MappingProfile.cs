﻿using AutoMapper;
using detailingAPI.DTOs;
using detailingAPI.Entities;
using detailingAPI.Models;

namespace detailingAPI.Profiles
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<ClientDTO, User>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Lastname, opt => opt.MapFrom(src => src.Lastname))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
                .ForMember(dest => dest.Password, opt => opt.MapFrom(src => src.Password));

            CreateMap<EmployeeDTO, User>()
               .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
               .ForMember(dest => dest.Lastname, opt => opt.MapFrom(src => src.Lastname))
               .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
               .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
               .ForMember(dest => dest.Password, opt => opt.MapFrom(src => src.Password));

            CreateMap<AdminDTO, User>()
               .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
               .ForMember(dest => dest.Lastname, opt => opt.MapFrom(src => src.Lastname))
               .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
               .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
               .ForMember(dest => dest.Password, opt => opt.MapFrom(src => src.Password));

            CreateMap<VehicleDTO, Vehicle>()
               .ForMember(dest => dest.CarPatent, opt => opt.MapFrom(src => src.CarPatent))
               .ForMember(dest => dest.Model, opt => opt.MapFrom(src => src.Model))
               .ForMember(dest => dest.Brand, opt => opt.MapFrom(src => src.Brand));


        }
    }
}
