﻿using detailingAPI.DBContext;
using detailingAPI.Entities;
using Microsoft.EntityFrameworkCore;

namespace detailingAPI.Repositories.Implementations
{
    public class BranchRepository : IBranchRepository
    {
        private readonly DetailingDbContext _detailingDbContext;
        public BranchRepository(DetailingDbContext detailingDbContext)
        {
            _detailingDbContext = detailingDbContext;
        }

        public async Task<IEnumerable<Branch>> GetAllBranches()
        {
            return await _detailingDbContext.Branches.ToListAsync();
        }
        public async Task<Branch?> GetBranchById(int id)
        {
            return await _detailingDbContext.Branches.FindAsync(id);
        }
        public async Task<Branch> CreateBranch(Branch branch)
        {
            _detailingDbContext.Branches.Add(branch);
            await _detailingDbContext.SaveChangesAsync();
            return branch;
        }
        public async Task<Branch> UpdateBranch(Branch branch)
        {
            _detailingDbContext.Entry(branch).State = EntityState.Modified;
            await _detailingDbContext.SaveChangesAsync();
            return branch;
        }
        public async Task<bool> DeleteBranch(int id)
        {
            var branch = await _detailingDbContext.Branches.FindAsync(id);
            if (branch != null)
            {
                _detailingDbContext.Branches.Remove(branch);
                await _detailingDbContext.SaveChangesAsync();
                return true;
            }
            return false;
        }

        public async Task<bool> BranchHasEmployees(int branchId)
        {
            var employeesCount = await _detailingDbContext.Users
                .CountAsync(u => u.IdBranch == branchId && u.UserType == "employee");

            return employeesCount > 0;
        }

    }
}
