﻿using detailingAPI.DBContext;
using detailingAPI.Entities;
using detailingAPI.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace detailingAPI.Repositories.Implementations
{
    public class ReviewRepository : IReviewRepository
    {
        private readonly DetailingDbContext _dbContext;

        public ReviewRepository(DetailingDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<Review>> GetAllReviews()
        {
            return await _dbContext.Reviews.ToListAsync();
        }

        public async Task<Review?> GetReviewById(int id)
        {
            return await _dbContext.Reviews.FindAsync(id);
        }

        public async Task<Review> CreateReview(Review review)
        {
            _dbContext.Reviews.Add(review);
            await _dbContext.SaveChangesAsync();
            return review;
        }

        public async Task<bool> DeleteReview(int id)
        {
            var review = await _dbContext.Reviews.FindAsync(id);
            if (review != null)
            {
                _dbContext.Reviews.Remove(review);
                await _dbContext.SaveChangesAsync();
                return true;
            }
            return false;
        }

    }
}
