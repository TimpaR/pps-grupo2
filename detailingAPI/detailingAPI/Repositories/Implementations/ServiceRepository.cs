﻿using detailingAPI.DBContext;
using detailingAPI.Entities;
using detailingAPI.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace detailingAPI.Repositories.Implementations
{
    public class ServiceRepository : IServiceRepository
    {
        private readonly DetailingDbContext _detailingDbContext;
        public ServiceRepository(DetailingDbContext detailingDbContext)
        {
            _detailingDbContext = detailingDbContext;
        }

        public async Task<IEnumerable<Service>> GetAllServices()
        {
            return await _detailingDbContext.Services.ToListAsync();
        }
        public async Task<Service?> GetServiceById(int id)
        {
            return await _detailingDbContext.Services.FindAsync(id);
        }
        public async Task<Service> CreateService(Service service)
        {
            _detailingDbContext.Services.Add(service);
            await _detailingDbContext.SaveChangesAsync();
            return service;
        }
        public async Task<Service> UpdateService(Service service)
        {
            _detailingDbContext.Entry(service).State = EntityState.Modified;
            await _detailingDbContext.SaveChangesAsync();
            return service;
        }
        public async Task<bool> DeleteService(int id)
        {
            var service = await _detailingDbContext.Services.FindAsync(id);
            if (service != null)
            {
                _detailingDbContext.Services.Remove(service);
                await _detailingDbContext.SaveChangesAsync();
                return true;
            }
            return false;
        }
        public async Task<bool> ServiceHasShifts(int serviceId)
        {
            var shiftsCount = await _detailingDbContext.Shifts
                .Where(s => s.IdService == serviceId)
                .CountAsync();

            return shiftsCount > 0;
        }
    }
}
