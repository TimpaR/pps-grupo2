﻿using detailingAPI.DBContext;
using detailingAPI.Entities;
using Microsoft.EntityFrameworkCore;

namespace detailingAPI.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly DetailingDbContext _dbContext;

        public UserRepository(DetailingDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void CreateUser(User user, string password)
        {
            user.Password = HashPassword(password);
            _dbContext.Users.Add(user);
            _dbContext.SaveChanges();
        }

        public async Task<User> GetUserByEmailAsync(string email)
        {
            return await _dbContext.Users.FirstOrDefaultAsync(u => u.Email == email);
        }

        private string HashPassword(string password)
        {
            return BCrypt.Net.BCrypt.HashPassword(password);
        }

        public User GetUserByEmail(string email)
        {
            return _dbContext.Users.FirstOrDefault(u => u.Email == email);
        }

        public async Task<IEnumerable<User>> GetAllUsers()
        {
            return await _dbContext.Users.ToListAsync();
        }

        public async Task<User?> GetUserById(int id)
        {
            return await _dbContext.Users.FindAsync(id);
        }

        public async Task<User> UpdateUser(User user)
        {
            _dbContext.Entry(user).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();
            return user;
        }

        public async Task<bool> DeleteUser(int id)
        {
            var user = await _dbContext.Users.FindAsync(id);
            if (user != null)
            {
                _dbContext.Users.Remove(user);
                await _dbContext.SaveChangesAsync();
                return true;
            }
            return false;
        }
    }
}
