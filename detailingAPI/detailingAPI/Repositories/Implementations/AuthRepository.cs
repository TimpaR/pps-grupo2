﻿using detailingAPI.DBContext;
using Microsoft.EntityFrameworkCore;

namespace detailingAPI.Repositories
{
    public class AuthRepository : IAuthRepository
    {
        private readonly DetailingDbContext _dbContext;

        public AuthRepository(DetailingDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<bool> AuthenticateAsync(string email, string password)
        {
            var user = await _dbContext.Users.FirstOrDefaultAsync(u => u.Email == email);

            if (user == null)
            {
                return false; // Usuario no encontrado
            }

            // Comparar la contraseña ingresada con la contraseña almacenada en la base de datos
            return BCrypt.Net.BCrypt.Verify(password, user.Password);
        }
    }
}
