﻿using detailingAPI.DBContext;
using detailingAPI.Entities;
using detailingAPI.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace detailingAPI.Repositories.Implementations
{
    public class ShiftRepository : IShiftRepository
    {
        private readonly DetailingDbContext _detailingDbContext;
        public ShiftRepository(DetailingDbContext detailingDbContext)
        {
            _detailingDbContext = detailingDbContext;
        }

        public async Task<IEnumerable<Shift>> GetAllShifts()
        {
            return await _detailingDbContext.Shifts.ToListAsync();
        }
        public async Task<Shift?> GetShiftById(int id)
        {
            return await _detailingDbContext.Shifts.FindAsync(id);
        }
        public async Task<Shift> CreateShift(Shift shift)
        {
            _detailingDbContext.Shifts.Add(shift);
            await _detailingDbContext.SaveChangesAsync();
            return shift;
        }
        public async Task<Shift> UpdateShift(Shift shift)
        {
            _detailingDbContext.Entry(shift).State = EntityState.Modified;
            await _detailingDbContext.SaveChangesAsync();
            return shift;
        }
        public async Task<bool> DeleteShift(int id)
        {
            var shift = await _detailingDbContext.Shifts.FindAsync(id);
            if (shift != null)
            {
                _detailingDbContext.Shifts.Remove(shift);
                await _detailingDbContext.SaveChangesAsync();
                return true;
            }
            return false;
        }
        public async Task<IEnumerable<Shift>> GetShiftsByBranchId(int branchId)
        {
            return await _detailingDbContext.Shifts
                .Where(s => s.IdBranch == branchId)
                .ToListAsync();
        }
    }
}
