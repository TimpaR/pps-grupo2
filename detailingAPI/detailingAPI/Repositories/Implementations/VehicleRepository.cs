﻿using detailingAPI.DBContext;
using detailingAPI.Repositories.Interfaces;
using detailingAPI.Entities;
using Microsoft.EntityFrameworkCore;


namespace detailingAPI.Repositories.Implementations
{
    public class VehicleRepository : IVehicleRepository
    {
        private readonly DetailingDbContext _dbContext;

        public VehicleRepository(DetailingDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Vehicle GetVehicleById(int id) 
        {
            return _dbContext.Vehicles.Find(id);
        }

        public IEnumerable<Vehicle> GetVehiclesByUserID(int userId)
        {
            return _dbContext.Vehicles.Where(v => v.IdClient == userId);
        }

        public void AddVehicle(Vehicle vehicle) 
        {
            _dbContext.Vehicles.Add(vehicle);
            _dbContext.SaveChanges();
        }

      

        public void DeleteVehicle(Vehicle vehicle)
        {
            _dbContext?.Vehicles.Remove(vehicle);
            _dbContext.SaveChanges();
        }
        public async Task<IEnumerable<Vehicle>> GetAllVehicles()
        {
            return await _dbContext.Vehicles.ToListAsync();
        }
    }
}
