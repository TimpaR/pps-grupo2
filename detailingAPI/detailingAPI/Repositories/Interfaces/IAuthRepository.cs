﻿using System.Threading.Tasks;

namespace detailingAPI.Repositories
{
    public interface IAuthRepository
    {
        Task<bool> AuthenticateAsync(string email, string password);
    }
}
