﻿using detailingAPI.DBContext;
using detailingAPI.Entities;

namespace detailingAPI.Repositories.Interfaces
{
    public interface IVehicleRepository
    {
        void AddVehicle(Vehicle vehicle);
        Task<IEnumerable<Vehicle>> GetAllVehicles();
        void DeleteVehicle(Vehicle vehicle);
        Vehicle GetVehicleById(int vehicleId);
        IEnumerable<Vehicle> GetVehiclesByUserID(int userId);

    }
}
