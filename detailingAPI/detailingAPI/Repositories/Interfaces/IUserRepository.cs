﻿using System.Threading.Tasks;
using detailingAPI.Entities;

namespace detailingAPI.Repositories
{
    public interface IUserRepository
    {
        void CreateUser(User user, string password);
        Task<User> GetUserByEmailAsync(string email);
        User GetUserByEmail(string email);
        Task<IEnumerable<User>> GetAllUsers();

        Task<User?> GetUserById(int id);
        Task<User> UpdateUser(User user);
        Task<bool> DeleteUser(int id);
    }
}
