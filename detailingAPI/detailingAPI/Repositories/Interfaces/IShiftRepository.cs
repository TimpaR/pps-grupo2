﻿using detailingAPI.Entities;

namespace detailingAPI.Repositories.Interfaces
{
    public interface IShiftRepository
    {
        Task<IEnumerable<Shift>> GetAllShifts();
        Task<Shift?> GetShiftById(int id);
        Task<Shift> CreateShift(Shift shift);
        Task<Shift> UpdateShift(Shift shift);
        Task<bool> DeleteShift(int id);
        Task<IEnumerable<Shift>> GetShiftsByBranchId(int branchId);
    }
}
