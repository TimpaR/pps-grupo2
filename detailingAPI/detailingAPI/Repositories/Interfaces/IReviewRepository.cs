﻿using detailingAPI.Entities;

namespace detailingAPI.Repositories.Interfaces
{
    public interface IReviewRepository
    {
        Task<IEnumerable<Review>> GetAllReviews();
        Task<Review?> GetReviewById(int id);
        Task<Review> CreateReview(Review review);
        Task<bool> DeleteReview(int id);
    }
}
