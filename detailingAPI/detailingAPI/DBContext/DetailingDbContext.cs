﻿using detailingAPI.Entities;
using Microsoft.EntityFrameworkCore;

namespace detailingAPI.DBContext;

public partial class DetailingDbContext : DbContext
{
    public DetailingDbContext()
    {
    }

    public DetailingDbContext(DbContextOptions<DetailingDbContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Branch> Branches { get; set; }

    public virtual DbSet<Review> Reviews { get; set; }

    public virtual DbSet<Service> Services { get; set; }

    public virtual DbSet<Shift> Shifts { get; set; }

    public virtual DbSet<User> Users { get; set; }

    public virtual DbSet<Vehicle> Vehicles { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see https://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseMySql("server=localhost;port=3306;database=detailing_db;uid=root;pwd=4370TomiR", Microsoft.EntityFrameworkCore.ServerVersion.Parse("8.0.36-mysql"));

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder
            .UseCollation("utf8mb4_0900_ai_ci")
            .HasCharSet("utf8mb4");

        modelBuilder.Entity<Branch>(entity =>
        {
            entity.HasKey(e => e.IdBranch).HasName("PRIMARY");

            entity.ToTable("branches");

            entity.Property(e => e.IdBranch).HasColumnName("id_branch");
            entity.Property(e => e.Location)
                .HasMaxLength(100)
                .HasColumnName("location");
            entity.Property(e => e.Name)
                .HasMaxLength(100)
                .HasColumnName("name");
            entity.Property(e => e.Phone)
                .HasMaxLength(20)
                .HasColumnName("phone");
        });

        modelBuilder.Entity<Review>(entity =>
        {
            entity.HasKey(e => e.IdReview).HasName("PRIMARY");

            entity.ToTable("reviews");

            entity.HasIndex(e => e.IdClient, "reviews_ibfk_1");

            entity.Property(e => e.IdReview).HasColumnName("id_review");
            entity.Property(e => e.Comment)
                .HasColumnType("text")
                .HasColumnName("comment");
            entity.Property(e => e.IdClient).HasColumnName("id_client");
            entity.Property(e => e.Rating).HasColumnName("rating");

            entity.HasOne(d => d.IdClientNavigation).WithMany(p => p.Reviews)
                .HasForeignKey(d => d.IdClient)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("reviews_ibfk_1");
        });

        modelBuilder.Entity<Service>(entity =>
        {
            entity.HasKey(e => e.IdServicio).HasName("PRIMARY");

            entity.ToTable("services");

            entity.Property(e => e.IdServicio).HasColumnName("id_servicio");
            entity.Property(e => e.Description)
                .HasColumnType("text")
                .HasColumnName("description");
            entity.Property(e => e.Name)
                .HasMaxLength(100)
                .HasColumnName("name");
            entity.Property(e => e.Price)
                .HasPrecision(10, 2)
                .HasColumnName("price");
            entity.Property(e => e.UrlImage)
                .HasMaxLength(255)
                .HasColumnName("url_image");
        });

        modelBuilder.Entity<Shift>(entity =>
        {
            entity.HasKey(e => e.IdShift).HasName("PRIMARY");

            entity.ToTable("shifts");

            entity.HasIndex(e => e.IdVehicle, "shifts_ibfk_1");

            entity.HasIndex(e => e.IdBranch, "shifts_ibfk_2");

            entity.HasIndex(e => e.IdService, "shifts_ibfk_3");

            entity.Property(e => e.IdShift).HasColumnName("id_shift");
            entity.Property(e => e.Date).HasColumnName("date");
            entity.Property(e => e.EndTime)
                .HasColumnType("time")
                .HasColumnName("end_time");
            entity.Property(e => e.IdBranch).HasColumnName("id_branch");
            entity.Property(e => e.IdService).HasColumnName("id_service");
            entity.Property(e => e.IdVehicle).HasColumnName("id_vehicle");
            entity.Property(e => e.StartTime)
                .HasColumnType("time")
                .HasColumnName("start_time");

            entity.HasOne(d => d.IdBranchNavigation).WithMany(p => p.Shifts)
                .HasForeignKey(d => d.IdBranch)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("shifts_ibfk_2");

            entity.HasOne(d => d.IdServiceNavigation).WithMany(p => p.Shifts)
                .HasForeignKey(d => d.IdService)
                .HasConstraintName("shifts_ibfk_3");

            entity.HasOne(d => d.IdVehicleNavigation).WithMany(p => p.Shifts)
                .HasForeignKey(d => d.IdVehicle)
                .HasConstraintName("shifts_ibfk_1");
        });

        modelBuilder.Entity<User>(entity =>
        {
            entity.HasKey(e => e.IdUser).HasName("PRIMARY");

            entity.ToTable("users");

            entity.HasIndex(e => e.IdBranch, "id_branch");

            entity.Property(e => e.IdUser).HasColumnName("id_user");
            entity.Property(e => e.Email)
                .HasMaxLength(100)
                .HasColumnName("email");
            entity.Property(e => e.IdBranch).HasColumnName("id_branch");
            entity.Property(e => e.Lastname)
                .HasMaxLength(50)
                .HasColumnName("lastname");
            entity.Property(e => e.Name)
                .HasMaxLength(50)
                .HasColumnName("name");
            entity.Property(e => e.Password)
                .HasMaxLength(100)
                .HasColumnName("password");
            entity.Property(e => e.Phone)
                .HasMaxLength(20)
                .HasColumnName("phone");
            entity.Property(e => e.UserType)
                .HasColumnType("enum('admin','employee','client')")
                .HasColumnName("user_type");

            entity.HasOne(d => d.IdBranchNavigation).WithMany(p => p.Users)
                .HasForeignKey(d => d.IdBranch)
                .HasConstraintName("users_ibfk_1");
        });

        modelBuilder.Entity<Vehicle>(entity =>
        {
            entity.HasKey(e => e.IdVehicle).HasName("PRIMARY");

            entity.ToTable("vehicles");

            entity.HasIndex(e => e.IdClient, "vehicles_ibfk_1");

            entity.Property(e => e.IdVehicle).HasColumnName("id_vehicle");
            entity.Property(e => e.Brand)
                .HasMaxLength(50)
                .HasColumnName("brand");
            entity.Property(e => e.CarPatent)
                .HasMaxLength(20)
                .HasColumnName("car_patent");
            entity.Property(e => e.IdClient).HasColumnName("id_client");
            entity.Property(e => e.Model)
                .HasMaxLength(50)
                .HasColumnName("model");

            entity.HasOne(d => d.IdClientNavigation).WithMany(p => p.Vehicles)
                .HasForeignKey(d => d.IdClient)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("vehicles_ibfk_1");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
