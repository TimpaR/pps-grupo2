import axios from "axios";
import Cookies from "js-cookie";

const instance = axios.create({
  baseURL: "https://localhost:7267",
  timeout: 10000,
  headers: {
    "Content-Type": "application/json",
  },
});

const getAuthToken = () => {
  return Cookies.get("authToken") || "";
};

instance.interceptors.request.use(
  (config) => {
    const authToken = getAuthToken();
    if (authToken) {
      config.headers.Authorization = `Bearer ${authToken}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default instance;
