import axios from "axios";
import instance from "../axiosInstance/AxiosInstance";

//LOGIN API
export const loginUser = async (formData) => {
  try {
    const response = await axios.post(
      "https://localhost:7267/api/Login",
      formData
    );
    return response.data;
  } catch (error) {
    if (error.response) {
      throw error.response.data;
    } else if (error.request) {
      throw new Error("No response from server");
    } else {
      throw new Error("Error in request setup");
    }
  }
};

//REGISTER API
export const registerUser = async (formData) => {
  try {
    const response = await axios.post(
      "https://localhost:7267/api/User/client",
      formData
    );
    return response.data;
  } catch (error) {
    if (error.response) {
      throw error.response.data;
    } else if (error.request) {
      throw new Error("No response from server");
    } else {
      throw new Error("Error in request setup");
    }
  }
};

// ADD SUCURSAL API
export const createBranch = async (values) => {
  try {
    const response = await instance.post("/api/Branches/createBranch", values);
    return response.data;
  } catch (error) {
    console.error("Error al enviar la sucursal:", error);
    throw error;
  }
};

// UPDATE SUCURSAL API
export const updateBranchAPI = async (idBranch, phone) => {
  try {
    const response = await instance.put(
      `/api/Branches/updateBranch/${idBranch}`,
      { phone }
    );
    return response.data;
  } catch (error) {
    throw new Error("Error al actualizar la sucursal");
  }
};

// DELETE SUCURSAL API
export const deleteBranchAPI = async (branchId) => {
  try {
    const response = await instance.delete(
      `/api/Branches/deleteBranch/${branchId}`
    );
    return response.data;
  } catch (error) {
    throw new Error("Error al eliminar la sucursal");
  }
};

// ADD SERVICIO API
export const createService = async (values) => {
  try {
    const response = await instance.post("/api/Service/createService", values);
    return response.data;
  } catch (error) {
    console.error("Error al enviar el servicio:", error);
    throw error;
  }
};

// UPDATE SERVICIO API
export const updateServiceAPI = async (
  idServicio,
  urlImage,
  description,
  price
) => {
  try {
    const response = await instance.put(
      `/api/Service/updateService/${idServicio}`,
      { urlImage, description, price }
    );
    return response.data;
  } catch (error) {
    throw new Error("Error al actualizar el servicio");
  }
};

// DELETE SERVICIO API
export const deleteServiceAPI = async (serviceId) => {
  try {
    const response = await instance.delete(
      `/api/Service/deleteService/${serviceId}`
    );
    return response.data;
  } catch (error) {
    throw new Error("Error al eliminar el servicio");
  }
};

// CREATE RESEÑA API
export const createReviewAPI = async (values) => {
  try {
    const response = await instance.post("/review/createReview", values);
    return response.data;
  } catch (error) {
    console.error("Error al enviar la reseña:", error);
    throw error;
  }
};

// DELETE RESEÑA API
export const deleteReviewAPI = async (idReview) => {
  try {
    const response = await instance.delete(`/review/deleteReview/${idReview}`);
    return response.data;
  } catch (error) {
    throw new Error("Error al eliminar la reseña");
  }
};

//ADD TURNO
export const createShift = async (values) => {
  try {
    const response = await instance.post(`/api/Shift/createShift`, values);
    return response.data;
  } catch (error) {
    throw new Error("Error al agregar el turno");
  }
};

//UPDATE TURNO
export const updateShiftAPI = async (
  idShift,
  idVehicle,
  idBranch,
  idService
) => {
  try {
    const response = await instance.put(`/api/Shift/updateShift/${idShift}`, {
      idVehicle,
      idBranch,
      idService,
    });
    return response.data;
  } catch (error) {
    throw new Error("Error al actualizar el turno");
  }
};

//DELETE TURNO
export const deleteShiftAPI = async (idShift) => {
  try {
    const response = await instance.delete(`/api/Shift/deleteShift/${idShift}`);
    return response.data;
  } catch (error) {
    throw new Error("Error al eliminar el turno");
  }
};

// CREATE VEHICULO API
export const createVehicle = async (values) => {
  try {
    const response = await instance.post("/api/Vehicle/createVehicle", values);
    return response.data;
  } catch (error) {
    console.error("Error al enviar el vehículo:", error);
    throw error;
  }
};

// DELETE VEHICULO API
export const deleteVehicleAPI = async (idVehicle) => {
  try {
    const response = await instance.delete(
      `/api/Vehicle/deleteVehicle/${idVehicle}`
    );
    return response.data;
  } catch (error) {
    throw new Error("Error al eliminar el vehículo");
  }
};

//OBTENER USUARIOS
export const fetchClients = async () => {
  return axios.get("https://localhost:7267/register/GetAllUsers");
};

//CARGAR SUCURSALES
export const fetchBranches = async () => {
  try {
    const response = await axios.get(
      "https://localhost:7267/api/Branches/getAllBranchs"
    );
    return response.data;
  } catch (error) {
    console.error("Error al obtener sucursales:", error);
    return [];
  }
};

//CARGAR USUARIOS
export const fetchUsers = async () => {
  try {
    const response = await axios.get(
      "https://localhost:7267/api/User/GetAllUsers"
    );
    return response.data;
  } catch (error) {
    console.error("Error al obtener usuarios:", error);
    return [];
  }
};

//ADD EMPLOYEE
export const addEmployee = async (values) => {
  try {
    const response = await instance.post("/api/User/employee", values);
    return response.data;
  } catch (error) {
    console.error("Error al añadir empleado:", error);
    return null;
  }
};

//DELETE USER
export const deleteUser = async (idUser) => {
  try {
    const response = await instance.delete(`/api/User/deleteUser/${idUser}`);
    return response.data;
  } catch (error) {
    console.error("Error al eliminar usuario:", error);
    throw error;
  }
};
