import axios from "axios";
import * as Yup from "yup";

export const loginValidation = Yup.object().shape({
  email: Yup.string()
    .email("Ingrese un email válido*")
    .required("El email es obligatorio*"),
  password: Yup.string().required("La contraseña es obligatoria*"),
});

export const registerValidation = Yup.object().shape({
  name: Yup.string()
    .matches(/^[a-zA-Z]+$/, "El nombre solo puede contener letras*")
    .min(2, "El nombre debe tener al menos 2 caracteres*")
    .max(50, "El nombre no puede tener más de 50 caracteres*")
    .required("El nombre es requerido*"),
  lastname: Yup.string()
    .matches(/^[a-zA-Z]+$/, "El apellido solo puede contener letras*")
    .min(2, "El apellido debe tener al menos 2 caracteres*")
    .max(50, "El apellido no puede tener más de 50 caracteres*")
    .required("El apellido es requerido*"),
  email: Yup.string()
    .email("Email inválido*")
    .required("El email es requerido*"),
  phone: Yup.string()
    .matches(/^\d{10}$/, "El teléfono debe tener 10 dígitos*")
    .required("El teléfono es requerido*"),
  password: Yup.string()
    .required("La contraseña es requerida*")
    .min(6, "La contraseña debe tener al menos 6 caracteres*")
    .max(50, "La contraseña no puede tener más de 50 caracteres*")
    .matches(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}$/,
      "La contraseña debe contener al menos una letra mayúscula, una letra minúscula, un número y un carácter especial*"
    ),
});

export const branchValidationSchema = Yup.object().shape({
  name: Yup.string()
    .min(2, "El nombre debe tener al menos 2 caracteres")
    .max(50, "El nombre no puede tener más de 50 caracteres")
    .required("El nombre es obligatorio"),
  location: Yup.string()
    .min(2, "La ubicación debe tener al menos 2 caracteres")
    .max(100, "La ubicación no puede tener más de 100 caracteres")
    .required("La ubicación es obligatoria"),
  phone: Yup.string()
    .matches(/^[0-9]+$/, "El teléfono debe contener solo números")
    .min(10, "El teléfono debe tener al menos 10 dígitos")
    .max(15, "El teléfono no puede tener más de 15 dígitos")
    .required("El teléfono es obligatorio"),
});

export const branchUpdateValidationSchema = Yup.object().shape({
  phone: Yup.string()
    .required("El teléfono es requerido")
    .matches(/^[0-9]+$/, "El teléfono debe contener solo números")
    .min(10, "El teléfono debe tener al menos 10 dígitos"),
});

export const updateServiceSchema = Yup.object().shape({
  description: Yup.string().required("La descripción es requerida"),
  price: Yup.number()
    .required("El precio es requerido")
    .min(0, "El precio debe ser mayor o igual a 0"),
});

export const employeeValidationSchema = Yup.object({
  name: Yup.string()
    .min(2, "El nombre debe tener al menos 2 caracteres")
    .max(50, "El nombre no puede tener más de 50 caracteres")
    .required("El nombre es obligatorio"),
  lastName: Yup.string()
    .min(2, "El apellido debe tener al menos 2 caracteres")
    .max(50, "El apellido no puede tener más de 50 caracteres")
    .required("El apellido es obligatorio"),
  email: Yup.string()
    .email("Correo electrónico inválido")
    .test(
      "unique-email",
      "Este correo electrónico ya está registrado",
      async function (value) {
        if (!value) return true;

        try {
          const response = await axios.get(
            "https://localhost:7267/api/User/GetAllUsers"
          );
          const users = response.data;
          const userWithEmail = users.find((user) => user.email === value);
          return !userWithEmail;
        } catch (error) {
          console.error("Error al obtener usuarios:", error);
          return true;
        }
      }
    )
    .required("El correo electrónico es obligatorio"),
  phone: Yup.string()
    .matches(/^[0-9]+$/, "El teléfono debe contener solo números")
    .min(10, "El teléfono debe tener al menos 10 dígitos")
    .max(15, "El teléfono no puede tener más de 15 dígitos")
    .required("El teléfono es obligatorio"),
  password: Yup.string()
    .min(8, "La contraseña debe tener al menos 8 caracteres")
    .matches(/[0-9]/, "La contraseña debe contener al menos un número")
    .required("La contraseña es obligatoria"),
  idBranch: Yup.string().required("La sucursal es obligatoria"),
});
export const reviewValidationSchema = Yup.object().shape({
  rating: Yup.number()
    .min(1, "Debe seleccionar una calificación")
    .required("Debe seleccionar una calificación"),
  comment: Yup.string().max(200, "No puede tener mas de 200 caracteres"),
});

export const vehicleValidationSchema = Yup.object().shape({
  carPatent: Yup.string()
    .min(6, "La patente debe tener al menos 6 caracteres")
    .max(7, "La patente no puede tener más de 7 caracteres")
    .required("La patente es obligatoria"),
  brand: Yup.string()
    .min(2, "La marca debe tener al menos 2 caracteres")
    .max(20, "La marca no puede tener más de 20 caracteres")
    .required("La marca es obligatoria"),
  model: Yup.string()
    .min(2, "El modelo debe tener al menos 2 caracteres")
    .max(20, "El modelo no puede tener más de 20 caracteres")
    .required("El modelo es obligatorio"),
  idClient: Yup.number().required("El ID de cliente es obligatorio"),
});

export const updateValidationSchema = Yup.object().shape({
  name: Yup.string()
    .min(2, "El nombre debe tener al menos 2 caracteres")
    .max(50, "El nombre no puede tener más de 50 caracteres")
    .required("El nombre es obligatorio"),
  lastName: Yup.string()
    .min(2, "El apellido debe tener al menos 2 caracteres")
    .max(50, "El apellido no puede tener más de 50 caracteres")
    .required("El apellido es obligatorio"),
  phone: Yup.string()
    .matches(/^[0-9]+$/, "El teléfono debe contener solo números")
    .min(10, "El teléfono debe tener al menos 10 dígitos")
    .max(15, "El teléfono no puede tener más de 15 dígitos")
    .required("El teléfono es obligatorio"),
  idBranch: Yup.string().when("userType", {
    is: "employee",
    then: Yup.string().required("La sucursal es obligatoria"),
  }),
});
