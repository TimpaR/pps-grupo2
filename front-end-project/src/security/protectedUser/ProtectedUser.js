import React, { useContext } from "react";
import { Navigate } from "react-router-dom";
import { AuthContext } from "../../services/authContext/AuthContext";

const ProtectedUser = ({ children }) => {
  const { user } = useContext(AuthContext);

  if (!user) {
    return <Navigate to="/" replace />;
  } else return children;
};

export default ProtectedUser;
