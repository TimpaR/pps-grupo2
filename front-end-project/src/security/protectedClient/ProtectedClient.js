import React from "react";
import { Navigate } from "react-router-dom";
import { useAuth } from "../../services/authContext/AuthContext";

const ProtectedEmployee = ({ children }) => {
  const { user } = useAuth();
  const userType = user ? user.userType : null;

  if (userType === "client") {
    return children;
  } else return <Navigate to="/permissionDenied" replace />;
};

export default ProtectedEmployee;
