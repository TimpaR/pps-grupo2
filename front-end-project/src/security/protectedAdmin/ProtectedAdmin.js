import React from "react";
import { Navigate } from "react-router-dom";
import { useAuth } from "../../services/authContext/AuthContext";

const ProtectedAdmin = ({ children }) => {
  const { user } = useAuth();
  const userType = user ? user.userType : null;

  if (userType === "admin") {
    return children;
  } else return <Navigate to="/permissionDenied" replace />;
};

export default ProtectedAdmin;
