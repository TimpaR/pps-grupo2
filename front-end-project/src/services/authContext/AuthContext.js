import { createContext, useContext, useEffect, useState } from "react";
import { jwtDecode } from "jwt-decode";
import Cookies from "js-cookie";

const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const [authToken, setAuthToken] = useState(
    () => Cookies.get("authToken") || ""
  );
  const [user, setUser] = useState(() => {
    const storedUser = Cookies.get("user");
    return storedUser ? JSON.parse(storedUser) : null;
  });

  useEffect(() => {
    const token = Cookies.get("authToken");
    if (token) {
      const decodedToken = jwtDecode(token);
      const idUser = decodedToken["idUser"];
      const idBranch = decodedToken["idBranch"];
      const name = decodedToken["Name"];
      const userType = decodedToken["Role"];

      setUser({
        idUser,
        idBranch,
        name,
        userType,
      });
    }
  }, []);

  const login = (token) => {
    setAuthToken(token);
    Cookies.set("authToken", token);
    const decodedToken = jwtDecode(token);
    const idUser = decodedToken["idUser"];
    const idBranch = decodedToken["idBranch"];
    const name = decodedToken["Name"];
    const userType = decodedToken["Role"];

    setUser({
      idUser,
      idBranch,
      name,
      userType,
    });
    Cookies.set("user", JSON.stringify({ name, userType }));
    if (userType === "admin") {
      window.location.href = "/adm";
    } else if (userType === "employee") {
      window.location.href = "/emp";
    }
  };

  const logout = () => {
    window.location.href = "/";
    setAuthToken("");
    setUser(null);
    Cookies.remove("authToken");
    Cookies.remove("user");
  };

  return (
    <AuthContext.Provider value={{ authToken, user, login, logout }}>
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => useContext(AuthContext);
