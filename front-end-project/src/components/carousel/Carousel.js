import React, { useEffect, useState } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import axios from "axios";
import { useAuth } from "../../services/authContext/AuthContext";

const Carousel = () => {
  const [services, setServices] = useState([]);
  const { user } = useAuth();

  useEffect(() => {
    const fetchServices = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7267/api/Service/getAllServices"
        );
        setServices(response.data);
      } catch (error) {
        console.error("Error fetching services:", error);
      }
    };

    fetchServices();
  }, []);

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    arrows: false,
    slidesToShow: 4,
    slidesToScroll: 1,
    centerMode: true,
    centerPadding: "0",
    responsive: [
      {
        breakpoint: 1300,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
          centerMode: true,
          centerPadding: "0",
        },
      },
      {
        breakpoint: 966,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          initialSlide: 2,
          centerMode: true,
          centerPadding: "0",
        },
      },
      {
        breakpoint: 660,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: true,
          centerPadding: "0",
        },
      },
    ],
  };

  return (
    <div className="w-full max-w-screen-xl m-auto pb-14 ">
      <div className="mt-20">
        <Slider {...settings} className="grid grid-cols-4 gap-4 ">
          {services.map((service, index) => (
            <div
              key={index}
              className="bg-slate-200 dark:bg-slate-700 dark:text-slate-400 text-black rounded-xl flex flex-col h-[420px] overflow-hidden"
            >
              <div className="h-56 w-full bg-slate-200 dark:bg-slate-700 flex justify-center items-center rounded-t-xl">
                {service.urlImage ? (
                  <img
                    src={service.urlImage}
                    alt={service.name}
                    className="h-full w-full object-cover rounded-t-xl"
                    onError={(e) => {
                      console.error(`Error loading image: ${service.urlImage}`);
                      e.target.src =
                        "https://via.placeholder.com/150?text=No+image+available";
                    }}
                  />
                ) : (
                  <div className="text-red-500">No image available</div>
                )}
              </div>
              <div className="flex flex-col flex-grow justify-between p-4 text-center">
                <div className="flex-grow">
                  <p className="text-xl font-semibold dark:bg-slate-700 dark:text-slate-400">
                    {service.name}
                  </p>
                </div>
                <div className="flex-grow">
                  <p className="text-black text-justify dark:bg-slate-700 dark:text-slate-400">
                    {service.description}
                  </p>
                </div>
                {user && (
                  <div className="mt-auto">
                    <p className="text-lg font-semibold text-green-500">
                      Precio: ${service.price}
                    </p>
                  </div>
                )}
              </div>
            </div>
          ))}
        </Slider>
      </div>
    </div>
  );
};

export default Carousel;
