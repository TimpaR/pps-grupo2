import React, { useEffect, useState } from "react";
import axios from "axios";
import { useAuth } from "../../services/authContext/AuthContext";
import { AiFillDelete } from "react-icons/ai";
import DeleteMyShift from "../deleteMyShift/DeleteMyShift";

const MyShift = () => {
  const { user } = useAuth();
  const [shifts, setShifts] = useState([]);
  const [services, setServices] = useState([]);
  const [branches, setBranches] = useState([]);
  const [vehicles, setVehicles] = useState([]);
  const [selectedShift, setSelectedShift] = useState(null);
  const [isConfirmModalOpen, setIsConfirmModalOpen] = useState(false);

  const fetchShifts = async () => {
    try {
      const response = await axios.get(
        "https://localhost:7267/api/Shift/getAllShift"
      );
      setShifts(response.data);
    } catch (error) {
      console.error("Error al obtener los turnos:", error);
    }
  };

  useEffect(() => {
    fetchShifts();
  }, []);

  useEffect(() => {
    const fetchBranches = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7267/api/Branches/getAllBranchs"
        );
        setBranches(response.data);
      } catch (error) {
        console.error("Error al obtener sucursales:", error);
      }
    };

    fetchBranches();
  }, []);

  useEffect(() => {
    const fetchServices = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7267/api/Service/getAllServices"
        );
        setServices(response.data);
      } catch (error) {
        console.error("Error fetching services:", error);
      }
    };

    fetchServices();
  }, []);

  useEffect(() => {
    const fetchVehicles = async () => {
      try {
        const response = await axios.get(
          `https://localhost:7267/api/Vehicle?userId=${user.idUser}`
        );
        setVehicles(response.data);
      } catch (error) {
        console.error("Error al obtener los vehiculos:", error);
      }
    };

    fetchVehicles();
  }, [user]);

  const formatDate = (dateString) => {
    const date = new Date(dateString);
    const options = { weekday: "long", day: "numeric", month: "long" };
    const formattedDate = date.toLocaleDateString("es-ES", options);

    return formattedDate.replace(/\b\w/g, (char) => char.toUpperCase());
  };

  const formatTime = (timeString) => {
    const [hours, minutes] = timeString.split(":");
    return `${hours}:${minutes} hs`;
  };

  const getServiceName = (id) => {
    const service = services.find((service) => service.idServicio === id);
    return service ? service.name : "N/A";
  };

  const getBranchName = (id) => {
    const branch = branches.find((branch) => branch.idBranch === id);
    return branch ? branch.location : "N/A";
  };

  const handleEditClick = (shift) => {
    setSelectedShift(shift);
    setIsConfirmModalOpen(true);
  };

  const handleUpdate = async () => {
    fetchShifts();
  };

  const handleConfirm = async () => {
    try {
      await axios.put(
        `https://localhost:7267/api/Shift/updateShift/${selectedShift.id}`,
        {
          ...selectedShift,
          idVehicle: null,
        }
      );
      handleUpdate();
      setIsConfirmModalOpen(false);
    } catch (error) {
      console.error("Error al actualizar el turno:", error);
    }
  };

  // Filtrar los turnos por los vehículos del usuario logueado
  const userVehicleIds = vehicles.map((vehicle) => vehicle.idVehicle);
  const today = new Date();
  const filteredShifts = shifts.filter(
    (shift) =>
      userVehicleIds.includes(shift.idVehicle) && new Date(shift.date) >= today
  );

  return (
    <div className="bg-gray-100 min-h-screen p-8 dark:bg-slate-800">
      <div className="grid gap-6 md:grid-cols-2 lg:grid-cols-3">
        {filteredShifts.length > 0 ? (
          filteredShifts.map((shift) => (
            <div
              key={shift.idShift}
              className="p-6 bg-white shadow-md rounded-lg"
            >
              <div className="flex items-center justify-between">
                <div className="flex items-center gap-4">
                  <CalendarIcon className="w-6 h-6 text-blue-500" />
                  <div>
                    <h3 className="text-lg font-semibold">
                      {getServiceName(shift.idService)}
                    </h3>
                    <p className="text-gray-500">{formatDate(shift.date)}</p>
                    <p className="text-gray-500">
                      <strong>
                        {formatTime(shift.startTime)} -{" "}
                        {formatTime(shift.endTime)}
                      </strong>
                    </p>
                  </div>
                </div>
                <div className="text-gray-500">
                  <p>{getBranchName(shift.idBranch)}</p>
                </div>
                <AiFillDelete
                  className="text-red-500 cursor-pointer"
                  onClick={() => handleEditClick(shift)}
                />
              </div>
            </div>
          ))
        ) : (
          <p className="text-center text-gray-700 text-lg dark:text-slate-300">
            <strong>No tienes proximos turnos</strong>
          </p>
        )}
      </div>
      {selectedShift && (
        <DeleteMyShift
          isOpen={isConfirmModalOpen}
          onClose={() => setIsConfirmModalOpen(false)}
          onConfirm={handleConfirm}
          shift={selectedShift}
        />
      )}
    </div>
  );
};

const CalendarIcon = (props) => {
  return (
    <svg
      {...props}
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    >
      <path d="M8 2v4" />
      <path d="M16 2v4" />
      <rect width="18" height="18" x="3" y="4" rx="2" />
      <path d="M3 10h18" />
    </svg>
  );
};

export default MyShift;
