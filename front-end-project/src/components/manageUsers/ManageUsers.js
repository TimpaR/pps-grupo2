import React, { useState } from "react";
import UsersList from "../usersList/UsersList";
import AddEmployee from "../addEmployee/AddEmployee";

const ManageUsers = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [filter, setFilter] = useState("all"); //Filtro

  const openModal = () => {
    setIsModalOpen(true);
  };

  //Filtro

  const getNoUsersMessage = () => {
    switch (filter) {
      case "employee":
        return "No hay empleados cargados.";
      case "admin":
        return "No hay administradores cargados.";
      case "client":
        return "No hay clientes cargados.";
      default:
        return "No hay usuarios cargados.";
    }
  };

  const handleFilterChange = (event) => {
    setFilter(event.target.value);
  };

  return (
    <div className="w-full h-full flex flex-col">
      <div className="mb-4 flex justify-between items-center">
        <h2 className="text-2xl font-bold text-gray-800  dark:text-slate-300">
          Listado de usuarios
        </h2>
        <button
          onClick={openModal}
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none"
        >
          Añadir Empleado
        </button>
      </div>
      {/* Filtro I*/}
      <div className="w-full p-4">
        <label
          htmlFor="userType"
          className="block text-gray-700  dark:text-slate-300"
        >
          Filtrar por tipo de usuario:
        </label>
        <select
          id="userType"
          value={filter}
          onChange={handleFilterChange}
          className="mt-1 block w-full p-2 bg-white border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm dark:bg-gray-900 dark:text-slate-400"
        >
          <option value="all">Todos</option>
          <option value="admin">Administrador</option>
          <option value="employee">Empleado</option>
          <option value="client">Cliente</option>
        </select>
      </div>
      {/* Filtro F*/}
      <div className="flex-1 overflow-auto">
        <UsersList filter={filter} getNoUsersMessage={getNoUsersMessage} />
      </div>
      {isModalOpen && (
        <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50">
          <AddEmployee
            isModalOpen={isModalOpen}
            setIsModalOpen={setIsModalOpen}
          />
        </div>
      )}
    </div>
  );
};

export default ManageUsers;
