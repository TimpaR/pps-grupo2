import React, { useState, useEffect } from "react";
import axios from "axios";
import { AiFillDelete, AiFillEdit } from "react-icons/ai";
import UpdateBranch from "../updateBranch/UpdateBranch";
import DeleteBranch from "../deleteBranch/DeleteBranch";

const BranchList = () => {
  const [branches, setBranches] = useState([]);
  const [selectedBranch, setSelectedBranch] = useState(null);
  const [isUpdateModalOpen, setIsUpdateModalOpen] = useState(false);
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);

  useEffect(() => {
    const fetchBranches = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7267/api/Branches/getAllBranchs"
        );
        setBranches(response.data);
      } catch (error) {
        console.error("Error al obtener sucursales:", error);
      }
    };

    fetchBranches();
  }, []);

  const handleEditClick = (branch) => {
    setSelectedBranch(branch);
    setIsUpdateModalOpen(true);
  };

  const handleDeleteClick = (branch) => {
    setSelectedBranch(branch);
    setIsDeleteModalOpen(true);
  };

  const handleUpdate = async () => {
    try {
      const response = await axios.get(
        "https://localhost:7267/api/Branches/getAllBranchs"
      );
      setBranches(response.data);
    } catch (error) {
      console.error("Error al obtener sucursales:", error);
    }
  };

  const handleDelete = async () => {
    try {
      const response = await axios.get(
        "https://localhost:7267/api/Branches/getAllBranchs"
      );
      setBranches(response.data);
    } catch (error) {
      console.error("Error al obtener sucursales:", error);
    }
  };

  return (
    <div className="flex justify-center items-center">
      <div className="w-full p-4 overflow-y-auto bg-gray-100 rounded-lg shadow-lg dark:bg-gray-900 ">
        {branches.length === 0 ? (
          <p className="text-center text-gray-500  dark:text-slate-300">
            No hay sucursales cargadas.
          </p>
        ) : (
          branches.map((branch) => (
            <div
              key={branch.idBranch}
              className="bg-white rounded-lg shadow-md p-4 mb-4 flex justify-between items-end dark:bg-slate-700 "
            >
              <div className=" dark:text-slate-300">
                <h3 className="text-lg font-semibold">{branch.name}</h3>
                <p className="text-sm">
                  <strong>Ubicación:</strong> {branch.location}
                </p>
                <p className="text-sm">
                  <strong>Teléfono:</strong> {branch.phone}
                </p>
              </div>
              <div className="flex space-x-4">
                <AiFillEdit
                  className="text-blue-500 cursor-pointer"
                  onClick={() => handleEditClick(branch)}
                />
                <AiFillDelete
                  className="text-red-500 cursor-pointer"
                  onClick={() => handleDeleteClick(branch)}
                />
              </div>
            </div>
          ))
        )}
      </div>
      {selectedBranch && (
        <>
          <UpdateBranch
            isOpen={isUpdateModalOpen}
            onClose={() => setIsUpdateModalOpen(false)}
            branch={selectedBranch}
            onUpdate={handleUpdate}
          />
          <DeleteBranch
            isOpen={isDeleteModalOpen}
            onClose={() => setIsDeleteModalOpen(false)}
            branch={selectedBranch}
            onDelete={handleDelete}
          />
        </>
      )}
    </div>
  );
};

export default BranchList;
