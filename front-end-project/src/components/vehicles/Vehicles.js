import React, { useState } from "react";
import VehiclesList from "../vehiclesList/VehiclesList";
import AddVehicle from "../addVehicle/AddVehicle";

const Vehicles = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [searchTerm, setSearchTerm] = useState("");

  const openModal = () => {
    setIsModalOpen(true);
  };

  return (
    <div className="w-full h-full flex flex-col">
      <div className="mb-4 flex justify-between items-center">
        <h2 className="text-2xl font-bold text-gray-800">
          Listado de Vehiculos
        </h2>
        <input
          type="text"
          placeholder="Buscar por nombre"
          className=" p-2 mr-2 border border-gray-300 rounded-lg dark:bg-gray-900 dark:text-slate-400"
          value={searchTerm}
          onChange={(e) => setSearchTerm(e.target.value)}
        />
      </div>

      <div className="flex-1 overflow-auto">
        <VehiclesList searchTerm={searchTerm} />
      </div>
      {isModalOpen && (
        <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50">
          <AddVehicle
            isModalOpen={isModalOpen}
            setIsModalOpen={setIsModalOpen}
          />
        </div>
      )}
    </div>
  );
};

export default Vehicles;
