import service from "../../assets/LogoBlack.png";

const Statics = () => {
  return (
    <div className="flex flex-col md:flex-row justify-center items-center">
      <div className="grid grid-cols-1 md:grid-cols-2 gap-5 w-full max-w-6xl px-4">
        <div className="flex flex-col justify-center items-center md:items-start">
          <h2 className="mb-1 font-bold text-4xl text-center md:text-left">
            ¡Oops! Esta sección se encuentra en construcción
          </h2>
        </div>

        <div className="flex justify-center items-center">
          <img
            src={service}
            alt="LogoHome"
            className="max-w-[200px] md:max-w-full h-auto"
          />
        </div>
      </div>
    </div>
  );
};

export default Statics;
