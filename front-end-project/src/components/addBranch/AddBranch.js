import React from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { branchValidationSchema } from "../../utils/validationSchema/ValidationSchema";
import { createBranch } from "../../utils/APIs/APIs";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const AddBranch = ({ isModalOpen, setIsModalOpen }) => {
  const handleSubmit = async (values, { resetForm }) => {
    try {
      await createBranch(values);
      toast.success("Sucursal creada exitosamente");
      setTimeout(() => {
        setIsModalOpen(false);
        resetForm();
      }, 3000);
    } catch (error) {
      toast.error(`Error al enviar la sucursal: ${error.message}`, {
        autoClose: 5000,
      });
    }
  };

  if (!isModalOpen) return null;

  return (
    <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50">
      <div className="bg-white rounded-lg p-6 w-full max-w-md mx-4 dark:bg-gray-900">
        <h2 className="text-2xl font-bold mb-4 dark:text-slate-300">
          Agregar Sucursal
        </h2>
        <Formik
          initialValues={{ name: "", location: "", phone: "" }}
          validationSchema={branchValidationSchema}
          onSubmit={handleSubmit}
        >
          {({ isSubmitting }) => (
            <Form>
              <div className="mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2  dark:text-slate-400"
                  htmlFor="name"
                >
                  Nombre
                </label>
                <Field
                  type="text"
                  name="name"
                  id="name"
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-slate-700 dark:text-slate-400"
                />
                <ErrorMessage
                  name="name"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2  dark:text-slate-400"
                  htmlFor="location"
                >
                  Ubicación
                </label>
                <Field
                  type="text"
                  name="location"
                  id="location"
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-slate-700 dark:text-slate-400"
                />
                <ErrorMessage
                  name="location"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2  dark:text-slate-400"
                  htmlFor="phone"
                >
                  Teléfono
                </label>
                <Field
                  type="text"
                  name="phone"
                  id="phone"
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-slate-700 dark:text-slate-400"
                />
                <ErrorMessage
                  name="phone"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="flex justify-end">
                <button
                  type="button"
                  onClick={() => setIsModalOpen(false)}
                  className="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded mr-2"
                >
                  Cancelar
                </button>
                <button
                  type="submit"
                  disabled={isSubmitting}
                  className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                >
                  Guardar
                </button>
              </div>
            </Form>
          )}
        </Formik>
      </div>
      <ToastContainer />
    </div>
  );
};

export default AddBranch;
