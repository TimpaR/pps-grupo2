import axios from "axios";
import * as Yup from "yup";
import { Formik, Form, Field, ErrorMessage } from "formik";
import React, { useEffect, useState } from "react";
import { updateShiftAPI } from "../../utils/APIs/APIs";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const validationSchema = Yup.object().shape({
  idBranch: Yup.string().required("La sucursal es obligatoria"),
  idService: Yup.string().required("El servicio es obligatorio"),
  idVehicle: Yup.string().required("El vehículo es obligatorio"),
});

const UpdateShift = ({ isOpen, onClose, shift, onUpdate }) => {
  const [branches, setBranches] = useState([]);
  const [clients, setClients] = useState([]);
  const [selectedClient, setSelectedClient] = useState("");
  const [vehicles, setVehicles] = useState([]);
  const [services, setServices] = useState([]);

  const initialValues = {
    idVehicle: shift.idVehicle || "",
    idBranch: shift.idBranch || "",
    idService: shift.idService || "",
  };

  const handleSubmit = async (values, { setSubmitting }) => {
    try {
      await updateShiftAPI(
        shift.idShift,
        values.idVehicle,
        values.idBranch,
        values.idService
      );
      toast.success("Turno actualizado exitosamente");
      setTimeout(() => {
        onUpdate();
        onClose();
      }, 1500);
    } catch (error) {
      console.error("Error updating shift:", error);
    } finally {
      setSubmitting(false);
    }
  };

  useEffect(() => {
    const fetchBranches = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7267/api/Branches/getAllBranchs"
        );
        setBranches(response.data);
      } catch (error) {
        console.error("Error al obtener sucursales:", error);
      }
    };

    fetchBranches();
  }, []);

  useEffect(() => {
    const fetchClients = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7267/api/User/GetAllUsers"
        );
        const allUsers = response.data;
        const clients = allUsers.filter((user) => user.userType === "client");
        setClients(clients);
      } catch (error) {
        console.error("Error al obtener clientes:", error);
      }
    };
    fetchClients();
  }, []);

  useEffect(() => {
    const fetchVehicles = async (clientId) => {
      try {
        const response = await axios.get(
          `https://localhost:7267/api/Vehicle?userId=${clientId}`
        );
        setVehicles(response.data);
      } catch (error) {
        console.error("Error al obtener los vehiculos:", error);
      }
    };

    if (selectedClient) {
      fetchVehicles(selectedClient);
    }
  }, [selectedClient]);

  useEffect(() => {
    const fetchServices = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7267/api/Service/getAllServices"
        );
        setServices(response.data);
      } catch (error) {
        console.error("Error fetching services:", error);
      }
    };

    fetchServices();
  }, []);

  if (!isOpen) return null;

  return (
    <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50">
      <div className="bg-white rounded-lg p-6 w-full max-w-md mx-4">
        <h2 className="text-2xl font-bold mb-4">Actualizar Turno</h2>
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={handleSubmit}
        >
          {({ isSubmitting }) => (
            <Form>
              <div className="mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2"
                  htmlFor="idBranch"
                >
                  Sucursal:
                </label>
                <Field
                  as="select"
                  name="idBranch"
                  id="idBranch"
                  className="hover:border-blue-500 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                >
                  <option value="">Seleccione una sucursal</option>
                  {branches.map((branch) => (
                    <option key={branch.idBranch} value={branch.idBranch}>
                      {branch.location}
                    </option>
                  ))}
                </Field>
                <ErrorMessage
                  name="idBranch"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2"
                  htmlFor="idService"
                >
                  Servicio:
                </label>
                <Field
                  as="select"
                  name="idService"
                  id="idService"
                  className="hover:border-blue-500 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                >
                  <option value="" label="Selecciona un servicio" />
                  {services.map((service) => (
                    <option key={service.idServicio} value={service.idServicio}>
                      {service.name}
                    </option>
                  ))}
                </Field>
                <ErrorMessage
                  name="idService"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2"
                  htmlFor="idClient"
                >
                  Cliente:
                </label>
                <Field
                  as="select"
                  name="idClient"
                  id="idClient"
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  onChange={(e) => {
                    const value = e.target.value;
                    setSelectedClient(value);
                  }}
                >
                  <option value="" label="Selecciona un cliente" />
                  {clients.map((client) => (
                    <option key={client.idUser} value={client.idUser}>
                      {client.name} {client.lastname}
                    </option>
                  ))}
                </Field>
                <ErrorMessage
                  name="idClient"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2"
                  htmlFor="idVehicle"
                >
                  Vehiculo del cliente:
                </label>
                <Field
                  as="select"
                  name="idVehicle"
                  id="idVehicle"
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                >
                  <option value="" label="Selecciona el vehiculo del cliente" />
                  {vehicles.map((vehicle) => (
                    <option key={vehicle.idVehicle} value={vehicle.idVehicle}>
                      {vehicle.brand} {vehicle.model}
                    </option>
                  ))}
                </Field>
                <ErrorMessage
                  name="idVehicle"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="flex justify-end">
                <button
                  type="button"
                  onClick={onClose}
                  className="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded mr-2"
                >
                  Cancelar
                </button>
                <button
                  type="submit"
                  disabled={isSubmitting}
                  className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                >
                  Guardar Cambios
                </button>
              </div>
            </Form>
          )}
        </Formik>
      </div>
      <ToastContainer />
    </div>
  );
};

export default UpdateShift;
