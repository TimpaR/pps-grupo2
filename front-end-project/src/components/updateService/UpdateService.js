import React from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { updateServiceAPI } from "../../utils/APIs/APIs";
import { updateServiceSchema } from "../../utils/validationSchema/ValidationSchema";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const UpdateService = ({ isOpen, onClose, service, onUpdate }) => {
  const initialValues = {
    urlImage: service?.urlImage || "",
    description: service.description || "",
    price: service.price || "",
  };

  const handleSubmit = async (values, { setSubmitting }) => {
    try {
      await updateServiceAPI(
        service.idServicio,
        values.urlImage,
        values.description,
        values.price
      );
      toast.success("Servicio actualizado exitosamente");
      setTimeout(() => {
        onUpdate();
        onClose();
      }, 1500);
    } catch (error) {
      console.error("Error updating service:", error);
    } finally {
      setSubmitting(false);
    }
  };

  if (!isOpen) return null;

  return (
    <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50">
      <div className="bg-white rounded-lg p-6 w-full max-w-md mx-4 dark:bg-gray-900 ">
        <h2 className="text-2xl font-bold mb-4  dark:text-slate-300">
          Modificar Servicio
        </h2>
        <Formik
          initialValues={initialValues}
          validationSchema={updateServiceSchema}
          onSubmit={handleSubmit}
        >
          {({ isSubmitting }) => (
            <Form>
              <div className="mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2  dark:text-slate-400"
                  htmlFor="urlImage"
                >
                  URL de la Imagen
                </label>
                <Field
                  type="text"
                  name="urlImage"
                  id="urlImage"
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-slate-700 dark:text-slate-400"
                />
                <ErrorMessage
                  name="urlImage"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2  dark:text-slate-400"
                  htmlFor="description"
                >
                  Descripción
                </label>
                <Field
                  as="textarea"
                  name="description"
                  id="description"
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-slate-700 dark:text-slate-400"
                />
                <ErrorMessage
                  name="description"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2  dark:text-slate-400"
                  htmlFor="price"
                >
                  Precio
                </label>
                <Field
                  type="number"
                  name="price"
                  id="price"
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-slate-700 dark:text-slate-400"
                />
                <ErrorMessage
                  name="price"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="flex justify-end">
                <button
                  type="button"
                  onClick={onClose}
                  className="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded mr-2"
                >
                  Cancelar
                </button>
                <button
                  type="submit"
                  disabled={isSubmitting}
                  className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                >
                  Guardar Cambios
                </button>
              </div>
            </Form>
          )}
        </Formik>
      </div>
      <ToastContainer />
    </div>
  );
};

export default UpdateService;
