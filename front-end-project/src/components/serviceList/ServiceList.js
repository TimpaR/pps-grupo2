import React, { useState, useEffect } from "react";
import axios from "axios";
import { AiFillDelete, AiFillEdit } from "react-icons/ai";
import UpdateService from "../updateService/UpdateService";
import DeleteService from "../deleteService/DeleteService";

const ServiceList = () => {
  const [services, setServices] = useState([]);
  const [selectedService, setSelectedService] = useState(null);
  const [isUpdateModalOpen, setIsUpdateModalOpen] = useState(false);
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);

  useEffect(() => {
    const fetchServices = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7267/api/Service/getAllServices"
        );
        setServices(response.data);
      } catch (error) {
        console.error("Error fetching services:", error);
      }
    };

    fetchServices();
  }, []);

  const handleEditClick = (service) => {
    setSelectedService(service);
    setIsUpdateModalOpen(true);
  };

  const handleDeleteClick = (service) => {
    setSelectedService(service);
    setIsDeleteModalOpen(true);
  };

  const handleUpdate = async () => {
    try {
      const response = await axios.get(
        "https://localhost:7267/api/Service/getAllServices"
      );
      setServices(response.data);
    } catch (error) {
      console.error("Error fetching services:", error);
    }
  };

  const handleDelete = async () => {
    try {
      const response = await axios.get(
        "https://localhost:7267/api/Service/getAllServices"
      );
      setServices(response.data);
    } catch (error) {
      console.error("Error fetching services:", error);
    }
  };

  return (
    <div className="flex justify-center items-center">
      <div className="w-full p-4 overflow-y-auto bg-gray-100 rounded-lg shadow-lg dark:bg-gray-900 ">
        {services.length === 0 ? (
          <p className="text-center text-gray-500  dark:text-slate-300">
            No hay servicios cargados.
          </p>
        ) : (
          services.map((service) => (
            <div
              key={service.idServicio}
              className="bg-white rounded-lg shadow-md p-4 mb-4 flex justify-between items-end dark:bg-slate-700 dark:text-slate-300"
            >
              <div>
                <h3 className="text-lg font-semibold">{service.name}</h3>
                <p className="text-sm">
                  <strong>Descripción:</strong> {service.description}
                </p>
                <p className="text-sm">
                  <strong>Precio:</strong> {service.price}
                </p>
              </div>
              <div className="flex space-x-4">
                <AiFillEdit
                  className="text-blue-500 cursor-pointer"
                  onClick={() => handleEditClick(service)}
                />
                <AiFillDelete
                  className="text-red-500 cursor-pointer"
                  onClick={() => handleDeleteClick(service)}
                />
              </div>
            </div>
          ))
        )}
      </div>
      {selectedService && (
        <>
          <UpdateService
            isOpen={isUpdateModalOpen}
            onClose={() => setIsUpdateModalOpen(false)}
            service={selectedService}
            onUpdate={handleUpdate}
          />
          <DeleteService
            isOpen={isDeleteModalOpen}
            onClose={() => setIsDeleteModalOpen(false)}
            service={selectedService}
            onDelete={handleDelete}
          />
        </>
      )}
    </div>
  );
};

export default ServiceList;
