import React from "react";
import { deleteServiceAPI } from "../../utils/APIs/APIs";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const DeleteService = ({ isOpen, onClose, service, onDelete }) => {
  if (!isOpen) return null;

  const handleDelete = async () => {
    if (!service.idServicio) {
      console.error("service.idServicio está indefinido");
      return;
    }

    try {
      await deleteServiceAPI(service.idServicio);
      onDelete();
      toast.success(`Servicio ${service.name} eliminado correctamente`, {
        autoClose: 3000,
      });
      setTimeout(() => {
        onClose();
      }, 3000);
    } catch (error) {
      console.error("Error al eliminar el servicio:", error);
      toast.error(
        "Error al eliminar el servicio. Por favor, inténtelo de nuevo.",
        {
          autoClose: 3000,
        }
      );
    }
  };

  return (
    <>
      <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50">
        <div className="bg-white rounded-lg p-6 w-full max-w-md mx-4 dark:bg-gray-900 ">
          <h2 className="text-2xl font-bold mb-4  dark:text-slate-300">
            Confirmar Eliminación
          </h2>
          <p className=" dark:text-slate-300">
            ¿Está seguro de que desea eliminar el servicio{" "}
            <strong>{service.name}</strong>?
          </p>
          <div className="flex justify-end mt-4">
            <button
              type="button"
              onClick={onClose}
              className="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded mr-2"
            >
              Cancelar
            </button>
            <button
              type="button"
              onClick={handleDelete}
              className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded"
            >
              Confirmar
            </button>
          </div>
        </div>
      </div>
      <ToastContainer autoClose={3000} />{" "}
    </>
  );
};

export default DeleteService;
