import axios from "axios";
import React, { useEffect, useState } from "react";
import { AiFillDelete, AiFillEdit } from "react-icons/ai";
import UpdateUser from "../updateUser/UpdateUser";
import DeleteUser from "../deleteUser/DeleteUser";
import { fetchUsers } from "../../utils/APIs/APIs";

const UsersList = ({ filter, getNoUsersMessage }) => {
  const [users, setUsers] = useState([]);
  const [selectedUser, setSelectedUser] = useState(null);
  const [isUpdateModalOpen, setIsUpdateModalOpen] = useState(false);
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);

  useEffect(() => {
    const loadUsers = async () => {
      const usersData = await fetchUsers();
      setUsers(usersData);
    };

    loadUsers();
  }, []);

  const handleEditClick = (user) => {
    setSelectedUser(user);
    setIsUpdateModalOpen(true);
  };

  const handleDeleteClick = (user) => {
    setSelectedUser(user);
    setIsDeleteModalOpen(true);
  };

  const handleUpdate = async () => {
    try {
      const response = await axios.get(
        "https://localhost:7267/api/User/GetAllUsers"
      );
      setUsers(response.data);
    } catch (error) {
      console.error("Error al obtener usuarios:", error);
    }
  };

  const handleDelete = async () => {
    try {
      const response = await axios.get(
        "https://localhost:7267/api/User/GetAllUsers"
      );
      setUsers(response.data);
    } catch (error) {
      console.error("Error al obtener usuarios:", error);
    }
  };

  const filteredUsers =
    filter === "all" ? users : users.filter((user) => user.userType === filter);

  return (
    <div className="flex flex-col justify-center items-center">
      <div className="w-full p-4 overflow-y-auto bg-gray-100 rounded-lg shadow-lg dark:bg-gray-900 dark:text-slate-300">
        {filteredUsers.length === 0 ? (
          <p className="text-center text-gray-500">{getNoUsersMessage()}</p>
        ) : (
          filteredUsers.map((user) => (
            <div
              key={user.idUser}
              className="bg-white rounded-lg shadow-md p-4 mb-4 flex justify-between items-end dark:bg-slate-700"
            >
              <div>
                <h3 className="text-lg font-semibold">
                  {user.name} {user.lastname}
                </h3>
                <p className="text-sm">
                  <strong>Email:</strong> {user.email}
                </p>
                <p className="text-sm">
                  <strong>Teléfono:</strong> {user.phone}
                </p>
                <p className="text-sm">
                  <strong>Tipo: </strong>
                  {user.userType === "admin"
                    ? "Administrador"
                    : user.userType === "employee"
                    ? "Empleado"
                    : user.userType === "client"
                    ? "Cliente"
                    : ""}
                </p>
                {user.userType === "employee" && (
                  <p className="text-sm">
                    <strong>Sucursal:</strong> {user.idBranch}
                  </p>
                )}
                {user.userType === "client" && (
                  <p className="text-sm">
                    <strong>Vehiculos:</strong>{" "}
                    {user.vehicles.length === 0
                      ? "No tiene vehiculos"
                      : user.vehicles}
                  </p>
                )}
              </div>
              <div></div>
              <div className="flex space-x-4">
                <AiFillEdit
                  className="text-blue-500 cursor-pointer"
                  onClick={() => handleEditClick(user)}
                />
                <AiFillDelete
                  className="text-red-500 cursor-pointer"
                  onClick={() => handleDeleteClick(user)}
                />
              </div>
            </div>
          ))
        )}
      </div>
      {selectedUser && (
        <>
          <UpdateUser
            isOpen={isUpdateModalOpen}
            onClose={() => setIsUpdateModalOpen(false)}
            userEdit={selectedUser}
            onUpdate={handleUpdate}
          />
          <DeleteUser
            isOpen={isDeleteModalOpen}
            onClose={() => setIsDeleteModalOpen(false)}
            user={selectedUser}
            onDelete={handleDelete}
          />
        </>
      )}
    </div>
  );
};

export default UsersList;
