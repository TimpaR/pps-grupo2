import React, { useState, useEffect } from "react";
import axios from "axios";
import { AiFillDelete } from "react-icons/ai";
import DeleteVehicle from "../deleteVehicle/DeleteVehicle";
import { useAuth } from "../../services/authContext/AuthContext";

const ClientVehiclesList = () => {
  const [vehicles, setVehicles] = useState([]);
  const [selectedVehicles, setSelectedVehicles] = useState(null);
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);
  const { authToken, user } = useAuth();
  const userId = user.idUser;

  useEffect(() => {
    const fetchVehicles = async () => {
      try {
        const response = await axios.get(
          `https://localhost:7267/api/Vehicle?userId=${userId}`,
          {
            headers: {
              Authorization: `Bearer ${authToken}`, // Incluir el token de autorización en los headers
            },
          }
        );
        setVehicles(response.data);
      } catch (error) {
        console.error("Error al obtener vehículos:", error);
      }
    };

    if (authToken && userId) {
      fetchVehicles();
    }
  }, [authToken, userId]);

  const handleDeleteClick = (vehicle) => {
    setSelectedVehicles(vehicle);
    setIsDeleteModalOpen(true);
  };

  const handleDelete = async () => {
    try {
      const response = await axios.get(
        `https://localhost:7267/api/Vehicle?userId=${userId}`
      );
      setVehicles(response.data);
    } catch (error) {
      console.error("Error al obtener sucursales:", error);
    }
  };

  return (
    <div className="flex flex-col justify-center items-center h-full overflow-y-auto">
      <div className="w-full p-4 bg-gray-100 rounded-lg shadow-lg dark:bg-slate-800">
        {vehicles.length === 0 ? (
          <p className="text-center text-gray-500 dark:text-slate-300">
            No hay vehiculos cargados.
          </p>
        ) : (
          vehicles.map((vehicle) => (
            <div
              key={vehicle.idVehicle}
              className="bg-white rounded-lg shadow-md p-4 mb-4 flex justify-between items-end"
            >
              <div>
                <h3 className="text-lg font-semibold">Automóvil</h3>
                <p className="text-sm">
                  <strong>ID:</strong> {vehicle.idVehicle}
                </p>
                <p className="text-sm">
                  <strong>Marca:</strong> {vehicle.brand}
                </p>
                <p className="text-sm">
                  <strong>Modelo:</strong> {vehicle.model}
                </p>
              </div>
              <div className="flex space-x-4">
                <AiFillDelete
                  className="text-red-500 cursor-pointer"
                  onClick={() => handleDeleteClick(vehicle)}
                />
              </div>
            </div>
          ))
        )}
      </div>
      {selectedVehicles && (
        <>
          <DeleteVehicle
            isOpen={isDeleteModalOpen}
            onClose={() => setIsDeleteModalOpen(false)}
            vehicle={selectedVehicles}
            onDelete={handleDelete}
          />
        </>
      )}
    </div>
  );
};

export default ClientVehiclesList;
