import React, { useState, useEffect } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { branchUpdateValidationSchema } from "../../utils/validationSchema/ValidationSchema";
import { updateBranchAPI } from "../../utils/APIs/APIs";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const UpdateBranch = ({ isOpen, onClose, branch, onUpdate }) => {
  const [phone, setPhone] = useState(branch.phone || "");

  useEffect(() => {
    setPhone(branch.phone || "");
  }, [branch.phone]);

  if (!isOpen) return null;

  const handleSubmit = async () => {
    if (!branch.idBranch) {
      console.error("branch.idBranch está indefinido");
      return;
    }

    try {
      await updateBranchAPI(branch.idBranch, phone);
      toast.success("Teléfono actualizado exitosamente");
      setTimeout(() => {
        onUpdate();
        onClose();
      }, 1500);
    } catch (error) {
      console.error("Error al actualizar la sucursal:", error);
    }
  };

  return (
    <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50">
      <div className="bg-white rounded-lg p-6 w-full max-w-md mx-4 dark:bg-gray-900">
        <h2 className="text-2xl font-bold mb-4  dark:text-slate-300">
          Actualizar Teléfono
        </h2>
        <Formik
          initialValues={{ phone }}
          validationSchema={branchUpdateValidationSchema}
          onSubmit={handleSubmit}
        >
          {({ isSubmitting }) => (
            <Form>
              <div className="mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2 dark:text-slate-400"
                  htmlFor="phone"
                >
                  Teléfono
                </label>
                <Field
                  type="text"
                  name="phone"
                  id="phone"
                  value={phone}
                  onChange={(e) => setPhone(e.target.value)}
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-slate-700 dark:text-slate-400"
                />
                <ErrorMessage
                  name="phone"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="flex justify-end">
                <button
                  type="button"
                  onClick={onClose}
                  className="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded mr-2"
                >
                  Cancelar
                </button>
                <button
                  type="submit"
                  disabled={isSubmitting}
                  className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                >
                  Guardar
                </button>
              </div>
            </Form>
          )}
        </Formik>
      </div>
      <ToastContainer />
    </div>
  );
};

export default UpdateBranch;
