import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import service from "../../assets/foto.png";
import LoginModal from "../loginModal/LoginModal";
import { useAuth } from "../../services/authContext/AuthContext";

const Main = () => {
  const { authToken } = useAuth();
  const navigate = useNavigate();

  const [isLoginModalOpen, setIsLoginModalOpen] = useState(false);

  const handleReservarTurno = () => {
    if (!authToken) {
      setIsLoginModalOpen(true);
    } else {
      navigate("/clientShift");
    }
  };

  return (
    <div className="flex flex-col md:flex-row justify-center items-center h-[75vh] dark:text-slate-400">
      <div className="grid grid-cols-1 md:grid-cols-2 gap-5 w-full max-w-6xl px-4">
        <div className="flex flex-col justify-center items-center md:items-start">
          <h2 className="font-bold text-4xl text-center md:text-left">
            ¡Renová la belleza <br /> de tu vehículo!
          </h2>
          <p className="text-lg text-center md:text-left">
            Descubrí nuestro servicio de detailing para lucir tu auto como nuevo
          </p>
          <button
            onClick={handleReservarTurno}
            className="mt-4 px-6 py-2 bg-blue-500 text-white font-semibold rounded-lg shadow-md hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-400 focus:ring-opacity-75"
          >
            Reservar turno
          </button>
        </div>
        <div className="flex justify-center items-center">
          <img
            src={service}
            alt="LogoHome"
            className="max-w-[200px] md:max-w-full h-auto"
          />
        </div>
      </div>
      {isLoginModalOpen && (
        <LoginModal
          isOpen={isLoginModalOpen}
          onClose={() => setIsLoginModalOpen(false)}
          onRegisterClick={() => {}}
        />
      )}
    </div>
  );
};

export default Main;
