import React from "react";
import { FaStar } from "react-icons/fa";
import { Formik, Form, Field, ErrorMessage } from "formik";
import review from "../../assets/Review.png";
import { useAuth } from "../../services/authContext/AuthContext";
import { createReviewAPI } from "../../utils/APIs/APIs";
import { reviewValidationSchema } from "../../utils/validationSchema/ValidationSchema";
import CarouselReviews from "../carouselReviews/CarouselReviews";

const Review = () => {
  const { user } = useAuth();

  const handleSubmit = async (values, { resetForm }) => {
    try {
      if (!user || user.userType !== "client") {
        alert("Debes estar logueado como cliente para dejar una reseña."); //Hacerlo con toastify
        return;
      }
      await createReviewAPI({ ...values, idClient: user.idUser });
      resetForm();
    } catch (error) {
      console.error("Error al enviar la reseña:", error);
    }
  };

  return (
    <div id="review-section" className="container mx-auto mt-5 mb-5 ">
      <div className="dark:bg-slate-700 dark:text-slate-400 flex flex-col md:flex-row items-center bg-white overflow-hidden">
        <div className="md:w-1/2 p-4">
          <img
            src={review}
            alt="Producto"
            className="rounded-lg w-full h-auto object-cover"
          />
        </div>
        <div className="p-5 w-full md:w-1/2 text-center sm:text-left">
          <h2 className="text-xl font-bold mb-2 ">Deja tu reseña</h2>
          <Formik
            initialValues={{
              rating: 0,
              comment: "",
            }}
            validationSchema={reviewValidationSchema}
            onSubmit={handleSubmit}
          >
            {({ values, handleChange, handleBlur }) => (
              <Form className="flex flex-col items-center sm:items-start">
                <div className="flex justify-center sm:justify-start mb-4">
                  {[...Array(5)].map((_, index) => {
                    const starIndex = index + 1;
                    return (
                      <button
                        key={starIndex}
                        type="button"
                        className="focus:outline-none"
                        onClick={() =>
                          handleChange({
                            target: { name: "rating", value: starIndex },
                          })
                        }
                        onMouseEnter={() =>
                          handleChange({
                            target: { name: "hover", value: starIndex },
                          })
                        }
                        onMouseLeave={() =>
                          handleChange({
                            target: { name: "hover", value: null },
                          })
                        }
                      >
                        <FaStar
                          className={`text-2xl ${
                            starIndex <= (values.hover || values.rating)
                              ? "text-yellow-500"
                              : "text-gray-400"
                          }`}
                        />
                      </button>
                    );
                  })}
                </div>
                <Field
                  as="textarea"
                  name="comment"
                  className="w-full border border-gray-300 rounded mb-4 dark:bg-slate-500 dark:border-slate-800"
                  rows="4"
                  placeholder="Escribe tu comentario aquí..."
                />
                <ErrorMessage
                  name="rating"
                  component="div"
                  className="text-red-500 mb-4"
                />
                <ErrorMessage
                  name="comment"
                  component="div"
                  className="text-red-500 mb-4"
                />
                <button
                  type="submit"
                  className="bg-blue-500 text-white px-4 py-2 rounded-md hover:bg-blue-700"
                >
                  Enviar
                </button>
              </Form>
            )}
          </Formik>
        </div>
      </div>
      <CarouselReviews />
    </div>
  );
};

export default Review;
