import axios from "axios";
import React, { useEffect, useState } from "react";
import { FaStar } from "react-icons/fa";

const CarouselReviews = () => {
  const [reviews, setReviews] = useState([]);
  const [users, setUsers] = useState([]);
  const [currentReview, setCurrentReview] = useState(0);

  useEffect(() => {
    const fetchReviews = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7267/review/GetAll"
        );
        setReviews(response.data);
      } catch (error) {
        console.error("Error al obtener las reseñas:", error);
      }
    };

    const fetchUsers = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7267/api/User/GetAllUsers"
        );
        setUsers(response.data);
      } catch (error) {
        console.error("Error al obtener los usuarios:", error);
      }
    };

    fetchReviews();
    fetchUsers();
  }, []);

  useEffect(() => {
    const intervalId = setInterval(() => {
      setCurrentReview((prevReview) => (prevReview + 1) % reviews.length);
    }, 5000); // Cambia cada 5 segundos

    return () => clearInterval(intervalId);
  }, [reviews]);

  if (reviews.length === 0 || users.length === 0) {
    return <div></div>;
  }

  const currentReviewUser = users.find(
    (user) => user.idUser === reviews[currentReview].idClient
  );

  if (!currentReviewUser) {
    return <div></div>;
  }

  return (
    <div className="container mx-auto mt-5 mb-5 flex justify-center px-4 ">
      <div className="w-full max-w-md ">
        <h2 className="text-xl font-bold mb-5 text-center dark:text-slate-400">
          Reseñas
        </h2>
        <div className="bg-white shadow-lg rounded-lg p-5 dark:bg-slate-700 dark:text-slate-400">
          <h3 className="text-xl font-bold mb-2 text-center">
            {currentReviewUser.name} {currentReviewUser.lastname}
          </h3>
          <div className="flex justify-center mb-4">
            {[...Array(5)].map((_, index) => {
              const starIndex = index + 1;
              return (
                <FaStar
                  key={starIndex}
                  className={`text-2xl ${
                    starIndex <= reviews[currentReview].rating
                      ? "text-yellow-500"
                      : "text-gray-400"
                  }`}
                />
              );
            })}
          </div>
          <p className="mb-4 text-center min-h-[50px] break-words">
            {reviews[currentReview].comment}
          </p>
        </div>
      </div>
    </div>
  );
};

export default CarouselReviews;
