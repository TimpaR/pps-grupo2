import React from "react";
import { Link } from "react-router-dom";

const Menu = ({ toggleOpen, toggleLog, isLog }) => {
  return (
    <div
      className="grid  grid-rows-3
       text-center items-center bg-neutral-400 "
      onClick={toggleOpen}
    >
      <Link className="p-4" to="#">
        Servicios
      </Link>
      <Link className="p-4" to="#">
        Sucursales
      </Link>
      {isLog && (
        <Link className="p-4" to="/clientShift">
          Reservar turno
        </Link>
      )}

      {!isLog && (
        <Link to="" className="p-4" onClick={toggleLog}>
          Iniciar sesión
        </Link>
      )}
    </div>
  );
};

export default Menu;
