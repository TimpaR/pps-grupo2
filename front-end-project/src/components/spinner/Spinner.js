import React, { useEffect, useState } from "react";

const Spinner = () => {
  const [count, setCount] = useState(5);

  useEffect(() => {
    const timer = setInterval(() => {
      setCount((prevCount) => (prevCount > 0 ? prevCount - 1 : 0));
    }, 1000);

    return () => clearInterval(timer);
  }, []);

  return (
    <div className="mt-10 ml-52 relative">
      <div className="animate-spin rounded-full h-20 w-20 border-b-2 border-gray-900"></div>
      <div className="absolute inset-0 flex justify-center items-center">
        <span className="text-xl font-bold">{count}</span>
      </div>
    </div>
  );
};

export default Spinner;
