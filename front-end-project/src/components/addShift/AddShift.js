import React, { useEffect, useState } from "react";
import { ErrorMessage, Field, Form, Formik } from "formik";
import axios from "axios";
import { createShift } from "../../utils/APIs/APIs";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const AddShift = ({ isModalOpen, setIsModalOpen }) => {
  const [clients, setClients] = useState([]);
  const [branches, setBranches] = useState([]);
  const [services, setServices] = useState([]);
  const [vehicles, setVehicles] = useState([]);
  const [selectedClient, setSelectedClient] = useState("");

  useEffect(() => {
    const fetchClients = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7267/api/User/GetAllUsers"
        );
        const allUsers = response.data;
        const clients = allUsers.filter((user) => user.userType === "client");
        setClients(clients);
      } catch (error) {
        console.error("Error al obtener clientes:", error);
      }
    };
    fetchClients();
  }, []);

  useEffect(() => {
    const fetchServices = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7267/api/Service/getAllServices"
        );
        setServices(response.data);
      } catch (error) {
        console.error("Error fetching services:", error);
      }
    };

    fetchServices();
  }, []);

  useEffect(() => {
    const fetchBranches = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7267/api/Branches/getAllBranchs"
        );
        setBranches(response.data);
      } catch (error) {
        console.error("Error al obtener sucursales:", error);
      }
    };

    fetchBranches();
  }, []);

  useEffect(() => {
    const fetchVehicles = async (clientId) => {
      try {
        const response = await axios.get(
          `https://localhost:7267/api/Vehicle?userId=${clientId}`
        );
        setVehicles(response.data);
      } catch (error) {
        console.error("Error al obtener los vehiculos:", error);
      }
    };

    if (selectedClient) {
      fetchVehicles(selectedClient);
    }
  }, [selectedClient]);

  const handleSubmit = async (values, { resetForm }) => {
    try {
      console.log("Submitted values:", values);
      await createShift(values);
      toast.success("Turno agregado exitosamente");
      setTimeout(() => {
        setIsModalOpen(false);
        resetForm();
      }, 2000);
    } catch (error) {
      toast.error(`Error al agregar el turno: ${error.message}`);
      console.error("Error al agregar el turno:", error);
    }
  };

  if (!isModalOpen) return null;

  return (
    <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50">
      <div className="bg-white rounded-lg p-6 w-full max-w-4xl mx-4 overflow-auto dark:bg-gray-900 dark:text-slate-300">
        <h2 className="text-2xl font-bold mb-4">Añadir Turno</h2>
        <Formik
          initialValues={{
            date: "",
            startTime: "",
            endTime: "",
            idService: null,
            idBranch: "",
            idClient: null,
            idVehicle: null,
          }}
          onSubmit={handleSubmit}
        >
          {({ setFieldValue, isSubmitting }) => (
            <Form className="flex flex-wrap -mx-2">
              <div className="w-full md:w-1/2 lg:w-1/3 px-2 mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2  dark:text-slate-400"
                  htmlFor="date"
                >
                  Dia
                </label>
                <Field
                  type="date"
                  name="date"
                  id="date"
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-slate-700 dark:text-slate-400"
                />
                <ErrorMessage
                  name="date"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="w-full md:w-1/2 lg:w-1/3 px-2 mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2  dark:text-slate-400"
                  htmlFor="startTime"
                >
                  Comienzo del Turno
                </label>
                <Field
                  type="time"
                  step="1"
                  name="startTime"
                  id="startTime"
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-slate-700 dark:text-slate-400"
                />
                <ErrorMessage
                  name="startTime"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="w-full md:w-1/2 lg:w-1/3 px-2 mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2  dark:text-slate-400"
                  htmlFor="endTime"
                >
                  Fin del Turno
                </label>
                <Field
                  type="time"
                  step="1"
                  name="endTime"
                  id="endTime"
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-slate-700 dark:text-slate-400"
                />
                <ErrorMessage
                  name="endTime"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="w-full md:w-1/2 lg:w-1/3 px-2 mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2  dark:text-slate-400"
                  htmlFor="idService"
                >
                  Servicio
                </label>
                <Field
                  as="select"
                  name="idService"
                  id="idService"
                  className="hover:border-blue-500 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-slate-700 dark:text-slate-400"
                >
                  <option value={null} label="Seleccione un servicio" />
                  {services.map((service) => (
                    <option key={service.idServicio} value={service.idServicio}>
                      {service.name}
                    </option>
                  ))}
                </Field>
                <ErrorMessage
                  name="idService"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="w-full md:w-1/2 lg:w-1/3 px-2 mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2  dark:text-slate-400"
                  htmlFor="idBranch"
                >
                  Sucursal
                </label>
                <Field
                  as="select"
                  name="idBranch"
                  id="idBranch"
                  className="hover:border-blue-500 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-slate-700 dark:text-slate-400"
                >
                  <option value="" label="Selecciona una sucursal" />
                  {branches.map((branch) => (
                    <option key={branch.idBranch} value={branch.idBranch}>
                      {branch.location}
                    </option>
                  ))}
                </Field>
                <ErrorMessage
                  name="idBranch"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="w-full md:w-1/2 lg:w-1/3 px-2 mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2  dark:text-slate-400"
                  htmlFor="idClient"
                >
                  Cliente
                </label>
                <Field
                  as="select"
                  name="idClient"
                  id="idClient"
                  className="hover:border-blue-500 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-slate-700 dark:text-slate-400"
                  onChange={(e) => {
                    const value = e.target.value;
                    setSelectedClient(value);
                    setFieldValue("idClient", value);
                  }}
                >
                  <option value={null} label="Seleccione un cliente" />
                  {clients.map((client) => (
                    <option key={client.idUser} value={client.idUser}>
                      {client.name} {client.lastname}
                    </option>
                  ))}
                </Field>
                <ErrorMessage
                  name="idClient"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="w-full md:w-1/2 lg:w-1/3 px-2 mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2  dark:text-slate-400"
                  htmlFor="idVehicle"
                >
                  Vehiculo del cliente
                </label>
                <Field
                  as="select"
                  name="idVehicle"
                  id="idVehicle"
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-slate-700 dark:text-slate-400"
                >
                  <option
                    value={null}
                    label="Seleccione el vehículo del cliente"
                  />
                  {vehicles.map((vehicle) => (
                    <option key={vehicle.idVehicle} value={vehicle.idVehicle}>
                      {vehicle.brand} {vehicle.model}
                    </option>
                  ))}
                </Field>
                <ErrorMessage
                  name="idVehicle"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="w-full flex justify-end px-2">
                <button
                  type="button"
                  onClick={() => setIsModalOpen(false)}
                  className="bg-gray-500 hover:bg-gray-700  text-white font-bold py-2 px-4 rounded mr-2"
                >
                  Cancelar
                </button>
                <button
                  type="submit"
                  disabled={isSubmitting}
                  className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                >
                  Guardar
                </button>
              </div>
            </Form>
          )}
        </Formik>
      </div>
      <ToastContainer />
    </div>
  );
};

export default AddShift;
