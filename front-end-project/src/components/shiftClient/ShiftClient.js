import React, { useEffect, useState } from "react";
import axios from "axios";
import { updateShiftAPI } from "../../utils/APIs/APIs";
import { useAuth } from "../../services/authContext/AuthContext";

const ShiftClient = () => {
  const { user } = useAuth();
  const [shifts, setShifts] = useState([]);
  const [services, setServices] = useState([]);
  const [branches, setBranches] = useState([]);
  const [vehicles, setVehicles] = useState([]);
  const [selectedService, setSelectedService] = useState("");
  const [selectedShift, setSelectedShift] = useState(null);
  const [modalOpen, setModalOpen] = useState(false);
  const [selectedVehicle, setSelectedVehicle] = useState("");

  useEffect(() => {
    const fetchShifts = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7267/api/Shift/getAllShift"
        );
        setShifts(response.data);
      } catch (error) {
        console.error("Error al obtener los turnos:", error);
      }
    };

    fetchShifts();
  }, []);

  useEffect(() => {
    const fetchBranches = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7267/api/Branches/getAllBranchs"
        );
        setBranches(response.data);
      } catch (error) {
        console.error("Error al obtener sucursales:", error);
      }
    };

    fetchBranches();
  }, []);

  useEffect(() => {
    const fetchServices = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7267/api/Service/getAllServices"
        );
        setServices(response.data);
      } catch (error) {
        console.error("Error fetching services:", error);
      }
    };

    fetchServices();
  }, []);

  useEffect(() => {
    const fetchVehicles = async () => {
      try {
        const response = await axios.get(
          `https://localhost:7267/api/Vehicle?userId=${user.idUser}`
        );
        setVehicles(response.data);
      } catch (error) {
        console.error("Error al obtener los vehiculos:", error);
      }
    };
    fetchVehicles();
  }, [user]);

  const formatDate = (dateString) => {
    const date = new Date(dateString);
    const options = { weekday: "long", day: "numeric", month: "long" };
    const formattedDate = date.toLocaleDateString("es-ES", options);

    return formattedDate.replace(/\b\w/g, (char) => char.toUpperCase());
  };

  const formatTime = (timeString) => {
    const [hours, minutes] = timeString.split(":");
    return `${hours}:${minutes} hs`;
  };

  const getServiceName = (id) => {
    const service = services.find((service) => service.idServicio === id);
    return service ? service.name : "N/A";
  };

  const getBranchName = (id) => {
    const branch = branches.find((branch) => branch.idBranch === id);
    return branch ? branch.location : "N/A";
  };

  const handleReserve = (shift) => {
    setSelectedShift(shift);
    setModalOpen(true);
  };

  const handleServiceChange = (event) => {
    setSelectedService(event.target.value);
  };

  const handleModalClose = () => {
    setModalOpen(false);
    setSelectedShift(null);
  };

  const handleVehicleChange = (event) => {
    setSelectedVehicle(event.target.value);
  };

  const handleConfirmShift = async () => {
    try {
      await updateShiftAPI(
        selectedShift.idShift,
        selectedVehicle,
        selectedShift.idBranch,
        selectedShift.idService
      );
      console.log("Turno confirmado");
      setShifts(
        shifts.filter((shift) => shift.idShift !== selectedShift.idShift)
      );
      handleModalClose();
    } catch (error) {
      console.error("Error al confirmar el turno:", error);
    }
  };

  // Filtrar turnos según el servicio seleccionado y que tengan idVehicule en null
  const filteredShifts = shifts
    .filter((shift) => shift.idVehicle === null)
    .filter(
      (shift) =>
        !selectedService || shift.idService === parseInt(selectedService)
    );

  return (
    <div className="bg-gray-100 min-h-screen p-8">
      <div className="mb-4">
        <label
          htmlFor="service"
          className="block text-gray-700 text-sm font-bold mb-2"
        >
          Filtrar por Servicio:
        </label>
        <select
          id="service"
          value={selectedService}
          onChange={handleServiceChange}
          className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
        >
          <option value="">Todos los servicios</option>
          {services.map((service) => (
            <option key={service.idServicio} value={service.idServicio}>
              {service.name}
            </option>
          ))}
        </select>
      </div>
      <div className="flex flex-wrap justify-center">
        {filteredShifts.length > 0 ? (
          filteredShifts.map((shift) => (
            <div
              key={shift.id}
              className="max-w-sm rounded-lg overflow-hidden shadow-lg p-4 m-4 bg-white bg-opacity-90"
            >
              <h3 className="text-xl font-bold mb-2 text-center">
                Turno disponible: {getServiceName(shift.idService)}
              </h3>
              <p className="text-gray-700 text-base">
                Sucursal {getBranchName(shift.idBranch)}
              </p>
              <br></br>
              <p className="text-gray-700 text-base">
                {" "}
                {formatDate(shift.date)}
              </p>
              <p className="text-gray-700 text-base">
                <strong>
                  {formatTime(shift.startTime)} - {formatTime(shift.endTime)}
                </strong>
              </p>
              <button
                onClick={() => handleReserve(shift)}
                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mt-4"
              >
                Reservar Turno
              </button>
            </div>
          ))
        ) : (
          <p className="text-center text-gray-700 text-lg">
            <strong>No hay turnos disponibles</strong>
          </p>
        )}
      </div>
      {modalOpen && selectedShift && (
        <div className="fixed top-0 left-0 w-full h-full bg-gray-800 bg-opacity-50 flex justify-center items-center">
          <div className="bg-white p-8 rounded shadow-lg max-w-md">
            <h2 className="text-xl font-bold mb-4">
              Confirmar Reserva de Turno
            </h2>
            <p>
              <strong>Servicio:</strong>{" "}
              {getServiceName(selectedShift.idService)}
            </p>
            <p>
              <strong>Fecha:</strong> {formatDate(selectedShift.date)}
            </p>
            <p>
              <strong>Comienzo del Turno:</strong>{" "}
              {formatTime(selectedShift.startTime)}
            </p>
            <p>
              <strong>Fin del Turno:</strong>{" "}
              {formatTime(selectedShift.endTime)}
            </p>
            <p>
              <strong>Sucursal:</strong> {getBranchName(selectedShift.idBranch)}
            </p>
            <label htmlFor="vehicle">
              <strong>Seleccione un Vehículo:</strong>
            </label>
            <select
              id="vehicle"
              value={selectedVehicle}
              onChange={handleVehicleChange}
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            >
              <option value="">Seleccione un vehículo</option>
              {vehicles.map((vehicle) => (
                <option key={vehicle.idVehicle} value={vehicle.idVehicle}>
                  {vehicle.brand} {vehicle.model}
                </option>
              ))}
            </select>
            <p>¿Desea confirmar este turno?</p>
            <div className="flex justify-end mt-4">
              <button
                onClick={handleModalClose}
                className="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded mr-4"
              >
                Cancelar
              </button>
              <button
                onClick={handleConfirmShift}
                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
              >
                Confirmar Turno
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default ShiftClient;
