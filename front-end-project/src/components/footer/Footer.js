import React from "react";
import {
  FaFacebookF,
  FaTwitter,
  FaInstagram,
  FaLinkedinIn,
} from "react-icons/fa";
import Logo from "../../assets/LogoBlack.png";
import { Link } from "react-router-dom";

const Footer = () => {
  return (
    <footer className="bg-cyan-500 text-white py-8">
      <div className="container mx-auto px-4">
        <div className="flex flex-wrap justify-between items-center">
          <div className="w-full md:w-1/3 text-center md:text-left mb-4 md:mb-0">
            <Link to={"/"}>
              <img src={Logo} alt="Logo" className="h-20" />
            </Link>
          </div>

          <div className="w-full md:w-1/3 text-center md:text-right">
            <div className="flex justify-center md:justify-end space-x-4">
              <a href="/" className="text-white hover:text-gray-400">
                <FaFacebookF />
              </a>
              <a href="/" className="text-white hover:text-gray-400">
                <FaTwitter />
              </a>
              <a href="/" className="text-white hover:text-gray-400">
                <FaInstagram />
              </a>
              <a href="/" className="text-white hover:text-gray-400">
                <FaLinkedinIn />
              </a>
            </div>
          </div>
        </div>
        <div className="mt-8 text-center text-sm">
          <p>&copy; 2024 DetailingGARAGE. Todos los derechos reservados.</p>
        </div>
        <div className="mt-2 text-center text-sm">
          <p>
            Sistema diseñado por: Roncoroni, Tomás | Ruiz, Francisco |
            Timpanaro, Renzo | Sueiro, Sebastían
          </p>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
