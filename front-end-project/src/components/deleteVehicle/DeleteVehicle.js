import React from "react";
import { deleteVehicleAPI } from "../../utils/APIs/APIs";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const DeleteVehicle = ({ isOpen, onClose, vehicle, onDelete }) => {
  if (!isOpen) return null;

  const handleDelete = async () => {
    if (!vehicle.idVehicle) {
      console.error("vehicle.idVehicle está indefinido");
      return;
    }

    try {
      await deleteVehicleAPI(vehicle.idVehicle);
      onDelete();
      toast.success(
        `Vehículo ${vehicle.brand} ${vehicle.model} eliminado correctamente`
      );
      setTimeout(() => {
        onClose();
      }, 3000);
    } catch (error) {
      console.error("Error al eliminar vehículo:", error);
      toast.error("Error al eliminar vehículo. Por favor, inténtelo de nuevo.");
    }
  };

  return (
    <>
      <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50">
        <div className="bg-white rounded-lg p-6 w-full max-w-md mx-4 dark:bg-gray-900 dark:text-slate-400">
          <h2 className="text-2xl font-bold mb-4">Confirmar Eliminación</h2>
          <p>
            ¿Está seguro de que desea eliminar este vehículo{" "}
            <strong>
              {vehicle.brand} {vehicle.model}
            </strong>
            ?
          </p>
          <div className="flex justify-end mt-4">
            <button
              type="button"
              onClick={onClose}
              className="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded mr-2"
            >
              Cancelar
            </button>
            <button
              type="button"
              onClick={handleDelete}
              className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded"
            >
              Confirmar
            </button>
          </div>
        </div>
      </div>
      <ToastContainer autoClose={3000} />
    </>
  );
};

export default DeleteVehicle;
