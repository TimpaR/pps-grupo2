import { ErrorMessage, Field, Form, Formik } from "formik";
import React, { useEffect, useState } from "react";
import instance from "../../utils/axiosInstance/AxiosInstance";
import { useAuth } from "../../services/authContext/AuthContext";
import { fetchBranches } from "../../utils/APIs/APIs";
import { updateValidationSchema } from "../../utils/validationSchema/ValidationSchema";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const UpdateUser = ({ isOpen, onClose, userEdit, onUpdate }) => {
  const { user } = useAuth();
  const [name, setName] = useState(userEdit.name || "");
  const [lastName, setLastName] = useState(userEdit.lastname || "");
  const [idBranch, setIdBranch] = useState(userEdit.idBranch || "");
  const [phone, setPhone] = useState(userEdit.phone || "");
  const [branches, setBranches] = useState([]);

  useEffect(() => {
    setName(userEdit.name || "");
    setLastName(userEdit.lastname || "");
    setIdBranch(userEdit.idBranch || "");
    setPhone(userEdit.phone || "");
  }, [userEdit]);

  useEffect(() => {
    if (!user || user.userType !== "client") {
      const loadBranches = async () => {
        const branchesData = await fetchBranches();
        setBranches(branchesData);
      };
      loadBranches();
    }
  }, []);

  if (!isOpen) return null;

  const handleSubmit = async () => {
    if (!userEdit.idUser) {
      console.error("user.idUser está indefinido");
      return;
    }

    let url;
    let dataToSend = { name, lastName, phone };

    if (userEdit.userEditType === "employee") {
      url = `/api/User/updateEmployee/${userEdit.idUser}`;
      dataToSend = { ...dataToSend, idBranch };
    } else {
      url = `/api/User/updateUser/${userEdit.idUser}`;
    }

    try {
      await instance.put(url, dataToSend);
      toast.success("Usuario actualizado exitosamente");
      setTimeout(() => {
        onUpdate();
        onClose();
      }, 1500);
    } catch (error) {
      console.error("Error al actualizar usuario:", error);
    }
  };

  return (
    <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50">
      <div className="bg-white rounded-lg p-6 w-full max-w-md mx-4 dark:bg-gray-900">
        <h2 className="text-2xl font-bold mb-4  dark:text-slate-300">
          Actualizar Datos
        </h2>
        <Formik
          initialValues={{ name, lastName, idBranch, phone }}
          validationSchema={updateValidationSchema}
          onSubmit={handleSubmit}
        >
          {({ isSubmitting, setFieldValue }) => (
            <Form>
              <div className="mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2 dark:text-slate-400"
                  htmlFor="name"
                >
                  Nombre
                </label>
                <Field
                  type="text"
                  name="name"
                  id="name"
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-slate-700 dark:text-slate-400"
                />
                <ErrorMessage
                  name="name"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2 dark:text-slate-400"
                  htmlFor="lastName"
                >
                  Apellido
                </label>
                <Field
                  type="text"
                  name="lastName"
                  id="lastName"
                  value={lastName}
                  onChange={(e) => setLastName(e.target.value)}
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-slate-700 dark:text-slate-400"
                />
                <ErrorMessage
                  name="lastName"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2 dark:text-slate-400"
                  htmlFor="phone"
                >
                  Teléfono
                </label>
                <Field
                  type="text"
                  name="phone"
                  id="phone"
                  value={phone}
                  onChange={(e) => setPhone(e.target.value)}
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-slate-700 dark:text-slate-400"
                />
                <ErrorMessage
                  name="phone"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              {branches.length > 0 && userEdit.userType === "employee" && (
                <div className="mb-4">
                  <label
                    className="block text-gray-700 text-sm font-bold mb-2 dark:text-slate-400"
                    htmlFor="idBranch"
                  >
                    Sucursal
                  </label>
                  <Field
                    as="select"
                    name="idBranch"
                    id="idBranch"
                    value={idBranch}
                    onChange={(e) => setIdBranch(e.target.value)}
                    className="hover:border-blue-500 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-slate-700 dark:text-slate-400"
                  >
                    {/* Mostrar la sucursal guardada en la BD */}
                    <option key={userEdit.idBranch} value={userEdit.idBranch}>
                      {
                        branches.find(
                          (branch) => branch.idBranch === userEdit.idBranch
                        )?.location
                      }
                    </option>
                    {/* Mostrar las sucursales restantes */}
                    {branches
                      .filter((branch) => branch.idBranch !== userEdit.idBranch)
                      .map((branch) => (
                        <option key={branch.idBranch} value={branch.idBranch}>
                          {branch.location}
                        </option>
                      ))}
                  </Field>
                  <ErrorMessage
                    name="idBranch"
                    component="div"
                    className="text-red-500 text-sm"
                  />
                </div>
              )}

              <div className="flex justify-end">
                <button
                  type="button"
                  onClick={onClose}
                  className="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded mr-2"
                >
                  Cancelar
                </button>
                <button
                  type="submit"
                  disabled={isSubmitting}
                  className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                >
                  Guardar
                </button>
              </div>
            </Form>
          )}
        </Formik>
      </div>
      <ToastContainer />
    </div>
  );
};

export default UpdateUser;
