import axios from "axios";
import React, { useEffect, useState } from "react";
import { AiFillDelete } from "react-icons/ai";
import { FaStar } from "react-icons/fa";
import DeleteReview from "../deleteReview/DeleteReview";

const ReviewList = () => {
  const [reviews, setReviews] = useState([]);
  const [selectedReview, setSelectedReview] = useState(null);
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);

  useEffect(() => {
    const fetchReviews = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7267/review/GetAll"
        );
        setReviews(response.data);
      } catch (error) {
        console.error("Error al obtener reseñas:", error);
      }
    };
    fetchReviews();
  }, []);

  const handleDeleteClick = (review) => {
    setSelectedReview(review);
    setIsDeleteModalOpen(true);
  };

  const handleDelete = async () => {
    try {
      const response = await axios.get("https://localhost:7267/review/GetAll");
      setReviews(response.data);
    } catch (error) {
      console.error("Error al obtener reseñas:", error);
    }
  };

  return (
    <div className="flex justify-center items-center">
      <div className="w-full p-4 overflow-y-auto bg-gray-100 rounded-lg shadow-lg dark:bg-gray-900">
        {reviews.map((review) => (
          <div
            key={review.idReview}
            className="bg-white rounded-lg shadow-md p-4 mb-4 flex justify-between items-end dark:bg-slate-700 dark:text-slate-300"
          >
            <div>
              <h3 className="text-lg flex mb-2">
                {[...Array(5)].map((_, index) => {
                  const starIndex = index + 1;
                  return (
                    <FaStar
                      key={starIndex}
                      className={`text-2xl ${
                        starIndex <= review.rating
                          ? "text-yellow-500"
                          : "text-gray-400"
                      }`}
                    />
                  );
                })}
              </h3>
              <p className="text-sm">
                <strong>Review ID:</strong> {review.idReview}
              </p>
              <p className="text-sm">
                <strong>Cliente ID:</strong> {review.idClient}
              </p>
              {review.comment.length === 0 ? (
                ""
              ) : (
                <p className="text-sm ">
                  <strong>Comentario:</strong> {review.comment}
                </p>
              )}
            </div>
            <div className="flex space-x-4">
              <AiFillDelete
                className="text-red-500 cursor-pointer"
                onClick={() => handleDeleteClick(review)}
              />
            </div>
          </div>
        ))}
      </div>
      {selectedReview && (
        <>
          <DeleteReview
            isOpen={isDeleteModalOpen}
            onClose={() => setIsDeleteModalOpen(false)}
            review={selectedReview}
            onDelete={handleDelete}
          />
        </>
      )}
    </div>
  );
};

export default ReviewList;
