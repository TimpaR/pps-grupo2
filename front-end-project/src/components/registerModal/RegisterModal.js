import React from "react";
import { FaTimes } from "react-icons/fa";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { registerValidation } from "../../utils/validationSchema/ValidationSchema";
import { registerUser } from "../../utils/APIs/APIs";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const RegisterModal = ({ isOpen, onClose }) => {
  const handleSubmit = async (values, { setSubmitting, setErrors }) => {
    try {
      await registerUser(values);
      toast.success("Registro exitoso");
      setTimeout(() => {
        onClose();
      }, 1500);
    } catch (error) {
      setErrors({ apiError: error.message || "Registration failed" });
    } finally {
      setSubmitting(false);
    }
  };

  if (!isOpen) {
    return null;
  }
  return (
    <div className="fixed inset-0 flex items-center justify-center bg-gray-800 bg-opacity-50">
      <div className="bg-white p-10 rounded-md shadow-md relative w-80 md:h-auto md:overflow-hidden md:rounded-lg">
        <button
          onClick={onClose}
          className="absolute top-2 right-2 text-red-500 hover:text-red-700"
          aria-label="Close"
        >
          <FaTimes />
        </button>
        <h2 className="text-center mb-4">Formulario de registro</h2>
        <Formik
          initialValues={{
            name: "",
            lastname: "",
            email: "",
            phone: "",
            password: "",
          }}
          validationSchema={registerValidation}
          onSubmit={handleSubmit}
        >
          {({ isSubmitting }) => (
            <Form className="space-y-4">
              <Field
                placeholder="Nombre"
                type="text"
                id="name"
                name="name"
                className="mt-1 block w-full border border-gray-300 rounded-md px-2 py-1 shadow-sm focus:ring-blue-500 focus:border-blue-500 sm:text-sm"
              />
              <ErrorMessage
                name="name"
                component="p"
                className="text-red-500 text-sm"
              />
              <Field
                placeholder="Apellido"
                type="text"
                id="lastname"
                name="lastname"
                className="mt-1 block w-full border border-gray-300 rounded-md px-2 py-1 shadow-sm focus:ring-blue-500 focus:border-blue-500 sm:text-sm"
              />
              <ErrorMessage
                name="lastname"
                component="p"
                className="text-red-500 text-sm"
              />
              <Field
                placeholder="Correo electrónico"
                type="email"
                id="email"
                name="email"
                className="mt-1 block w-full border border-gray-300 rounded-md px-2 py-1 shadow-sm focus:ring-blue-500 focus:border-blue-500 sm:text-sm"
              />
              <ErrorMessage
                name="email"
                component="p"
                className="text-red-500 text-sm"
              />
              <Field
                placeholder="Teléfono"
                type="tel"
                id="phone"
                name="phone"
                className="mt-1 block w-full border border-gray-300 rounded-md px-2 py-1 shadow-sm focus:ring-blue-500 focus:border-blue-500 sm:text-sm"
              />
              <ErrorMessage
                name="phone"
                component="p"
                className="text-red-500 text-sm"
              />
              <Field
                placeholder="Contraseña"
                type="password"
                id="password"
                name="password"
                className="mt-1 block w-full border border-gray-300 rounded-md px-2 py-1 shadow-sm focus:ring-blue-500 focus:border-blue-500 sm:text-sm"
              />
              <ErrorMessage
                name="password"
                component="p"
                className="text-red-600 text-sm"
              />

              <button
                type="submit"
                disabled={isSubmitting}
                className="mt-4 bg-blue-500 text-white px-3 py-2 rounded-md w-full"
              >
                Registrarse
              </button>
              <ErrorMessage
                name="apiError"
                component="p"
                className="text-red-500 text-sm"
              />
            </Form>
          )}
        </Formik>
      </div>
      <ToastContainer />
    </div>
  );
};

export default RegisterModal;
