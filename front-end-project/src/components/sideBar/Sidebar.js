import { FaUsers, FaRegCalendarAlt, FaStar } from "react-icons/fa";
import { BsBuilding, BsGraphUp } from "react-icons/bs";
import { MdMiscellaneousServices } from "react-icons/md";
import { IoCarSport } from "react-icons/io5";

import { Link } from "react-router-dom";
import { useAuth } from "../../services/authContext/AuthContext";

const Sidebar = ({ setSelectedOption, selectedOption }) => {
  const { user } = useAuth();

  const handleLinkClick = (link) => {
    setSelectedOption(link);
  };

  return (
    <div className="h-full bg-gray-900">
      <div className="flex flex-col items-center w-56 h-full overflow-hidden text-gray-400">
        <div className="w-full px-2 flex-grow">
          {user.userType === "admin" && (
            <>
              <Link
                className={`flex items-center w-full h-12 px-3 mt-2 rounded hover:bg-gray-700 hover:text-gray-300 ${
                  selectedOption === "usuarios" && "bg-gray-700 text-white"
                }`}
                href="#"
                onClick={() => handleLinkClick("usuarios")}
              >
                <FaUsers className="w-6 h-6" />
                <span className="ml-4 text-sm font-medium">Usuarios</span>
              </Link>
              <Link
                className={`flex items-center w-full h-12 px-3 mt-2 rounded hover:bg-gray-700 hover:text-gray-300 ${
                  selectedOption === "sucursales" && "bg-gray-700 text-white"
                }`}
                href="#"
                onClick={() => handleLinkClick("sucursales")}
              >
                <BsBuilding className="w-6 h-6" />
                <span className="ml-4 text-sm font-medium">Sucursales</span>
              </Link>
              <Link
                className={`flex items-center w-full h-12 px-3 mt-2 rounded hover:bg-gray-700 hover:text-gray-300 ${
                  selectedOption === "services" && "bg-gray-700 text-white"
                }`}
                href="#"
                onClick={() => handleLinkClick("services")}
              >
                <MdMiscellaneousServices className="w-6 h-6" />
                <span className="ml-4 text-sm font-medium">Servicios</span>
              </Link>
              <Link
                className={`flex items-center w-full h-12 px-3 mt-2 rounded hover:bg-gray-700 hover:text-gray-300 ${
                  selectedOption === "reseñas" && "bg-gray-700 text-white"
                }`}
                href="#"
                onClick={() => handleLinkClick("reseñas")}
              >
                <FaStar className="w-6 h-6" />
                <span className="ml-4 text-sm font-medium">Reseñas</span>
              </Link>
            </>
          )}
          {user.userType === "employee" && (
            <>
              <Link
                className={`flex items-center w-full h-12 px-3 mt-2 rounded hover:bg-gray-700 hover:text-gray-300 ${
                  selectedOption === "turnos" && "bg-gray-700 text-white"
                }`}
                href="#"
                onClick={() => handleLinkClick("turnos")}
              >
                <FaRegCalendarAlt className="w-6 h-6" />
                <span className="ml-4 text-sm font-medium">Turnos</span>
              </Link>
            </>
          )}
          <Link
            className={`flex items-center w-full h-12 px-3 mt-2 rounded hover:bg-gray-700 hover:text-gray-300 ${
              selectedOption === "vehiculos" && "bg-gray-700 text-white"
            }`}
            href="#"
            onClick={() => handleLinkClick("vehiculos")}
          >
            <IoCarSport className="w-6 h-6" />
            <span className="ml-4 text-sm font-medium">Vehículos</span>
          </Link>
          <Link
            className={`flex items-center w-full h-12 px-3 mt-2 rounded hover:bg-gray-700 hover:text-gray-300 ${
              selectedOption === "estadisticas" && "bg-gray-700 text-white"
            }`}
            href="#"
            onClick={() => handleLinkClick("estadisticas")}
          >
            <BsGraphUp className="w-6 h-6" />
            <span className="ml-4 text-sm font-medium">Estadisticas</span>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Sidebar;
