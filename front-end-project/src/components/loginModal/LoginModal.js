import { FaTimes } from "react-icons/fa";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { loginValidation } from "../../utils/validationSchema/ValidationSchema";
import { loginUser } from "../../utils/APIs/APIs";
import { useAuth } from "../../services/authContext/AuthContext";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const LoginModal = ({ isOpen, onClose, onRegisterClick }) => {
  const { login } = useAuth();

  const handleSubmit = async (values, { setSubmitting, setErrors }) => {
    try {
      const data = await loginUser(values);
      login(data);
      toast.success("Inicio de sesión exitoso", {
        onClose: () => {
          setTimeout(() => {
            onClose();
          }, 2000);
        },
      });
    } catch (error) {
      if (error.inner) {
        error.inner.forEach((err) => {
          toast.error(err.message, { toastId: err.path });
        });
      } else {
        toast.error(error.message || "Inicio de sesión fallido");
      }
    } finally {
      setSubmitting(false);
    }
  };

  if (!isOpen) {
    return null;
  }

  return (
    <div className="fixed inset-0 flex items-center justify-center bg-gray-800 bg-opacity-50">
      <div className="bg-white p-10 rounded-md shadow-md relative w-80 md:h-auto md:overflow-hidden md:rounded-lg">
        <button
          onClick={onClose}
          className="absolute top-2 right-2 text-red-500 hover:text-red-700"
          aria-label="Cerrar"
        >
          <FaTimes />
        </button>
        <h2 className="mb-4 text-center">Inicio de sesión</h2>
        <Formik
          initialValues={{
            email: "",
            password: "",
          }}
          validationSchema={loginValidation}
          onSubmit={handleSubmit}
        >
          {({ isSubmitting }) => (
            <Form className="space-y-4">
              <Field
                placeholder="Email"
                type="email"
                id="email"
                name="email"
                className="mt-1 block w-full border border-gray-300 rounded-md px-2 py-1 shadow-sm focus:ring-blue-500 focus:border-blue-500 sm:text-sm"
              />
              <ErrorMessage
                name="email"
                component="p"
                className="text-red-500 text-sm"
              />
              <Field
                placeholder="Contraseña"
                type="password"
                id="password"
                name="password"
                className="mt-1 block w-full border border-gray-300 rounded-md px-2 py-1 shadow-sm focus:ring-blue-500 focus:border-blue-500 sm:text-sm"
              />
              <ErrorMessage
                name="password"
                component="p"
                className="text-red-500 text-sm"
              />

              <button
                type="submit"
                disabled={isSubmitting}
                className="mt-4 bg-blue-500 text-white px-3 py-2 rounded-md w-full"
              >
                Iniciar Sesión
              </button>
              <ErrorMessage
                name="apiError"
                component="p"
                className="text-red-500 text-sm"
              />
            </Form>
          )}
        </Formik>
        <div className="mt-4 text-center">
          <p className="text-sm text-gray-600">
            ¿No tienes una cuenta?{" "}
            <button
              onClick={onRegisterClick}
              className="text-blue-500 hover:underline"
            >
              Registrarse
            </button>
          </p>
        </div>
      </div>
      <ToastContainer />
    </div>
  );
};

export default LoginModal;
