import React, { useEffect, useState } from "react";
import { employeeValidationSchema } from "../../utils/validationSchema/ValidationSchema";
import { ErrorMessage, Field, Form, Formik } from "formik";
import { addEmployee, fetchBranches } from "../../utils/APIs/APIs";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const AddEmployee = ({ isModalOpen, setIsModalOpen }) => {
  const [branches, setBranches] = useState([]);

  useEffect(() => {
    const loadBranches = async () => {
      const branchesData = await fetchBranches();
      setBranches(branchesData);
    };

    loadBranches();
  }, []);

  const handleSubmit = async (values, { resetForm }) => {
    try {
      const response = await addEmployee(values);
      if (response) {
        toast.success("Empleado añadido exitosamente");
        setTimeout(() => {
          setIsModalOpen(false);
          resetForm();
        }, 3000);
      }
    } catch (error) {
      toast.error(`Error al añadir empleado: ${error.message}`, {
        autoClose: 5000,
      });
    }
  };

  if (!isModalOpen) return null;

  return (
    <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50">
      <div className="bg-white rounded-lg p-6 w-full max-w-md mx-4 dark:bg-gray-900">
        <h2 className="text-2xl font-bold mb-4  dark:text-slate-300">
          Añadir Empleado
        </h2>
        <Formik
          initialValues={{
            name: "",
            lastName: "",
            email: "",
            phone: "",
            password: "",
            idBranch: "",
          }}
          validationSchema={employeeValidationSchema}
          onSubmit={handleSubmit}
        >
          {({ isSubmitting }) => (
            <Form>
              <div className="mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2 dark:text-slate-400"
                  htmlFor="name"
                >
                  Nombre
                </label>
                <Field
                  type="text"
                  name="name"
                  id="name"
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-slate-700 dark:text-slate-400"
                />
                <ErrorMessage
                  name="name"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2  dark:text-slate-400"
                  htmlFor="lastName"
                >
                  Apellido
                </label>
                <Field
                  type="text"
                  name="lastName"
                  id="lastName"
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-slate-700 dark:text-slate-400"
                />
                <ErrorMessage
                  name="lastName"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2 dark:text-slate-400"
                  htmlFor="email"
                >
                  Email
                </label>
                <Field
                  type="text"
                  name="email"
                  id="email"
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-slate-700 dark:text-slate-400"
                />
                <ErrorMessage
                  name="email"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2  dark:text-slate-400"
                  htmlFor="phone"
                >
                  Teléfono
                </label>
                <Field
                  type="text"
                  name="phone"
                  id="phone"
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-slate-700 dark:text-slate-400"
                />
                <ErrorMessage
                  name="phone"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2 dark:text-slate-400"
                  htmlFor="password"
                >
                  Contraseña
                </label>
                <Field
                  type="password"
                  name="password"
                  id="password"
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-slate-700 dark:text-slate-400"
                />
                <ErrorMessage
                  name="password"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2  dark:text-slate-400"
                  htmlFor="idBranch"
                >
                  Sucursal
                </label>
                <Field
                  as="select"
                  name="idBranch"
                  id="idBranch"
                  className=" hover:border-blue-500 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-slate-700 dark:text-slate-400"
                >
                  <option value="" label="Selecciona una sucursal" />
                  {branches.map((branch) => (
                    <option key={branch.idBranch} value={branch.idBranch}>
                      {branch.location}
                    </option>
                  ))}
                </Field>

                <ErrorMessage
                  name="idBranch"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="flex justify-end">
                <button
                  type="button"
                  onClick={() => setIsModalOpen(false)}
                  className="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded mr-2"
                >
                  Cancelar
                </button>
                <button
                  type="submit"
                  disabled={isSubmitting}
                  className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                >
                  Guardar
                </button>
              </div>
            </Form>
          )}
        </Formik>
      </div>
      <ToastContainer />
    </div>
  );
};

export default AddEmployee;
