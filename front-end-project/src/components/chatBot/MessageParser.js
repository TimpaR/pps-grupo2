import React from "react";

const MessageParser = ({ children, actions }) => {
  const parse = (message) => {
    console.log(message);
    if (message.includes(`hola`)) {
      actions.handleName();
    }
    if (message.includes(`1`)) {
      actions.handlePhone();
    }
    if (message.includes(`4`)) {
      actions.handleDevs();
    }
    if (message.includes(`chau`) || message.includes(`saludos`)) {
      actions.handleBye();
    }
    if (message.includes("3")) {
      actions.handleAbout();
    }
    if (message.includes("2")) {
      actions.handleServices();
    }
  };

  return (
    <div>
      {React.Children.map(children, (child) => {
        return React.cloneElement(child, {
          parse: parse,
          actions,
        });
      })}
    </div>
  );
};

export default MessageParser;
