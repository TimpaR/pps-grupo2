import { createChatBotMessage } from "react-chatbot-kit";

const config = {
  initialMessages: [
    createChatBotMessage(
      <>
        <p>Bienvenido a Detaling Garage, escoge una opción para continuar:</p>
        <ul>
          <li>1.- Teléfono</li>
          <li>2.- Servicios</li>
          <li>3.- Empresa</li>
          <li>4.- Desarrolladores</li>
        </ul>
        <p>Elija una opción:</p>
      </>
    ),
  ],
  botName: `Detailing Garage`,
  customStyles: {
    botMessageBox: { backgroundColor: "#374151" },
    chatButton: { backgroundColor: "#374151" },
    position: "fixed",
    rigth: "25px",
    bottom: "50px",
    boxShadow: "rgba(100,100,111, 0.2) 0px 7px 29px 0px",
  },
  customComponents: {
    header: () => (
      <div className="p-4 bg-gray-700 text-white rounded-l">
        Detailing Garage
      </div>
    ),
  },
};

export default config;
