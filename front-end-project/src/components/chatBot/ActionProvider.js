import React from "react";

const ActionProvider = ({ createChatBotMessage, setState, children }) => {
  const handleName = () => {
    const botMessage = createChatBotMessage(
      `¡Hola!, un gusto en conocerte, en qué puedo ayudarte?`
    );
    setState((prev) => ({ ...prev, messages: [...prev.messages, botMessage] }));
  };
  const handlePhone = () => {
    const botMessage = createChatBotMessage(
      `Nuestro número telefónico 341-6456674`
    );
    setState((prev) => ({ ...prev, messages: [...prev.messages, botMessage] }));
  };
  const handleDevs = () => {
    const botMessage = createChatBotMessage(
      <>
        <p>Los desarrolladores de la aplicación web son:</p>
        <ul>
          <li>-Roncoroni, Tomas</li>
          <li>-Ruiz, Francisco</li>
          <li>-Sueiro, Sebastian</li>
          <li>-Timpanaro, Renzo</li>
        </ul>
      </>
    );
    setState((prev) => ({ ...prev, messages: [...prev.messages, botMessage] }));
  };
  const handleBye = () => {
    const botMessage = createChatBotMessage(
      `Espero que mi ayude te haya servido, deja una reseña en la pagina para puntear el servicio. Saludos!!`
    );
    setState((prev) => ({ ...prev, messages: [...prev.messages, botMessage] }));
  };
  const handleAbout = () => {
    const botMessage = createChatBotMessage(
      `Somos una empresa especializada en detailing automotriz, dedicada a resaltar la belleza y proteger la inversión de tu vehículo. Con un equipo apasionado por el detalle`
    );
    setState((prev) => ({ ...prev, messages: [...prev.messages, botMessage] }));
  };
  const handleServices = () => {
    const botMessage = createChatBotMessage(
      `Brindamos una amplia variedad de servicios, algunos son ploteo, polarizado, lavado profundo  y lavado interior. Si quieres conocer mas tenemos un apartado en el cual te mostrara todos los servicios disponibles`
    );
    setState((prev) => ({ ...prev, messages: [...prev.messages, botMessage] }));
  };
  return (
    <div>
      {React.Children.map(children, (child) => {
        return React.cloneElement(child, {
          actions: {
            handleName,
            handlePhone,
            handleDevs,
            handleBye,
            handleAbout,
            handleServices,
          },
        });
      })}
    </div>
  );
};

export default ActionProvider;
