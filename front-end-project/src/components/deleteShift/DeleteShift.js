import React, { useEffect, useState } from "react";
import axios from "axios";
import { deleteShiftAPI } from "../../utils/APIs/APIs";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const DeleteShift = ({ isOpen, onClose, shift, onDelete }) => {
  const [services, setServices] = useState([]);
  const [branches, setBranches] = useState([]);

  useEffect(() => {
    const fetchServices = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7267/api/Service/getAllServices"
        );
        setServices(response.data);
      } catch (error) {
        console.error("Error fetching services:", error);
      }
    };

    fetchServices();
  }, []);

  useEffect(() => {
    const fetchBranches = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7267/api/Branches/getAllBranchs"
        );
        setBranches(response.data);
      } catch (error) {
        console.error("Error al obtener sucursales:", error);
      }
    };

    fetchBranches();
  }, []);

  const getBranchName = (id) => {
    const branch = branches.find((branch) => branch.idBranch === id);
    return branch ? branch.location : "N/A";
  };

  const getServiceName = (id) => {
    const service = services.find((service) => service.idServicio === id);
    return service ? service.name : "N/A";
  };

  const formatDate = (dateString) => {
    const [year, month, day] = dateString.split("-");
    return `${day}/${month}/${year}`;
  };

  const formatTime = (timeString) => {
    const [hours, minutes] = timeString.split(":");
    return `${hours}:${minutes} hs`;
  };

  if (!isOpen) return null;

  const handleDelete = async () => {
    if (!shift.idShift) {
      return;
    }
    try {
      await deleteShiftAPI(shift.idShift);
      onDelete();
      toast.success("Turno eliminado correctamente", {
        autoClose: 3000,
      });
      setTimeout(() => {
        onClose();
      }, 3000);
    } catch (error) {
      console.error("Error al eliminar el turno:", error);
      toast.error(
        "Error al eliminar el turno. Por favor, inténtelo de nuevo.",
        {
          autoClose: 3000,
        }
      );
    }
  };

  return (
    <>
      <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50">
        <div className="bg-white rounded-lg p-6 w-full max-w-md mx-4">
          <h2 className="text-2xl font-bold mb-4">Confirmar Eliminación</h2>
          <p>
            ¿Está seguro de que desea eliminar el turno de{" "}
            <strong>{getServiceName(shift.idService)}</strong> el día{" "}
            <strong>{formatDate(shift.date)}</strong> a las{" "}
            <strong>{formatTime(shift.startTime)}</strong> hasta{" "}
            <strong>{formatTime(shift.endTime)}</strong> en{" "}
            <strong>{getBranchName(shift.idBranch)}</strong>?
          </p>
          <div className="flex justify-end mt-4">
            <button
              type="button"
              onClick={onClose}
              className="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded mr-2"
            >
              Cancelar
            </button>
            <button
              type="button"
              onClick={handleDelete}
              className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded"
            >
              Confirmar
            </button>
          </div>
        </div>
      </div>
      <ToastContainer autoClose={3000} />
    </>
  );
};

export default DeleteShift;
