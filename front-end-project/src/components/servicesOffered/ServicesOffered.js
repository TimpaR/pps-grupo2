import React from "react";
import Carousel from "../carousel/Carousel";

const ServicesOffered = () => {
  return (
    <div id="services-section" className="text-center dark:text-slate-400">
      <h2 className="text-3xl font-bold mb-4">Nuestros servicios</h2>
      <Carousel />
    </div>
  );
};

export default ServicesOffered;
