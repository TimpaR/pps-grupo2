import React from "react";
import { deleteUser } from "../../utils/APIs/APIs";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const DeleteUser = ({ isOpen, onClose, user, onDelete }) => {
  if (!isOpen) return null;

  const handleDelete = async () => {
    if (!user.idUser) {
      console.error("user.idUser está indefinido");
      return;
    }

    try {
      await deleteUser(user.idUser);
      onDelete();
      toast.success(
        `Usuario ${user.name} ${user.lastname} eliminado correctamente`
      );
      setTimeout(() => {
        onClose();
      }, 3000);
    } catch (error) {
      toast.error("Error al eliminar usuario. Por favor, inténtelo de nuevo.");
    }
  };

  return (
    <>
      <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50">
        <div className="bg-white rounded-lg p-6 w-full max-w-md mx-4 dark:bg-gray-900">
          <h2 className="text-2xl font-bold mb-4 dark:text-slate-300">
            Confirmar Eliminación
          </h2>
          <p className="dark:text-slate-400">
            ¿Está seguro de que desea eliminar este usuario{" "}
            <strong>
              {user.name} {user.lastname}
            </strong>
            ?
          </p>
          <div className="flex justify-end mt-4">
            <button
              type="button"
              onClick={onClose}
              className="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded mr-2"
            >
              Cancelar
            </button>
            <button
              type="button"
              onClick={handleDelete}
              className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded"
            >
              Confirmar
            </button>
          </div>
        </div>
      </div>
      <ToastContainer autoClose={3000} />
    </>
  );
};

export default DeleteUser;
