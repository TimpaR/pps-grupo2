import axios from "axios";
import { updateShiftAPI } from "../../utils/APIs/APIs";
import React, { useEffect, useState } from "react";
import { Formik, Form } from "formik";
import { useAuth } from "../../services/authContext/AuthContext";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const DeleteMyShift = ({ isOpen, onClose, shift }) => {
  const { user } = useAuth();
  const [services, setServices] = useState([]);

  useEffect(() => {
    const fetchVehicles = async () => {
      try {
        await axios.get(
          `https://localhost:7267/api/Vehicle?userId=${user.idUser}`
        );
      } catch (error) {
        console.error("Error al obtener los vehiculos:", error);
      }
    };

    fetchVehicles();
  }, [user]);

  useEffect(() => {
    const fetchServices = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7267/api/Service/getAllServices"
        );
        setServices(response.data);
      } catch (error) {
        console.error("Error fetching services:", error);
      }
    };

    fetchServices();
  }, []);

  const initialValues = {
    idVehicle: shift ? shift.idVehicle : "",
    idBranch: shift ? shift.idBranch : "",
    idService: shift ? shift.idService : "",
  };

  const handleSubmit = async (values, { setSubmitting }) => {
    try {
      await updateShiftAPI(
        shift.idShift,
        null,
        values.idBranch,
        values.idService
      );
      toast.success("Turno eliminado correctamente", {
        onClose: onClose,
      });
    } catch (error) {
      toast.error("Error al eliminar el turno. Por favor, inténtelo de nuevo.");
    } finally {
      setSubmitting(false);
    }
  };

  const getServiceName = (id) => {
    const service = services.find((service) => service.idServicio === id);
    return service ? service.name : "N/A";
  };

  const formatDate = (dateString) => {
    const date = new Date(dateString);
    const options = { weekday: "long", day: "numeric", month: "long" };
    const formattedDate = date.toLocaleDateString("es-ES", options);

    return formattedDate.replace(/\b\w/g, (char) => char.toUpperCase());
  };

  if (!isOpen || !shift) return null;

  return (
    <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50">
      <div className="bg-white rounded-lg p-6 w-full max-w-md mx-4">
        <h2 className="text-2xl font-bold mb-4">Eliminar Turno</h2>
        <p>
          ¿Estás seguro que quieres eliminar el turno de{" "}
          <strong>{getServiceName(shift.idService)}</strong> el día{" "}
          <strong>{formatDate(shift.date)}</strong>?
        </p>
        <Formik initialValues={initialValues} onSubmit={handleSubmit}>
          {({ isSubmitting }) => (
            <Form>
              <div className="flex justify-end">
                <button
                  type="button"
                  onClick={onClose}
                  className="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded mr-2"
                >
                  Cancelar
                </button>
                <button
                  type="submit"
                  disabled={isSubmitting}
                  className="bg-red-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                >
                  Confirmar
                </button>
              </div>
            </Form>
          )}
        </Formik>
      </div>
      <ToastContainer />
    </div>
  );
};

export default DeleteMyShift;
