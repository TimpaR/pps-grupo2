import React, { useEffect, useState } from "react";
import { FaBars } from "react-icons/fa";
import { LuUserCircle2 } from "react-icons/lu";
import { MdOutlineLightMode, MdOutlineDarkMode } from "react-icons/md";
import LoginModal from "../loginModal/LoginModal";
import Logo from "../../assets/LogoBlack.png";
import { useAuth } from "../../services/authContext/AuthContext";
import RegisterModal from "../registerModal/RegisterModal";
import { Link } from "react-router-dom";

const Navbar = () => {
  const { authToken, user, logout } = useAuth();
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const [isLoginModalOpen, setIsLoginModalOpen] = useState(false);
  const [isRegisterModalOpen, setIsRegisterModalOpen] = useState(false);
  const [isUserDropdownOpen, setIsUserDropdownOpen] = useState(false);
  const [theme, setTheme] = useState("light");

  const handleChangeTheme = () => {
    setTheme((prevTheme) => (prevTheme == "light" ? "dark" : "light"));
  };

  useEffect(() => {
    const savedTheme = localStorage.getItem("theme");
    if (savedTheme) {
      setTheme(savedTheme);
    }
  }, []);

  useEffect(() => {
    localStorage.setItem("theme", theme);
    applyTheme(theme);
  }, [theme]);

  const applyTheme = (theme) => {
    if (theme === "dark") {
      document.documentElement.classList.add("dark");
    } else {
      document.documentElement.classList.remove("dark");
    }
  };

  const toggleMenu = () => {
    setIsMenuOpen(!isMenuOpen);
  };

  const toggleLoginModal = () => {
    setIsLoginModalOpen(!isLoginModalOpen);
  };

  const toggleRegisterModal = () => {
    setIsLoginModalOpen(false);
    setIsRegisterModalOpen(!isRegisterModalOpen);
  };

  const toggleUserDropdown = () => {
    setIsUserDropdownOpen(!isUserDropdownOpen);
  };

  const closeSession = () => {
    logout();
    toggleUserDropdown();
  };

  const handleScrollToSection = (id) => {
    const element = document.getElementById(id);
    if (element) {
      element.scrollIntoView({ behavior: "smooth" });
    }
  };

  return (
    <nav className="bg-cyan-500 p-6 lg:p-0">
      <div className="flex items-center justify-between">
        <div className="text-white font-bold md:block hidden ">
          <Link to={"/"}>
            <img src={Logo} alt="Logo" className="h-20" />
          </Link>
        </div>
        <div className="md:hidden">
          <button className="text-white " onClick={toggleMenu}>
            <FaBars />
          </button>
        </div>
        <button className="text-white ml-6" onClick={handleChangeTheme}>
          {theme === "light" ? (
            <MdOutlineDarkMode style={{ fontSize: "1.5rem", color: "black" }} />
          ) : (
            <MdOutlineLightMode style={{ fontSize: "1.5rem" }} />
          )}
        </button>
        <div className="flex-grow flex justify-center">
          <div className="hidden md:flex space-x-4">
            {!authToken ||
            (user?.userType !== "admin" && user?.userType !== "employee") ? (
              <>
                <a href="/" className="text-white ">
                  Inicio
                </a>
                <Link
                  onClick={() => handleScrollToSection("services-section")}
                  className="text-white"
                >
                  Servicios
                </Link>
                <Link
                  onClick={() => handleScrollToSection("review-section")}
                  className="text-white"
                >
                  Reseñas
                </Link>
                {authToken && user?.userType === "client" ? (
                  <a href="/clientShift" className="text-white">
                    Turnos
                  </a>
                ) : null}
              </>
            ) : null}
          </div>
        </div>
        <div className="flex items-center">
          {authToken ? (
            <div className="relative">
              <div className="flex items-center">
                <span className="text-white text-lg p-5">
                  Hola, {user?.name}
                </span>
                <button
                  className="text-white mr-6"
                  onClick={toggleUserDropdown}
                >
                  <LuUserCircle2 className="text-3xl" />
                </button>
              </div>
              {isUserDropdownOpen && (
                <div className="absolute right-0 mt-2 w-48 bg-gray-400 rounded-md shadow-lg">
                  {user.userType === "client" && (
                    <>
                      <a href="/myData" className="block px-4 py-2 text-white ">
                        Mis datos
                      </a>
                      <a
                        href="/clientvehicles"
                        className="block px-4 py-2 text-white"
                      >
                        Mis vehículos
                      </a>
                      <a href="/myShift" className="block px-4 py-2 text-white">
                        Mis turnos
                      </a>
                    </>
                  )}
                  <button
                    onClick={closeSession}
                    className="block px-4 py-2 text-white w-full text-left"
                  >
                    Cerrar Sesión
                  </button>
                </div>
              )}
            </div>
          ) : (
            <button
              className="bg-blue-500 text-white px-3 py-2 mr-6 rounded-md"
              onClick={toggleLoginModal}
            >
              Iniciar Sesión
            </button>
          )}
        </div>
      </div>

      {/* Menú hamburguesa para dispositivos móviles */}
      {isMenuOpen && (
        <div className="md:hidden mt-2">
          <a href="/" className="block text-white">
            Inicio
          </a>
          <Link
            onClick={() => handleScrollToSection("services-section")}
            className="block text-white"
          >
            Servicios
          </Link>
          <Link
            onClick={() => handleScrollToSection("review-section")}
            className="text-white"
          >
            Reseñas
          </Link>
        </div>
      )}

      <LoginModal
        isOpen={isLoginModalOpen}
        onClose={toggleLoginModal}
        onRegisterClick={toggleRegisterModal}
      />
      <RegisterModal
        isOpen={isRegisterModalOpen}
        onClose={toggleRegisterModal}
      />
    </nav>
  );
};

export default Navbar;
