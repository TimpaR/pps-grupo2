import React, { useState } from "react";
import AddVehicleClient from "../addVehicleClient/AddVehicleClient";

const ClientVehicles = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const openModal = () => {
    setIsModalOpen(true);
  };

  return (
    <div className="w-full h-full flex flex-col mt-2">
      <div className="mb-4 flex justify-between items-center">
        <h2 className="ml-2 text-2xl font-bold text-gray-800 dark:text-slate-300">
          Listado de Vehiculos
        </h2>
        <button
          onClick={openModal}
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none mr-2 "
        >
          Agregar Vehiculo
        </button>
      </div>
      <div className="flex-1 overflow-auto"></div>
      {isModalOpen && (
        <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50">
          <AddVehicleClient
            isModalOpen={isModalOpen}
            setIsModalOpen={setIsModalOpen}
          />
        </div>
      )}
    </div>
  );
};

export default ClientVehicles;
