import React, { useState } from "react";
import AddShift from "../addShift/AddShift";
import ShiftList from "../shiftList/ShiftList";

const Shift = () => {
    const [isModalOpen, setIsModalOpen] = useState(false);

    const openModal = () => {
        setIsModalOpen(true);
      };


  return (
    <div className="w-full h-full flex flex-col">
        <div className="mb-4 flex justify-between items-center">
        <h2 className="text-2xl font-bold text-gray-800">
          Listado de Turnos
        </h2>
        <button
          onClick={openModal}
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none"
        >
          Añadir Turno
        </button>
        </div>
        <div className="flex-1 overflow-auto">
        <ShiftList />
      </div>
        {isModalOpen && (
        <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50">
          <AddShift
            isModalOpen={isModalOpen}
            setIsModalOpen={setIsModalOpen}
          />
        </div>
      )}
    </div>
  )
}

export default Shift