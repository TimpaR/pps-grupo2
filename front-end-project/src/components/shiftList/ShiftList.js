import React, { useEffect, useState } from "react";
import axios from "axios";
import { AiFillDelete, AiFillEdit } from "react-icons/ai";
import UpdateShift from "../updateShift/UpdateShift";
import DeleteShift from "../deleteShift/DeleteShift";

const ShiftList = () => {
  const [shifts, setShifts] = useState([]);
  const [users, setUsers] = useState([]);
  const [services, setServices] = useState([]);
  const [branches, setBranches] = useState([]);
  const [vehicles, setVehicles] = useState([]);
  const [selectedShift, setSelectedShift] = useState(null);
  const [isUpdateModalOpen, setIsUpdateModalOpen] = useState(false);
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);

  const fetchShifts = async () => {
    try {
      const response = await axios.get(
        "https://localhost:7267/api/Shift/getAllShift"
      );
      setShifts(response.data);
    } catch (error) {
      console.error("Error al obtener los turnos:", error);
    }
  };

  const fetchVehicles = async (idVehicle) => {
    try {
      const response = await axios.get(
        `https://localhost:7267/api/Vehicle/getVehicleById/${idVehicle}`
      );
      setVehicles((prevVehicles) => [...prevVehicles, response.data]);
    } catch (error) {
      console.error("Error al obtener los vehículos:", error);
    }
  };

  useEffect(() => {
    fetchShifts();
  }, []);

  useEffect(() => {
    shifts.forEach((shift) => {
      if (shift.idVehicle) {
        fetchVehicles(shift.idVehicle);
      }
    });
  }, [shifts]);

  useEffect(() => {
    const fetchUsers = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7267/api/User/GetAllUsers"
        );
        setUsers(response.data);
      } catch (error) {
        console.error("Error al obtener usuarios:", error);
      }
    };

    fetchUsers();
  }, []);

  useEffect(() => {
    const fetchServices = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7267/api/Service/getAllServices"
        );
        setServices(response.data);
      } catch (error) {
        console.error("Error fetching services:", error);
      }
    };

    fetchServices();
  }, []);

  useEffect(() => {
    const fetchBranches = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7267/api/Branches/getAllBranchs"
        );
        setBranches(response.data);
      } catch (error) {
        console.error("Error al obtener sucursales:", error);
      }
    };

    fetchBranches();
  }, []);

  const handleEditClick = (shift) => {
    setSelectedShift(shift);
    setIsUpdateModalOpen(true);
  };

  const handleDeleteClick = (shift) => {
    setSelectedShift(shift);
    setIsDeleteModalOpen(true);
  };

  const formatDate = (dateString) => {
    const [year, month, day] = dateString.split("-");
    return `${day}/${month}/${year}`;
  };

  const formatTime = (timeString) => {
    const [hours, minutes] = timeString.split(":");
    return `${hours}:${minutes} hs`;
  };

  const getClientName = (id) => {
    const vehicle = vehicles.find((vehicle) => vehicle.idVehicle === id);
    if (vehicle) {
      const user = users.find((user) => user.idUser === vehicle.idClient);
      return user ? `${user.name} ${user.lastname}` : "N/A";
    }
    return "N/A";
  };

  const getServiceName = (id) => {
    const service = services.find((service) => service.idServicio === id);
    return service ? service.name : "N/A";
  };

  const getBranchName = (id) => {
    const branch = branches.find((branch) => branch.idBranch === id);
    return branch ? branch.location : "N/A";
  };

  const getVehicleName = (id) => {
    const vehicle = vehicles.find((vehicle) => vehicle.idVehicle === id);
    return vehicle ? `${vehicle.brand} ${vehicle.model}` : "N/A";
  };

  const handleUpdate = async () => {
    fetchShifts();
  };

  const handleDelete = async () => {
    fetchShifts();
  };

  return (
    <div className="flex justify-center items-center">
      <div className="w-full p-4 overflow-y-auto bg-gray-100 rounded-lg shadow-lg dark:bg-gray-900">
        {shifts.length === 0 ? (
          <p className="text-center text-gray-500 dark:text-slate-400">
            No hay sucursales cargadas.
          </p>
        ) : (
          shifts.map((shift) => (
            <div
              key={shift.idShift}
              className="bg-white rounded-lg shadow-md p-4 mb-4 flex justify-between items-end dark:bg-slate-700 dark:text-slate-300"
            >
              <div>
                <h3 className="text-lg font-semibold">{shift.name}</h3>
                <p className="text-sm">
                  <strong>Día:</strong> {formatDate(shift.date)}
                </p>
                <p className="text-sm">
                  <strong>Comienzo del Turno:</strong>{" "}
                  {formatTime(shift.startTime)}
                </p>
                <p className="text-sm">
                  <strong>Fin del Turno:</strong> {formatTime(shift.endTime)}
                </p>
                <p className="text-sm">
                  <strong>Servicio:</strong> {getServiceName(shift.idService)}
                </p>
                <p className="text-sm">
                  <strong>Sucursal:</strong> {getBranchName(shift.idBranch)}
                </p>
                <p className="text-sm">
                  <strong>Cliente:</strong> {getClientName(shift.idVehicle)}
                </p>
                <p className="text-sm">
                  <strong>Vehículo del cliente:</strong>{" "}
                  {getVehicleName(shift.idVehicle)}
                </p>
              </div>
              <div className="flex space-x-4">
                <AiFillEdit
                  className="text-blue-500 cursor-pointer"
                  onClick={() => handleEditClick(shift)}
                />
                <AiFillDelete
                  className="text-red-500 cursor-pointer"
                  onClick={() => handleDeleteClick(shift)}
                />
              </div>
            </div>
          ))
        )}
      </div>
      {selectedShift && (
        <>
          <UpdateShift
            isOpen={isUpdateModalOpen}
            onClose={() => setIsUpdateModalOpen(false)}
            shift={selectedShift}
            onUpdate={handleUpdate}
          />
          <DeleteShift
            isOpen={isDeleteModalOpen}
            onClose={() => setIsDeleteModalOpen(false)}
            shift={selectedShift}
            onDelete={handleDelete}
          />
        </>
      )}
    </div>
  );
};

export default ShiftList;
