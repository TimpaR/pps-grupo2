import React, { useState, useEffect } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { vehicleValidationSchema } from "../../utils/validationSchema/ValidationSchema";
import { createVehicle, fetchClients } from "../../utils/APIs/APIs";

const AddVehicle = ({ isModalOpen, setIsModalOpen }) => {
  const [clients, setClients] = useState([]);

  useEffect(() => {
    const getClients = async () => {
      try {
        const response = await fetchClients();
        setClients(response.data);
        const filteredClients = response.data.filter(
          (client) => client.userType === "client"
        );
        setClients(filteredClients);
        console.log("Clientes filtrados:", filteredClients);
      } catch (error) {
        console.error("Error al obtener los clientes:", error);
      }
    };

    if (isModalOpen) {
      getClients();
    }
  }, [isModalOpen]);

  const handleSubmit = async (values, { resetForm }) => {
    const modifiedValues = {
      ...values,
      idUser: parseInt(values.idClient, 10),
      carPatent: values.carPatent.toUpperCase(),
      brand: capitalizeFirstLetter(values.brand),
      model: capitalizeFirstLetter(values.model),
    };
    try {
      console.log("Valores enviados:", modifiedValues);
      await createVehicle(modifiedValues);
      setIsModalOpen(false);
      resetForm();
    } catch (error) {
      console.error("Error al enviar el vehículo:", error);
    }
  };

  const normalizeCarPatent = (value) => {
    return value.toUpperCase();
  };

  const capitalizeFirstLetter = (value) => {
    return value.charAt(0).toUpperCase() + value.slice(1);
  };

  if (!isModalOpen) return null;

  return (
    <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50">
      <div className="bg-white rounded-lg p-6 w-full max-w-md mx-4 dark:bg-gray-900 dark:text-slate-300">
        <h2 className="text-2xl font-bold mb-4">Agregar Vehículo</h2>
        <Formik
          initialValues={{ carPatent: "", brand: "", model: "", idClient: "" }}
          validationSchema={vehicleValidationSchema}
          onSubmit={handleSubmit}
        >
          {({ isSubmitting, setFieldValue }) => (
            <Form>
              <div className="mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2 dark:text-slate-400"
                  htmlFor="carPatent"
                >
                  Patente
                </label>
                <Field
                  type="text"
                  name="carPatent"
                  id="carPatent"
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-slate-700 dark:text-slate-400"
                  onChange={(e) =>
                    setFieldValue(
                      "carPatent",
                      normalizeCarPatent(e.target.value)
                    )
                  }
                />
                <ErrorMessage
                  name="carPatent"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2 dark:text-slate-400"
                  htmlFor="brand"
                >
                  Marca
                </label>
                <Field
                  type="text"
                  name="brand"
                  id="brand"
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-slate-700 dark:text-slate-400"
                  onChange={(e) =>
                    setFieldValue(
                      "brand",
                      capitalizeFirstLetter(e.target.value)
                    )
                  }
                />
                <ErrorMessage
                  name="brand"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2 dark:text-slate-400"
                  htmlFor="model"
                >
                  Modelo
                </label>
                <Field
                  type="text"
                  name="model"
                  id="model"
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-slate-700 dark:text-slate-400"
                  onChange={(e) =>
                    setFieldValue(
                      "model",
                      capitalizeFirstLetter(e.target.value)
                    )
                  }
                />
                <ErrorMessage
                  name="model"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2 dark:text-slate-400"
                  htmlFor="idClient"
                >
                  Dueño del Vehículo
                </label>
                <Field
                  as="select"
                  name="idClient"
                  id="idClient"
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-slate-700 dark:text-slate-400"
                >
                  <option value="" label="Seleccione un cliente" />
                  {clients.map((client) => (
                    <option key={client.idUser} value={client.idUser}>
                      {client.name} {client.lastname}
                    </option>
                  ))}
                </Field>
                <ErrorMessage
                  name="idClient"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="flex justify-end">
                <button
                  type="button"
                  onClick={() => setIsModalOpen(false)}
                  className="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded mr-2"
                >
                  Cancelar
                </button>
                <button
                  type="submit"
                  disabled={isSubmitting}
                  className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                >
                  Guardar
                </button>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
};

export default AddVehicle;
