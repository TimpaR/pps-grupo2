import React, { useState, useEffect } from "react";
import axios from "axios";
import { AiFillDelete } from "react-icons/ai";
import DeleteVehicle from "../deleteVehicle/DeleteVehicle";
import { useAuth } from "../../services/authContext/AuthContext";

const VehiclesList = ({ searchTerm }) => {
  const [vehicles, setVehicles] = useState([]);
  const [users, setUsers] = useState([]);
  const { user } = useAuth();
  const [filteredVehicles, setFilteredVehicles] = useState([]);
  const [selectedVehicles, setSelectedVehicles] = useState(null);
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);

  useEffect(() => {
    const fetchVehicles = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7267/api/Vehicle/getAllVehicles"
        );
        setVehicles(response.data);
        setFilteredVehicles(response.data);
      } catch (error) {
        console.error("Error al obtener vehículos:", error);
      }
    };

    fetchVehicles();
  }, []);

  useEffect(() => {
    const fetchUsers = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7267/api/User/GetAllUsers"
        );
        setUsers(response.data);
      } catch (error) {
        console.error("Error al obtener usuarios:", error);
      }
    };

    fetchUsers();
  }, []);

  useEffect(() => {
    const filtered = vehicles.filter((vehicle) => {
      const user = getClientName(vehicle.idVehicle);
      return user.toLowerCase().includes(searchTerm.toLowerCase());
    });
    setFilteredVehicles(filtered);
  }, [searchTerm, vehicles]);

  const getClientName = (id) => {
    const vehicle = vehicles.find((vehicle) => vehicle.idVehicle === id);
    if (vehicle) {
      const user = users.find((user) => user.idUser === vehicle.idClient);
      return user ? `${user.name} ${user.lastname}` : "N/A";
    }
    return "N/A";
  };

  const handleDeleteClick = (vehicle) => {
    setSelectedVehicles(vehicle);
    setIsDeleteModalOpen(true);
  };

  const handleDelete = async () => {
    try {
      const response = await axios.get(
        "https://localhost:7267/api/Vehicle/getAllVehicles"
      );
      setVehicles(response.data);
      setFilteredVehicles(response.data);
    } catch (error) {
      console.error("Error al obtener vehículos:", error);
    }
  };

  return (
    <div className="flex justify-center items-center">
      <div className="w-full p-4 overflow-y-auto bg-gray-100 rounded-lg shadow-lg dark:bg-gray-900 dark:text-slate-300">
        {filteredVehicles.length === 0 ? (
          <p className="text-center text-gray-500">
            No hay vehículos cargados.
          </p>
        ) : (
          filteredVehicles.map((vehicle) => (
            <div
              key={vehicle.idVehicle}
              className="bg-white rounded-lg shadow-md p-4 mb-4 flex justify-between items-end dark:bg-slate-700"
            >
              <div>
                <h3 className="text-lg font-semibold">Automóvil</h3>
                <p className="text-sm">
                  <strong>Marca:</strong> {vehicle.brand}
                </p>
                <p className="text-sm">
                  <strong>Modelo:</strong> {vehicle.model}
                </p>
                <p className="text-sm">
                  <strong>Patente:</strong> {vehicle.carPatent}
                </p>
                <p className="text-sm">
                  <strong>Nombre y Apellido:</strong>{" "}
                  {getClientName(vehicle.idVehicle)}
                </p>
              </div>
              {user.userType === "client" && (
                <div className="flex space-x-4">
                  <AiFillDelete
                    className="text-red-500 cursor-pointer"
                    onClick={() => handleDeleteClick(vehicle)}
                  />
                </div>
              )}
            </div>
          ))
        )}
      </div>
      {selectedVehicles && (
        <>
          <DeleteVehicle
            isOpen={isDeleteModalOpen}
            onClose={() => setIsDeleteModalOpen(false)}
            vehicle={selectedVehicles}
            onDelete={handleDelete}
          />
        </>
      )}
    </div>
  );
};

export default VehiclesList;
