import React, { useState } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { vehicleValidationSchema } from "../../utils/validationSchema/ValidationSchema";
import { createVehicle } from "../../utils/APIs/APIs";
import { useAuth } from "../../services/authContext/AuthContext";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const AddVehicleClient = ({ isModalOpen, setIsModalOpen }) => {
  const { user } = useAuth();
  const [isSubmitting, setIsSubmitting] = useState(false);

  const handleSubmit = async (values, { resetForm }) => {
    setIsSubmitting(true); // Indicar que se está enviando el formulario
    try {
      await createVehicle({ ...values, idClient: user.idUser });
      toast.success("¡Vehículo agregado correctamente!", {
        autoClose: 3000, // Cerrar la notificación después de 3 segundos (3000 milisegundos)
      });
      setTimeout(() => {
        setIsModalOpen(false); // Cerrar el modal después de que se muestra la notificación
        resetForm(); // Reiniciar el formulario
      }, 3000); // Cerrar el modal después de 3 segundos (3000 milisegundos)
    } catch (error) {
      toast.error("Error al agregar vehículo. Por favor, inténtelo de nuevo.");
    } finally {
      setIsSubmitting(false); // Indicar que el envío del formulario ha terminado
    }
  };

  const normalizeCarPatent = (value) => {
    return value.toUpperCase(); // Convertir a mayusculas
  };

  const capitalizeFirstLetter = (value) => {
    return value.charAt(0).toUpperCase() + value.slice(1); // Primer letra en mayuscula
  };

  if (!isModalOpen) return null;

  return (
    <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50">
      <div className="bg-white rounded-lg p-6 w-full max-w-md mx-4 dark:bg-gray-900">
        <h2 className="text-2xl font-bold mb-4 dark:text-slate-300">
          Agregar Vehículo
        </h2>
        <Formik
          initialValues={{
            carPatent: "",
            brand: "",
            model: "",
            idClient: user.idUser,
          }}
          validationSchema={vehicleValidationSchema}
          onSubmit={handleSubmit}
          enableReinitialize // Permite reinicializar los valores iniciales cuando cambian
        >
          {({ isSubmitting, setFieldValue }) => (
            <Form>
              <div className="mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2 dark:text-slate-400"
                  htmlFor="carPatent"
                >
                  Patente
                </label>
                <Field
                  type="text"
                  name="carPatent"
                  id="carPatent"
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-slate-700 dark:text-slate-400"
                  onChange={(e) =>
                    setFieldValue(
                      "carPatent",
                      normalizeCarPatent(e.target.value)
                    )
                  }
                />
                <ErrorMessage
                  name="carPatent"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2 dark:text-slate-400"
                  htmlFor="brand"
                >
                  Marca
                </label>
                <Field
                  type="text"
                  name="brand"
                  id="brand"
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-slate-700 dark:text-slate-400"
                  onChange={(e) =>
                    setFieldValue(
                      "brand",
                      capitalizeFirstLetter(e.target.value)
                    )
                  }
                />
                <ErrorMessage
                  name="brand"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="mb-4">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2 dark:text-slate-400"
                  htmlFor="model"
                >
                  Modelo
                </label>
                <Field
                  type="text"
                  name="model"
                  id="model"
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-slate-700 dark:text-slate-400"
                  onChange={(e) =>
                    setFieldValue(
                      "model",
                      capitalizeFirstLetter(e.target.value)
                    )
                  }
                />
                <ErrorMessage
                  name="model"
                  component="div"
                  className="text-red-500 text-sm"
                />
              </div>
              <div className="flex justify-end">
                <button
                  type="button"
                  onClick={() => setIsModalOpen(false)}
                  className="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded mr-2"
                >
                  Cancelar
                </button>
                <button
                  type="submit"
                  disabled={isSubmitting}
                  className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                >
                  Guardar
                </button>
              </div>
            </Form>
          )}
        </Formik>
      </div>
      <ToastContainer />
    </div>
  );
};

export default AddVehicleClient;
