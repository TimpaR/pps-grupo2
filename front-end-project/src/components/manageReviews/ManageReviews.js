import React from "react";
import ReviewList from "../reviewList/ReviewList";

const ManageReviews = () => {
  return (
    <div className="w-full h-full flex flex-col">
      <div className="mb-4 flex justify-between items-center">
        <h2 className="text-2xl font-bold text-gray-800  dark:text-slate-300">
          Listado de Reseñas
        </h2>
      </div>
      <div className="flex-1 overflow-auto">
        <ReviewList />
      </div>
    </div>
  );
};

export default ManageReviews;
