import "./index.css";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import ViewAdEm from "./pages/viewAdEm/ViewAdEm";
import ViewEmployee from "./pages/viewEmployee/ViewEmployee";
import ProtectedAdmin from "./security/protectedAdmin/ProtectedAdmin";
import ProtectedEmployee from "./security/protectedEmployee/ProtectedEmployee";
import ProtectedClient from "./security/protectedClient/ProtectedClient";
import PageNotFound from "./pages/pageNotFound/PageNotFound";
import Home from "./pages/HomePage/Home";
import ViewClientShift from "./pages/viewClientShift/ViewClientShift";
import ViewMyShift from "./pages/viewMyShift/ViewMyShift";
import ViewUserData from "./pages/viewUserData/ViewUserData";
import ViewClientVehicles from "./pages/viewClientVehicles/ViewClientVehicles";
import { ToastContainer } from "react-toastify";
import PermissionDenied from "./pages/permissionDenied/PermissionDenied";

function App() {
  return (
    <div className="bg-white dark:bg-slate-800">
      <Router>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/side" element={<PageNotFound />} />
          <Route
            path="/adm"
            element={
              <ProtectedAdmin>
                <ViewAdEm />
              </ProtectedAdmin>
            }
          />
          <Route
            path="/emp"
            element={
              <ProtectedEmployee>
                <ViewEmployee />
              </ProtectedEmployee>
            }
          />
          <Route
            path="/clientShift"
            element={
              <ProtectedClient>
                <ViewClientShift />
              </ProtectedClient>
            }
          />
          <Route
            path="/myShift"
            element={
              <ProtectedClient>
                <ViewMyShift />
              </ProtectedClient>
            }
          />
          <Route
            path="/myData"
            element={
              <ProtectedClient>
                <ViewUserData />
              </ProtectedClient>
            }
          />
          <Route
            path="/clientvehicles"
            element={
              <ProtectedClient>
                <ViewClientVehicles />
              </ProtectedClient>
            }
          />
          <Route path="*" element={<PageNotFound />} />
          <Route path="/permissionDenied" element={<PermissionDenied />} />
        </Routes>
      </Router>
      <ToastContainer />
    </div>
  );
}

export default App;
