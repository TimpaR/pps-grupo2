import React, { useState } from "react";
import Navbar from "../../components/navbar/Navbar";
import Sidebar from "../../components/sideBar/Sidebar";
import Branches from "../../components/Branches/Branches";
import ManageUsers from "../../components/manageUsers/ManageUsers";
import ManageReviews from "../../components/manageReviews/ManageReviews";
import Services from "../../components/services/Services";
import Vehicles from "../../components/vehicles/Vehicles";
import Statics from "../../components/statics/Statics";

const ViewAdEm = () => {
  const [selectedOption, setSelectedOption] = useState("usuarios");

  const renderContent = () => {
    switch (selectedOption) {
      case "usuarios":
        return <ManageUsers />;
      case "sucursales":
        return <Branches />;
      case "services":
        return <Services />;
      case "estadisticas":
        return <Statics />;
      case "reseñas":
        return <ManageReviews />;
      case "vehiculos":
        return <Vehicles />;

      default:
        return null;
    }
  };

  return (
    <div className="flex flex-col h-screen">
      <Navbar />
      <div className="flex flex-1 overflow-hidden">
        <div className="w-64 bg-gray-200">
          <Sidebar
            setSelectedOption={setSelectedOption}
            selectedOption={selectedOption}
          />
        </div>
        <div className="flex-1 p-4 overflow-auto">{renderContent()}</div>
      </div>
    </div>
  );
};

export default ViewAdEm;
