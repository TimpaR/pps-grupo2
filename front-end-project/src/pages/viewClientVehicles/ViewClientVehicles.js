import React from "react";
import Navbar from "../../components/navbar/Navbar";
import Footer from "../../components/footer/Footer";
import ClientVehicles from "../../components/clientVehicles/ClientVehicles";
import ClientVehiclesList from "../../components/clientVehiclesList/ClientVehiclesList";

const ViewClientVehicles = () => {
  return (
    <div className="flex flex-col min-h-screen">
      <Navbar />
      <ClientVehicles />
      <div className="flex-1">
        <ClientVehiclesList />
      </div>
      <Footer />
    </div>
  );
};

export default ViewClientVehicles;
