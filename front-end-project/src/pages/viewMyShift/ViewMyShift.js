import React from 'react'
import Navbar from '../../components/navbar/Navbar'
import Footer from '../../components/footer/Footer'
import MyShift from '../../components/myShift/MyShift'

const ViewMyShift = () => {
  return (
    <div>
      <Navbar/>
        <MyShift/>
      <Footer/>
    </div>
  )
}

export default ViewMyShift