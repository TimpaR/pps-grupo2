import React, { useEffect } from "react";
import { useNavigate } from "react-router";
import service from "../../assets/LogoBlack.png";
import Spinner from "../../components/spinner/Spinner";

const PageNotFound = () => {
  const navigate = useNavigate();

  useEffect(() => {
    const timer = setTimeout(() => {
      navigate("/");
    }, 5000);

    return () => clearTimeout(timer);
  }, []);
  return (
    <div className="flex flex-col md:flex-row justify-center items-center h-screen">
      <div className="grid grid-cols-1 md:grid-cols-2 gap-5 w-full max-w-6xl px-4">
        <div className="flex flex-col justify-center items-center md:items-start">
          <h2 className="mb-1 font-bold text-4xl text-center md:text-left">
            ¡Oops! No se encontró la página
          </h2>
          <Spinner />
        </div>

        <div className="flex justify-center items-center">
          <img
            src={service}
            alt="LogoHome"
            className="max-w-[200px] md:max-w-full h-auto"
          />
        </div>
      </div>
    </div>
  );
};

export default PageNotFound;
