import React, { useEffect, useState } from "react";
import Navbar from "../../components/navbar/Navbar";
import Footer from "../../components/footer/Footer";
import { useAuth } from "../../services/authContext/AuthContext";
import axios from "axios";
import UpdateUser from "../../components/updateUser/UpdateUser";

const ViewUserData = () => {
  const { authToken, user } = useAuth();
  const [userData, setUserData] = useState(null);
  const [isUpdateModalOpen, setIsUpdateModalOpen] = useState(false);
  const userId = user.idUser;

  useEffect(() => {
    const fetchUserData = async () => {
      try {
        const response = await axios.get(
          `https://localhost:7267/api/User/GetUserById/${userId}`,
          {
            headers: {
              Authorization: `Bearer ${authToken}`,
            },
          }
        );
        setUserData(response.data);
      } catch (error) {
        console.error("Error fetching user data:", error);
      }
    };

    if (authToken && userId) {
      fetchUserData();
    }
  }, [authToken, userId]);

  if (!userData) {
    return (
      <div className="flex flex-col items-center justify-center h-screen">
        <div className="animate-spin rounded-full h-16 w-16 border-t-4 border-b-4 border-blue-500 mb-4"></div>
        <p className="text-xl">Cargando...</p>
      </div>
    );
  }

  const handleEditClick = () => {
    setIsUpdateModalOpen(true);
  };

  const handleUpdate = async () => {
    try {
      const response = await axios.get(
        `https://localhost:7267/api/User/GetUserById/${userId}`
      );
      setUserData(response.data);
    } catch (error) {
      console.error("Error al obtener usuarios:", error);
    }
  };

  return (
    <div className="flex flex-col min-h-screen">
      <Navbar />
      <div className="flex-grow dark:text-slate-300">
        <div className="container mx-auto my-8 p-6">
          <h1 className="text-2xl font-bold mb-4">Mis datos</h1>
          <div className="flex flex-col gap-4">
            <div className="mb-2">
              <p className="font-semibold">Nombre:</p>
              <p>{userData.name}</p>
            </div>
            <div className="mb-2">
              <p className="font-semibold">Apellido:</p>
              <p>{userData.lastname}</p>
            </div>
            <div className="mb-2">
              <p className="font-semibold">Email:</p>
              <p>{userData.email}</p>
            </div>
            <div className="mb-2">
              <p className="font-semibold">Teléfono:</p>
              <p>{userData.phone}</p>
            </div>
          </div>

          <div className="flex justify-end mt-20">
            <button
              onClick={() => handleEditClick()}
              className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
            >
              Modificar Datos
            </button>
          </div>
        </div>
      </div>
      <UpdateUser
        isOpen={isUpdateModalOpen}
        onClose={() => setIsUpdateModalOpen(false)}
        userEdit={userData}
        onUpdate={handleUpdate}
      />
      <Footer className="mt-auto" />
    </div>
  );
};

export default ViewUserData;
