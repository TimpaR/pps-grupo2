import React, { useState } from "react";
import Navbar from "../../components/navbar/Navbar";
import Footer from "../../components/footer/Footer";
import Main from "../../components/main/Main";
import ServicesOffered from "../../components/servicesOffered/ServicesOffered";
import Review from "../../components/review/Review";
import ChatBot from "../../components/chatBot/ChatBot";
import { RiChatSmile3Line } from "react-icons/ri";

const Home = () => {
  const [showChatBot, setShowChatBot] = useState(false);

  const handleToggleChatBot = () => {
    setShowChatBot(!showChatBot);
  };

  return (
    <div>
      <Navbar />
      <Main />
      <ServicesOffered />
      <Review />
      <Footer />
      <div
        onClick={handleToggleChatBot}
        className={`fixed bottom-4 right-4 z-50 bg-blue-500 text-white p-2 rounded-full shadow-xl cursor-pointer ${
          showChatBot ? "scale-75" : ""
        }`}
      >
        <RiChatSmile3Line className="w-8 h-8" />
      </div>
      {showChatBot && (
        <div className="fixed bottom-16 right-4 z-50 bg-white p-4 rounded-lg shadow-xl">
          <ChatBot setShowChatBot={setShowChatBot} />
        </div>
      )}
    </div>
  );
};

export default Home;
