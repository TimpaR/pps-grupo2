
# Proyecto
DetailingGARAGE (Front-End)

## Descripción del Proyecto
El objetivo principal de este proyecto es implementar la interfaz de usuario para una aplicación web que permita a los usuarios programar y gestionar fácilmente sus turnos para servicios de mantenimiento y mejora estética, proporcionándoles así la tranquilidad y comodidad que necesitan para mantener sus vehículos en perfectas condiciones, sin sacrificar tiempo ni esfuerzo en la coordinación de citas y servicios.

## Estructura del Proyecto
El proyecto está organizado en diferentes directorios y archivos clave:

/public: Contiene archivos estáticos accesibles directamente desde la interfaz de usuario.
/src: Contiene el código fuente principal del front-end, incluyendo componentes, estilos y lógica de la aplicación.
/components: Componentes reutilizables de React utilizados en toda la aplicación.
/pages: Páginas principales de la aplicación, cada una representando una vista específica.
/styles: Estilos globales y/o específicos de componentes utilizando CSS o Styled Components.
/utils: Funciones de utilidad y lógica compartida en toda la aplicación.
App.js: Punto de entrada principal de la aplicación React.

## Tecnologías Utilizadas

* React
* Tailwind

## Librerias - Bibliotecas
* axios
* formik
* js-cookie
* jsonwebtoken
* jwt-decode
* navigate
* react-alice-carousel
* react-chatbot-kit
* react-dom
* react-icons
* react-router
* react-router-dom
* react-scripts
* react-slick
* react-toastify
* slick-carousel
* stream-browserify
* util
* web-vitals
* yup

## Instalación y Uso

1) Clona este repositorio en tu máquina local
```bash
https://gitlab.com/TimpaR/pps-grupo2.git
```
2) Navega al directorio del proyecto:
```bash
cd front-end-project
```
3) Instala las dependencias usando npm:
```bash
npm install
```
4) Inicia la aplicación en modo de desarrollo:
```bash
npm start
```
5) La aplicación estará disponible en tu navegador en la dirección http://localhost:3000.


