CREATE DATABASE  IF NOT EXISTS `detailing_db` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `detailing_db`;
-- MySQL dump 10.13  Distrib 8.0.36, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: detailing_db
-- ------------------------------------------------------
-- Server version	8.0.36

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `branches`
--

DROP TABLE IF EXISTS `branches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `branches` (
  `id_branch` int NOT NULL AUTO_INCREMENT,
  `location` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_branch`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `branches`
--

LOCK TABLES `branches` WRITE;
/*!40000 ALTER TABLE `branches` DISABLE KEYS */;
INSERT INTO `branches` VALUES (1,'Rosario','DetailingRosario','34154375886'),(2,'Funes','DetailingFunes','34154365780');
/*!40000 ALTER TABLE `branches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reviews` (
  `id_review` int NOT NULL AUTO_INCREMENT,
  `id_client` int NOT NULL,
  `rating` int NOT NULL,
  `comment` text,
  PRIMARY KEY (`id_review`),
  KEY `reviews_ibfk_1` (`id_client`),
  CONSTRAINT `reviews_ibfk_1` FOREIGN KEY (`id_client`) REFERENCES `users` (`id_user`),
  CONSTRAINT `reviews_chk_1` CHECK (((`rating` >= 1) and (`rating` <= 5)))
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews`
--

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
INSERT INTO `reviews` VALUES (1,4,5,'Muy buenos servicios');
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `services` (
  `id_servicio` int NOT NULL AUTO_INCREMENT,
  `url_image` varchar(255) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `price` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id_servicio`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (1,'https://fuelcarmagazine.com/wp-content/uploads/2020/09/Alineacion-y-Balanceo.jpg','Alineacion de goma','Alineacion de gomas',1900000.00),(2,'https://www.firststop.es/adobe/dynamicmedia/deliver/dm-aid--424b6c3d-2a0d-4103-ba12-70db79f2af06/cambiar-bujias-del-coche-01.jpg?quality=100&preferwebp=true&width=600','Limpieza del filtro de aire, de gasolina y las bujías','Limpieza filtros aire y gasolina, y bujías',190000.00),(4,'https://www.jacsautocare.com/images/Alinear-los-Faros-del-vehiculo-en-guayaquil.jpg','Revisión de luces ','Revisión y ajuste de luces',135000.00);
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shifts`
--

DROP TABLE IF EXISTS `shifts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `shifts` (
  `id_shift` int NOT NULL AUTO_INCREMENT,
  `id_vehicle` int DEFAULT NULL,
  `id_branch` int NOT NULL,
  `id_service` int DEFAULT NULL,
  `date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  PRIMARY KEY (`id_shift`),
  KEY `shifts_ibfk_1` (`id_vehicle`),
  KEY `shifts_ibfk_2` (`id_branch`),
  KEY `shifts_ibfk_3` (`id_service`),
  CONSTRAINT `shifts_ibfk_1` FOREIGN KEY (`id_vehicle`) REFERENCES `vehicles` (`id_vehicle`),
  CONSTRAINT `shifts_ibfk_2` FOREIGN KEY (`id_branch`) REFERENCES `branches` (`id_branch`),
  CONSTRAINT `shifts_ibfk_3` FOREIGN KEY (`id_service`) REFERENCES `services` (`id_servicio`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shifts`
--

LOCK TABLES `shifts` WRITE;
/*!40000 ALTER TABLE `shifts` DISABLE KEYS */;
INSERT INTO `shifts` VALUES (1,2,1,1,'2024-07-01','22:00:22','22:00:25'),(2,2,1,1,'2024-07-02','22:00:34','22:00:35'),(3,5,1,2,'2024-07-04','20:13:56','20:13:59'),(4,2,1,1,'2024-07-05','20:14:11','20:14:13'),(5,NULL,1,1,'2024-07-22','10:00:00','11:00:00'),(7,5,1,2,'2024-07-24','10:00:00','11:00:00'),(8,5,1,1,'2024-07-22','10:00:00','11:00:00'),(9,NULL,2,4,'2024-07-23','09:00:00','10:00:00');
/*!40000 ALTER TABLE `shifts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id_user` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `user_type` enum('admin','employee','client') NOT NULL,
  `id_branch` int DEFAULT NULL,
  PRIMARY KEY (`id_user`),
  KEY `id_branch` (`id_branch`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`id_branch`) REFERENCES `branches` (`id_branch`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Tomas','Roncoroni','tomi@gmail.com','3416648588','$2a$11$3MEuRKVpBurlwMZ1Andix.qZEYbx13/B/S0wMma6fW72bl56FyTAm','admin',NULL),(2,'Renzo','Timpanaro','renzo@gmail.com','3415837454','$2a$11$xJ4ruBHyv8pkI/6Je9164.sngndLa9M4mYQOcE3812fOcLjkae7OW','employee',1),(4,'Sebastian','Sueiro','seba@gmail.com','3416848596','$2a$11$r1oYvw0r4KKV6r4mZ2eWhupGdfACpPAveJ9d8bCe/lC/Kq7rngJLO','client',NULL),(6,'Francisco','Ruiz','fran@gmail.com','3415467587','$2a$11$hZ6q510mHx4sI7bAzALreuFJW3X2Mzy5G6Q/A3CfLazqvBBLAPQJu','client',NULL),(7,'Carlos','Monzon','carlos@gmail.com','3416645869','$2a$11$by3e.Fn5lJtS6QFiWBbKbuU1DHHiuJH8DTn/GFFkWIE7sw/JVtCc6','client',NULL),(8,'Roberto','Jimenez','roberto@gmail.com','3415438580','$2a$11$Yu1ZE8HY06LvkAHdnbRu3.6S1nfosWOYUMl6j0g/JLA0iQUvFTQ66','client',NULL),(9,'Juan','Diaz','juan@gmail.com','3416438690','$2a$11$j.QuQupkJeDOPeNWmuR5YOfoNFsrimL.3q.KtQOa4h5enwZ384cji','client',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehicles`
--

DROP TABLE IF EXISTS `vehicles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vehicles` (
  `id_vehicle` int NOT NULL AUTO_INCREMENT,
  `id_client` int NOT NULL,
  `car_patent` varchar(20) NOT NULL,
  `model` varchar(50) NOT NULL,
  `brand` varchar(50) NOT NULL,
  PRIMARY KEY (`id_vehicle`),
  KEY `vehicles_ibfk_1` (`id_client`),
  CONSTRAINT `vehicles_ibfk_1` FOREIGN KEY (`id_client`) REFERENCES `users` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicles`
--

LOCK TABLES `vehicles` WRITE;
/*!40000 ALTER TABLE `vehicles` DISABLE KEYS */;
INSERT INTO `vehicles` VALUES (2,4,'ICI235','Astra','Chevrolet'),(4,8,'ICI234','Captiva','Chevrolet'),(5,9,'ICI234','Captiva','Chevrolet');
/*!40000 ALTER TABLE `vehicles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'detailing_db'
--

--
-- Dumping routines for database 'detailing_db'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-07-22 17:06:05
